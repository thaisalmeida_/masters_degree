from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from numpy import asarray, sum, arange, array
from numpy import zeros, log2, nditer, isnan
from sklearn.model_selection import LeaveOneOut
from sklearn.naive_bayes import MultinomialNB, GaussianNB
from sklearn import svm, linear_model
from csv import reader, writer, QUOTE_ALL
from csv import field_size_limit
from sklearn.preprocessing import MinMaxScaler
from re import sub, findall, match, MULTILINE
from textstat.textstat import textstat
from nltk.parse.stanford import StanfordParser
from nltk.tokenize import sent_tokenize
from nltk import Tree

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from sklearn.decomposition import PCA
from linguistic_module import *
from math import log
from os import listdir
from time import strftime,time
from json import loads
from sys import maxsize,argv





def data_cleaning(directory, data, labels, news_field, params):
	list_docs_3, labels_docs = [], []
	list_docs_2, list_docs_1 = [], []

	for i in range(len(data)):
		if(news_field == 'HEADLINE'):
			text_field = data[i][0]
		else:
			text_field = data[i][1]

		aux1, aux2 = [], []
		if('punct' in params):
			text = sub(r'[^\w\s]',' ', text_field).strip()    
		if('digits' in params):
			text = sub(r'[^\D]',' ',text).strip()
			aux1 = [text] + data[i][3:] #punct + digits
			aux2 = [sub(r'[^\D]',' ',text_field).strip()] + data[i][3:] #digits    			
		if('stopwords' in params):
			text = tokenizer(text)
			text = remove_stopwords_corpus(text)
			text = ' '.join(text)

		if(text != ''):
			list_docs_3.append([text] + data[i][3:])
			list_docs_2.append(aux1)
			list_docs_1.append(aux2)						
			labels_docs.append(labels[i])

	
	return list_docs_3, list_docs_2, list_docs_1, labels_docs 


def characters_stat(text):
    try:
        return character_count(text)
    except ValueError:
        return 0
     
def complexWords_stat(text):
    try:
        return textstat.dale_chall_readability_score(text)
    except ValueError:
        return 0
    
def longWords_stat(text):
    try:
        return avg_sentence_length(text)
    except ValueError:
        return 0
    
def numberSyllables_stat(text):
    try:
        return textstat.syllable_count(text, lang='en_US')
    except ValueError:
        return 0
    
def lexicon_count_stat(text):
    try:
        return textstat.lexicon_count(text, removepunct=True)
    except ValueError:
        return 0

def sentence_count_stat(text):
    try:
        return textstat.sentence_count(text)
    except ValueError:
        return 0
    

def flesch_reading_ease_stat(text):
    try:
        return textstat.flesch_reading_ease(text)
    except ValueError:
        pass
        return 0
    
def smog_index_stat(text):
    try:
        return textstat.smog_index(text)
    except ValueError:
        return 0
    
def flesch_kincaid_grade_stat(text):
    try:
        return textstat.flesch_kincaid_grade(text)
    except ValueError:
        return 0
    
def coleman_liau_index_stat(text):
    try:
        return textstat.coleman_liau_index(text)
    except ValueError:
        return 0
    
def automated_readability_index_stat(text):
    try:
        return textstat.automated_readability_index(text)
    except ValueError:
        return 0
    
def difficult_words_stat(text):
    try:
        return textstat.difficult_words(text)
    except ValueError:
        return 0
    
def linsear_write_formula_stat(text):
    try:
        return textstat.linsear_write_formula(text)
    except ValueError:
        return 0
    
def gunning_fog_stat(text):
    try:
        return textstat.gunning_fog(text)
    except ValueError:
        return 0

def store_silverman_files(X , label):

	directory = ''
	label_name = 'fake' if label == 0 else 'real'
	filename = label_name+'news_silverman.csv' 

	file_out = open(directory+filename,'w')
	writer_out = writer(file_out, delimiter = ',', quoting=QUOTE_ALL)
	for doc in X:
		headline, content =  doc[0], doc[1]
		writer_out.writerow(tuple([headline, content, label]))

	file_out.close()  


def read_dataset(dataset, label):
	X = []
	directory = 'datasets/'+ dataset + '/'
	with open(directory+label+'news_'+dataset+'_liwc.csv') as file_in:
		reader_in = reader(file_in, delimiter = ',', quoting=QUOTE_ALL)
		for doc in reader_in:
			X.append(doc)	
	return X						


def read_traintest_folds(directory, news_field, n,cfg=None):
	X_train, Y_train, X_test, Y_test = [], [], [], []
	doc, label = [], []
	field_size_limit(maxsize)	

	if(cfg):
		file_train = 'clean_train_' + news_field + '_cfgprod_' + str(n) + '.csv'
		file_test  = 'clean_test_'  + news_field + '_cfgprod.csv'	
	else:
		file_train = 'clean_train_' + news_field + '_' +  str(n) + '.csv'
		file_test  = 'clean_test_'  + news_field + '.csv'			

	sample = 0
	with open(directory + file_train, "r") as file_train_in,\
		open(directory + file_test, "r") as file_test_in:
		reader_train = reader(file_train_in, delimiter = ',', quoting = QUOTE_ALL)
		reader_test  = reader(file_test_in, delimiter = ',', quoting = QUOTE_ALL)
		for i in reader_train:
			if(cfg):
				X_train.append(i[0])								
			else:        		
				aux = [i[0]]
				for j in i[1:-1]:
					aux.append(float(j))
				X_train.append(aux)				
			Y_train.append(int(i[-1]))
		for i in reader_test:	     
			if(sample == n):
				if(cfg):
					X_test = [i[0]]       			        			
				else:
					aux = [i[0]]
					for j in i[1:-1]:
						aux.append(float(j))
					X_test = aux					
				Y_test.append(int(i[-1]))
				break
			sample +=1	        	

	return X_train, Y_train, X_test, Y_test 

def build_silverman(news_field):    
    #This function returns all the articles versions where 
    #headline and body share the same stance
    #for, ignoring, against, observing check the body text (page 101-103)
    #340 claims -> True 71, False 130, Unknown, 139
    #True news -> 1789, False news -> 1032
    #Total number of lines = 7112
    
    claims, articles, fake_news, real_news = {}, {}, [], []
    HEADLINE, CONTENT, BOTH = 1, 2, 3
    directory = 'datasets_public_available/emergent/'
    filename  = 'url-versions-2015-06-14.csv'
    keys = {'claimId':0, 'claimSlug':1, 'claimHeadline':2, 'claimTruthiness':3, 'articleId':4,
    'articleUrl':5, 'articleVersion':6, 'articleVersionId':7, 'articleHeadline':8, 'articleByline':9,
    'articleStance':10, 'articleHeadlineStance':11, 'articleBody':12} 
    
    with open(directory+filename, "r") as arq_in:
        reader_in = reader(arq_in, delimiter=',', quoting=QUOTE_ALL)
        next(reader_in)
        for t in reader_in:
            key, clstance = t[keys['claimId']], t[keys['claimTruthiness']]
            hstance, bstance = t[keys['articleHeadlineStance']], t[keys['articleStance']]
            if((hstance == bstance) and (hstance in ['for','against','observing'])):
                articles[t[keys['articleVersionId']]] = [t[keys['articleHeadline']],\
                         t[keys['articleBody']], hstance, key]
            if(clstance in ['false','true']):
                claims[key] = clstance

    for news in articles:
        id = articles[news][3]
        if(claims.get(id) != None):
            claims_stance, news_stance = claims[id], articles[news][2]
            if(news_stance == 'for' or news_stance == 'observing'):
                articles[news].append(claims_stance)
            elif(news_stance == 'against'):
                if(claims_stance =='false'):
                    articles[news].append('true')
                else:
                    articles[news].append('false')
            
            if(news_field == HEADLINE):
                text = articles[news][0]
            elif(news_field == CONTENT):
                text = articles[news][1]
            else:
                text = [articles[news][0], articles[news][1]]
            
            if(articles[news][4] == 'true'):
                real_news.append(text)
            else:
                fake_news.append(text)
                    
    return fake_news, real_news
    

def tfidf_average_values(X):
	#sum of tfidf values of a term in all documents.
	nro_docs, nro_terms = X.shape
	tfidf_sum_list = sum(X, axis=0)

	tfidf_avg_val = tfidf_sum_list/float(nro_docs)
	total = tfidf_avg_val.sum()

	return tfidf_avg_val, total


def build_reference_histogram(X):
	tfidf_avg_val, tfidf_sum = tfidf_average_values(X)
	word_avg_prob = np.zeros((tfidf_avg_val.size,), dtype=float)    

	index = 0
	#improve	
	for tfidf in nditer(tfidf_avg_val):
		if(tfidf_sum > 0):
			word_avg_prob[index] = tfidf/tfidf_sum
		else:
			word_avg_prob[index] = 0.0
		index += 1
	return word_avg_prob

def build_individual_histogram(X, vectorizer, new_doc, setup):
	total = 0.0
	nro_docs, nro_terms = X.shape
	word_prob = np.zeros((nro_terms,), dtype=float)    	 
	word_prob_pdf = np.zeros((nro_terms,), dtype=float)    


	if(vectorizer != None):
		X_new = vectorizer.transform([new_doc])
		rows, cols = X_new.nonzero()

		for row, col in zip(rows, cols):
			word_prob[col] = X_new[row,col]/float(nro_docs)
		total = word_prob.sum()

	# else:
	# 	if('CFG-H+JSD' not in setup):
	# 		tokens = tokenizer(new_doc)
	# 		X_new = part_of_speech_tagger(tokens) 
	# 	else:
	# 		X_new = new_doc
	# 	for index in range(nro_terms):
	# 		value = X_new[index]/nro_docs
	# 		word_prob.append(value)
	# 		total += value        

	index = 0		
	for i in nditer(word_prob):    
		if(total > 0):
			word_prob_pdf[index] = i/total
		else:
			word_prob_pdf[index] = 0.0
		index += 1

	return word_prob_pdf         

def entropy(p, base):
    p_nzero  = p[ p != 0 ] 
    p_log = log2(p_nzero)
    product = p_nzero * p_log
    sum_p = product.sum() 
    entropy = ((-1) * sum_p) if sum_p != 0.0 else 0.0        

    return entropy 


def calculate_jsd(p_ref, q_ind, S_ref):
    mean_hist = (p_ref/2) + (q_ind/2)

    jsd = entropy(mean_hist,2) - (S_ref + entropy(q_ind,2))/2

    return jsd     

def build_fvector_postag(X_train, Y_train, docs_test, setup):
	train_vector, test_vector = [], []
	vect_fake, vect_msm = [], []
	fake_train, msm_train = [], []
	FAKE, REAL = 0, 1

	if('POSTAG' in setup):
		for text in X_train:
			tokens = tokenizer(text)
			train_vector.append(part_of_speech_tagger(tokens))

		for text in docs_test:
			tokens = tokenizer(text)
			test_vector.append(part_of_speech_tagger(tokens))      	    	
	else:   
		#spliting per class
		for i in range(len(Y_train)): 
			if Y_train[i] == FAKE:
				fake_train.append(X_train[i][0])
			else:
				msm_train.append(X_train[i][0])

		#building the postag matrix for each class
		for text in fake_train:
			tokens = tokenizer(text)
			vect_fake.append(part_of_speech_tagger(tokens))
		for text in msm_train:
			tokens = tokenizer(text)
			vect_msm.append(part_of_speech_tagger(tokens))   

		size_fake_voc, size_msm_voc = len(vect_fake[0]), len(vect_msm[0])
		vect_fake, vect_msm = asarray(vect_fake), asarray(vect_msm)
		#calculating reference histogram
		fake_href = build_reference_histogram(vect_fake)
		msm_href  = build_reference_histogram(vect_msm)
	    
		#calculating features
		for doc in X_train:
			fake_hind = build_individual_histogram(vect_fake, None, doc, setup)
			msm_hind  = build_individual_histogram(vect_msm, None, doc, setup)
			H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
			H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
			JSD_fake = calculate_jsd(fake_href, fake_hind)
			JSD_msm  = calculate_jsd(msm_href,  msm_hind)

			train_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])

		for doc_test in docs_test:
			fake_hind = build_individual_histogram(vect_fake, None, doc_test, setup)
			msm_hind  = build_individual_histogram(vect_msm, None, doc_test, setup)
			H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
			H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
			JSD_fake = calculate_jsd(fake_href, fake_hind)
			JSD_msm  = calculate_jsd(msm_href,  msm_hind)

			test_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])

	return train_vector, test_vector



def build_fvector_words(X_train, Y_train, docs_test, setup):
	train_vector, test_vector = [], []
	fake_train, msm_train = [], []
	FAKE, REAL = 0, 1

	#spliting per class
	for i in range(len(Y_train)): 
		if Y_train[i] == FAKE:
			fake_train.append(X_train[i][0])
		else:
			msm_train.append(X_train[i][0])    

	#building the term matrix for each class
	if(setup[-1].find('CHAR') >= 0):
	
		if('TF' in setup):
			vect_fake = CountVectorizer(analyzer='char')
			vect_msm  = CountVectorizer(analyzer='char')
		elif('TF-IDF' in setup):
			vect_fake = TfidfVectorizer(use_idf=True, sublinear_tf=False,analyzer='char')
			vect_msm  = TfidfVectorizer(use_idf=True, sublinear_tf=False,analyzer='char')	
	else:
		ngram_range = (int(setup[-1].strip()[1]), int(setup[-1].strip()[3]))

		if('TF' in setup):
			vect_fake = CountVectorizer(ngram_range = ngram_range)
			vect_msm  = CountVectorizer(ngram_range = ngram_range)
		elif('TF-IDF' in setup):
			vect_fake = TfidfVectorizer(use_idf=True, sublinear_tf=False, ngram_range = ngram_range)
			vect_msm  = TfidfVectorizer(use_idf=True, sublinear_tf=False, ngram_range = ngram_range)    
     
        
	X_fake_train = vect_fake.fit_transform(fake_train)
	X_msm_train  = vect_msm.fit_transform(msm_train)
	size_fake_voc = len(vect_fake.vocabulary_.keys())
	size_msm_voc  = len(vect_msm.vocabulary_.keys())
 	
	#calculating reference histogram
	fake_href = build_reference_histogram(X_fake_train)
	msm_href  = build_reference_histogram(X_msm_train)

	S_fake = entropy(fake_href, 2)
	S_msm  = entropy(msm_href, 2)

	#train docs
	# for doc in X_train:
	# 	fake_hind = build_individual_histogram(X_fake_train, vect_fake, doc[0], setup)
	# 	msm_hind  = build_individual_histogram(X_msm_train, vect_msm, doc[0], setup)
	# 	H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
	# 	H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
	# 	JSD_fake = calculate_jsd(fake_href, fake_hind, S_fake)
	# 	JSD_msm  = calculate_jsd(msm_href,  msm_hind, S_msm) 

	# 	train_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])

	#test docs
	fake_hind = build_individual_histogram(X_fake_train, vect_fake, docs_test[0], setup)
	msm_hind  = build_individual_histogram(X_msm_train, vect_msm, docs_test[0], setup)
	H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
	H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
	JSD_fake = calculate_jsd(fake_href, fake_hind, S_fake)
	JSD_msm  = calculate_jsd(msm_href,  msm_hind, S_msm)

	test_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])


	return train_vector, test_vector

def training_classifier_loo(X_fake, X_msm, Y, classifier):
    predict_list, grndtruth_list = [], []
    FAKE, REAL = 0, 1
    fake_train, msm_train, doc_test = [], [], []
    count = 0
    for i in Y: 
        if i == FAKE:
            fake_train = X_fake[0:i] + X_fake[i+1:]
            doc_test = X_fake[i]
            msm_train = X_msm
        else:
            msm_train  = X_msm[0:i]  + X_msm[i+1:]
            doc_test = X_msm[i]
            fake_train = X_fake

    #     X_train, Y_train, X_test = build_feature_vector(fake_train, msm_train, doc_test)
        
    #     classifier.fit(X_train, Y_train)
    #     predict_list.append(classifier.predict([X_test]))
    #     grndtruth_list.append(i)
    #     print(count,strftime("%I:%M%p"))
        
    #     count +=1
    
    # recall = recall_score(grndtruth_list, predict_list, average=None)
    # precision = precision_score(grndtruth_list, predict_list, average=None)    
    # f1 = f1_score(grndtruth_list, predict_list, average=None)
    # accuracy = accuracy_score(grndtruth_list, predict_list)

    # print("Precision: ", precision)
    # print("Recall: ", recall)
    # print("F1: ", f1)


def show_results(precision, recall, f1, f1_macro):
	precision_text_0 = precision[0][0] + '+/-' + precision[0][1]
	recall_text_0    = recall[0][0] + '+/-' + recall[0][1]
	f1_text_0 	   = f1[0][0] + '+/-' + f1[0][1]
	
	precision_text_1 = precision[1][0] + '+/-' + precision[1][1]
	recall_text_1    = recall[1][0] + '+/-' + recall[1][1]
	f1_text_1 	   = f1[1][0] + '+/-' + f1[1][1]	
	
	fake_text = precision_text_0 + '\t' + recall_text_0 + '\t' + f1_text_0
	real_text = precision_text_1 + '\t' + recall_text_1 + '\t' + f1_text_1	
	f1_macro_text = f1_macro[0] + '+/-' + f1_macro[1]
	
	return fake_text, real_text, f1_macro_text 	


def evaluate_effectiveness(ground_truth, predict_list):
	precision_list,recall_list,confusion_matrix_list = [],[],[]
	f1_list,accuracy_list,f1_macro_list = [],[],[]
	for i in range(len(predict_list)):
		precision = precision_score(ground_truth[i], predict_list[i], average=None)
		recall = recall_score(ground_truth[i], predict_list[i], average=None)
		f1 = f1_score(ground_truth[i], predict_list[i], average=None)
		accuracy = accuracy_score(ground_truth[i], predict_list[i])

		precision_list.append(precision)
		recall_list.append(recall)
		f1_list.append(f1)
		accuracy_list.append(accuracy)
		#f1_macro = (f1[0] + f1[1])/2
		#f1_macro_list.append(f1_macro)
				
		confusion_matrix_list.append(confusion_matrix(ground_truth[i], predict_list[i]))

		precision_final = statistics_metric_value(precision_list)
		recall_final = statistics_metric_value(recall_list)
		f1_final = statistics_metric_value(f1_list)
		accuracy_final = statistics_metric_value(accuracy_list)
		f1_macro_final = statistics_metric_value(f1_macro_list)

	#show_confusion_matrix(calculate_mean_confmatrix(confusion_matrix_list))
	#print(f1_list)
	return show_results(precision_final, recall_final, f1_final, f1_macro_final)

    
    
def build_fvector_readaby(X_train, X_test):
    readby_train, readby_test = [], []
    for i in range(len(X_train)):
        readby_train.append([characters_stat(X_train[i]), complexWords_stat(X_train[i]), longWords_stat(X_train[i]),
                            numberSyllables_stat(X_train[i]), lexicon_count_stat(X_train[i]), sentence_count_stat(X_train[i]),
                            flesch_reading_ease_stat(X_train[i]), smog_index_stat(X_train[i]), flesch_kincaid_grade_stat(X_train[i]),
                            coleman_liau_index_stat(X_train[i]), automated_readability_index_stat(X_train[i]),
                            difficult_words_stat(X_train[i]), linsear_write_formula_stat(X_train[i]), gunning_fog_stat(X_train[i])]) 
        
    for i in range(len(X_test)):
        readby_test.append([characters_stat(X_test[i]), complexWords_stat(X_test[i]), longWords_stat(X_test[i]),
                            numberSyllables_stat(X_test[i]), lexicon_count_stat(X_test[i]), sentence_count_stat(X_test[i]),
                            flesch_reading_ease_stat(X_test[i]), smog_index_stat(X_test[i]), flesch_kincaid_grade_stat(X_test[i]),
                            coleman_liau_index_stat(X_test[i]), automated_readability_index_stat(X_test[i]),
                            difficult_words_stat(X_test[i]), linsear_write_formula_stat(X_test[i]), gunning_fog_stat(X_test[i])])   

    return readby_train, readby_test

def combine_features(X_words, X_postag, X_readby, X_liwc, X_cfg, Y):
	X_fold = []

	for i in range(len(Y)):
		fvector = []
		if(X_words  != []):
			fvector += X_words[i]
		if(X_postag != []):
			fvector += X_postag[i]        		
		if(X_readby != []):

			fvector += X_readby[i]      		
		if(X_liwc != []):
			fvector += X_liwc[i]        		
		if(X_cfg != []):      
			fvector += X_cfg[i]  				
		X_fold.append(fvector)
	return X_fold


def split_leaveone_out(X, Y, news_field, cfg):
	sample = 0

	if(cfg):
		filename_train = 'clean_train_'+ news_field + '_cfgprod_' 
		filename_test  = 'clean_test_' + news_field + '_cfgprod.csv'		
	else:
		filename_train = 'clean_train_'+ news_field + '_'  
		filename_test  = 'clean_test_' + news_field + '.csv'

	file_test_out  = open(directory + filename_test, 'w')			
	writer_test    = writer(file_test_out, delimiter = ',', quoting=QUOTE_ALL)
	rows_test = []
	Y_train = []	
	for i in range(len(Y)):
		if(sample % 50 == 0):
			print('-->', sample, strftime("%I:%M%p"))
			print(len(Y_train))
		X_test, Y_test  =  X[i], Y[i]

		if(i == 0):
			X_train, Y_train = X[i+1:], Y[i+1:]
		elif(i == len(Y)-1):
			X_train, Y_train = X[0:i], Y[0:i]
		else:
			X_train, Y_train = X[0:i] + X[i+1:], Y[0:i] + Y[i+1:]


		file_train_out = open(directory + filename_train+ str(sample) + '.csv', 'w')	
		writer_train   = writer(file_train_out, delimiter = ',', quoting=QUOTE_ALL)


		for i in range(len(Y_train)):
			writer_train.writerow(tuple(X_train[i] + [Y_train[i]]))
		rows_test.append(X_test + [Y_test])	

		file_train_out.close()
		sample += 1

	for i in range(len(rows_test)):
		if(i % 50 == 0):
			print(i,rows_test[i],len(rows_test))
		writer_test.writerow(tuple(rows_test[i]))

	file_test_out.close()  				


def get_cfg_productions(X):
	directory = './stanford-parser-full-2014-08-27/'
	english_parser = StanfordParser(directory+'stanford-parser.jar',\
	     directory+'stanford-parser-3.4.1-models.jar')    
	
	sintax_trees = []
	sample = 0
	for item in X:
		if(sample % 10 == 0):
			print(sample)
		text = item[0]
		sentences = sent_tokenize(text)
		clean_sentences = []
		for i in sentences:
			text = sub(r'[^\w\s]',' ', i).strip() #remove punct    
			clean_sentences.append(text)
		try:
			parser = english_parser.raw_parse_sents(clean_sentences)
			parser = list(parser)
			cfg_productions = ''
		except:
			print(text)
			print(clean_sentences)

		for sent in parser:
			sent = list(sent)
			for j in sent:
				cfg = Tree.fromstring(str(j))
				cfg_productions += transform_into_tokens(cfg.productions())

		sintax_trees.append([cfg_productions])
		sample += 1
	return sintax_trees

def get_postag(X):	
	X_postag = []
	for item in X:
		text = item[0]
		tokens = tokenizer(text)
		X_postag.append(part_of_speech_tagger(tokens))	

	return X_postag

def get_readability(X):
	X_readaby = []
	for item in X:
		text = item[0]
		X_readaby.append([characters_stat(text), complexWords_stat(text), longWords_stat(text),
					numberSyllables_stat(text), lexicon_count_stat(text), sentence_count_stat(text),
					flesch_reading_ease_stat(text), smog_index_stat(text), flesch_kincaid_grade_stat(text),
					coleman_liau_index_stat(text), automated_readability_index_stat(text),
					difficult_words_stat(text), linsear_write_formula_stat(text), gunning_fog_stat(text)]) 	
	return X_readaby


def store_folds(directory, X, Y):

	news_field = 'HEADLINE'
	X_h3clean, X_h2clean, X_h1clean, Y_hclean  = data_cleaning(directory, X, Y, news_field, ['stopwords','digits','punct'])

	# X_readaby = get_readability(X_h1clean)
	# X_postag = get_postag(X_h2clean)


	# X = []
	# print(len(X_readaby[0]),len(X_postag[0]), len(X_h3clean[0]))
	# for i in range(len(Y_hclean)):

	# 	X.append(X_h3clean[i] + X_readaby[i] + X_postag[i])

	# print('data cleaning finished')
	# split_leaveone_out(X, Y_hclean, news_field, None)
	
	X_cfg = get_cfg_productions(X_h1clean)
	print('cfg_finished', strftime("%I:%M%p"))
	print(len(X_cfg))
	# for i in X_cfg:
	# 	print(len(i))

	split_leaveone_out(X_cfg, Y_hclean, news_field, 'cfg')
	
	# news_field = 'CONTENT'
	# X_c3clean, X_c2clean, X_c1clean, Y_cclean  = data_cleaning(directory, X, Y, news_field, ['stopwords','digits','punct'])

	# X_readaby = get_readability(X_c1clean)
	# X_postag = get_postag(X_c2clean)

	# print(len(X_readaby[0]), len(X_postag[0]), len(X_c3clean[0]))
	# X = []
	# for i in range(len(Y_cclean)):
	# 	X.append(X_c3clean[i] + X_readaby[i] + X_postag[i])

	# print('data cleaning finished')
	# split_leaveone_out(X, Y_cclean, news_field, None)
	#560 problematic
	# X_cfg = get_cfg_productions(X_c1clean)
	# print('cfg_finished', strftime("%I:%M%p"))
	# split_leaveone_out(X_cfg, Y_cclean, news_field, 'cfg')

	return True


def transform_into_tokens(sentence_list):
    text = ""
    for index, sent in enumerate(sentence_list):
        parent = str(sent)
        if(parent[-1] == "'"):
            gparent = str(sentence_list[index-1]).split('->')[0].strip()            
            parent = parent.replace("'",'')
            text += "'" + gparent + '^' + parent + "'" + ' '
        
    return text.strip()


def calculate_sintax_trees(X_train, X_test):
    
    pattern = "\'.*?\'"
    vect_train = TfidfVectorizer(use_idf = True, sublinear_tf = False, token_pattern = pattern)
    X_train = vect_train.fit_transform(X_train)
    print(len(vect_train.vocabulary_.keys()))
    X_test  = vect_train.transform(X_test)

    return X_train.toarray().tolist(), X_test.toarray().tolist()	


def calculate_sintax_trees_hjsd(X_train, Y_train, X_test,setup):
	train_vector, test_vector = [], []
	vect_fake, vect_msm = [], []
	FAKE, REAL = 0, 1

	#spliting per class
	for i in range(len(Y_train)): 
		if Y_train[i] == FAKE:
			vect_fake.append(X_train[i])
		else:
			vect_msm.append(X_train[i])   

	size_fake_voc, size_msm_voc = len(vect_fake[0]), len(vect_msm[0])
	vect_fake, vect_msm = asarray(vect_fake), asarray(vect_msm)
	#calculating reference histogram
	fake_href = build_reference_histogram(vect_fake)
	msm_href  = build_reference_histogram(vect_msm)
    
	#calculating features
	for doc in X_train:
		fake_hind = build_individual_histogram(vect_fake, None, doc,setup)
		msm_hind  = build_individual_histogram(vect_msm, None, doc,setup)
		H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
		H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
		JSD_fake = calculate_jsd(fake_href, fake_hind)
		JSD_msm  = calculate_jsd(msm_href,  msm_hind)

		train_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])

	for doc_test in X_test:
		fake_hind = build_individual_histogram(vect_fake, None, doc_test,setup)
		msm_hind  = build_individual_histogram(vect_msm, None, doc_test,setup)
		H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
		H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
		JSD_fake = calculate_jsd(fake_href, fake_hind)
		JSD_msm  = calculate_jsd(msm_href,  msm_hind)

		test_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])

	return train_vector, test_vector

def build_fvector_sintax(X_train, X_test, dataset, news_field, setup, fold):
	if('CFG' in setup):
		train_vector, test_vector = calculate_sintax_trees(X_train, X_test)
	else:
		X_train, Y_train, X_test, Y_test = [], [], [], []
		directory = 'datasets/' + dataset +'/train_test_folds/' + news_field + '/'
		
		filename_train = news_field + '_CFG_' + 'train_fold_' + str(fold + 1) + '.csv'
		filename_test  = news_field + '_CFG_' + 'test_fold_'  + str(fold + 1) + '.csv'		
		with open(directory + filename_train) as file_train,\
			open(directory + filename_test) as file_test:
			reader_train = reader(file_train, delimiter = ',', quoting=QUOTE_ALL)
			reader_test = reader(file_test, delimiter = ',', quoting=QUOTE_ALL)
			for doc in reader_train:
				float_list = []
				for i in range(len(doc)-1):
					float_list.append(float(doc[i]))
				X_train.append(float_list)
				Y_train.append(int(doc[-1]))
			for doc in reader_test:
				float_list = []				
				for i in range(len(doc)-1):
					float_list.append(float(doc[i]))
				X_test.append(float_list)
				Y_test.append(int(doc[-1]))
			
			train_vector, test_vector  = calculate_sintax_trees_hjsd(X_train, Y_train, X_test,setup)


	return train_vector, test_vector


def extract_store_feature(X_train, Y_train, X_test, Y_test, directory, news_field, setup, n):
	X_train_fold, X_test_fold = [], []
	directory += news_field+'/' 

	filename_train = news_field + '_' + setup[0]
	filename_test  = news_field + '_' + setup[0]

	if('TF' in setup or 'TF-IDF' in setup):
		filename_train += setup[1] 
		filename_test  += setup[1] 
		X_train_fold, X_test_fold = build_fvector_words(X_train, Y_train, X_test, setup)

	if('POSTAG' in setup or 'POSTAG-H+JSD' in setup):
		X_train_fold, X_test_fold = build_fvector_postag(X_train, Y_train, X_test, setup)

	if('READABILITY' in setup):
		X_train_fold, X_test_fold = build_fvector_readaby(X_train, X_test)
	
	if('CFG' in setup or 'CFG-H+JSD' in setup):
		X_train_fold, X_test_fold = build_fvector_sintax(X_train, X_test, dataset, news_field, setup, n)			

	filename_train += '_train_fold_' + str(n) + '.csv'
	filename_test  += '_test_fold_'  + str(n) + '.csv'
	#file_train_out = open(directory + filename_train, 'w')
	file_test_out  = open(directory + filename_test, 'w')		
	#writer_train   = writer(file_train_out, delimiter = ',', quoting=QUOTE_ALL)
	writer_test    = writer(file_test_out, delimiter = ',', quoting=QUOTE_ALL)
	#print(filename_train)

	# for i in range(len(Y_train)):
	# 	writer_train.writerow(tuple(X_train_fold[i] + [Y_train[i]]))

	for i in range(len(Y_test)):
		writer_test.writerow(tuple(X_test_fold[i]+[Y_test[i]]))			

	#file_train_out.close()
	file_test_out.close()


def loading_liwc(dataset, news_field):
	X_train, X_test = [], []
	field = 0 if (news_field == 'HEADLINE') else 1
	directory = 'datasets/' + dataset +'/train_test_folds/' + news_field + '/'
	for fold in range(0, 10):
		filename_train = news_field + '_LIWC_' + 'train_fold_' + str(fold + 1) + '.csv'
		filename_test  = news_field + '_LIWC_' + 'test_fold_'  + str(fold + 1) + '.csv'		
		X =  []
		with open(directory + filename_train) as file_train,\
			open(directory + filename_test) as file_test:			
			reader_train = reader(file_train, delimiter = ',', quoting=QUOTE_ALL)
			reader_test = reader(file_test, delimiter = ',', quoting=QUOTE_ALL)
			next(reader_train)
			next(reader_test)
			for doc in reader_train:
				float_list = []
				text = doc[field]
				text = sub(r'[^\w\s]',' ', text).strip()    
				text = sub(r'[^\D]',' ',text).strip()
				if(text != ''):
					for i in range(3,len(doc)):
						float_list.append(float(doc[i]))
					X.append(float_list)
			X_train.append(X)
			X = []
			for doc in reader_test:
				float_list = []
				text = doc[field]
				text = sub(r'[^\w\s]',' ', text).strip()    
				text = sub(r'[^\D]',' ',text).strip()
				if(text != ''):
					for i in range(3,len(doc)):
						float_list.append(float(doc[i]))
					X.append(float_list)
			X_test.append(X)

	return X_train, X_test



def loading_features(dataset, news_field, feature, n):
	X_train, Y_train, X_test, Y_test = [], [], [], []
	directory = 'datasets/' + dataset +'/train_test_folds/' + news_field + '/'
	filename_train = news_field + '_' + feature + '_' + 'train_fold_' + str(n) + '.csv'
	filename_test  = news_field + '_' + feature + '_' + 'test_fold_'  + str(n) + '.csv'		
	with open(directory + filename_train) as file_train,\
		open(directory + filename_test) as file_test:
		
		reader_train = reader(file_train, delimiter = ',', quoting=QUOTE_ALL)
		reader_test = reader(file_test, delimiter = ',', quoting=QUOTE_ALL)
		for doc in reader_train:
			float_list = []
			for i in range(len(doc)-1):
				float_list.append(float(doc[i]))
			X_train.append(float_list)

		for doc in reader_test:
			float_list = []				
			for i in range(len(doc)-1):
				float_list.append(float(doc[i]))
			X_test.append(float_list)

	return X_train, X_test

def classification(X_train, Y_train, X_test, Y_test, algorithm):

	svm_rbf_res, svc_linear_res, mnb_res = [], [], []
	knn_res, rf_res, ground_truth = [], [], []
	train_time, test_time = 0.0, 0.0
	classifier = None

	if(algorithm   == 'SVM'):
		classifier = svm.SVC(kernel= 'linear')

	elif(algorithm == 'KNN'):
		classifier = KNeighborsClassifier(n_neighbors=5)

	elif(algorithm == 'RNF'):
		classifier = RandomForestClassifier(max_depth=None, random_state=0)

	elif(algorithm == 'MNB'):
		classifier = GaussianNB()#MultinomialNB()
	
	time_then = time()
	classifier.fit(X_train, Y_train)
	time_now = time()
	train_time = time_now - time_then

	time_then = time()
	prediction = classifier.predict(X_test)
	time_now = time()
	test_time = time_now - time_then



	return prediction, train_time, test_time

def check_directory(path):
	files = listdir('./' + path)
	for i in files:
		if(i.endswith('.csv')):
			return False
	return True

def store_results(dataset, news_field, predictions, ground_truths, time_train,\
					 time_test, features_list, classifier):	
	directory = 'datasets/' + dataset +'/results/' + news_field + '/'
	feature = ''

	for i in features_list:
		feature += '_' + i		
	
	filename = news_field + feature + '_' + classifier + '_result.csv'
	file_out = open(directory+filename, 'w')
	writer_out = writer(file_out, delimiter = ',', quoting=QUOTE_ALL)

	writer_out.writerow(tuple(predictions))
	writer_out.writerow(tuple(ground_truths))

	writer_out.writerow(tuple(time_train))
	writer_out.writerow(tuple(time_test))			
	
	file_out.close()
	print(filename+' stored!')


def loading_ngrams(docs_train, docs_test):
	X_train, X_test = [], []
	vect_train = TfidfVectorizer(use_idf=True, sublinear_tf=False, ngram_range = (1,2))
	
	for i in docs_train:
		X_train.append(i[0])

	X_train_fold = vect_train.fit_transform(X_train).toarray().tolist()
	#print(docs_test)
	X_test_fold  = vect_train.transform([docs_test[0]]).toarray().tolist()


	return X_train_fold, X_test_fold



def model_evaluation(directory, news_field, params, sample_size,lim):	
	predictions_svm, predictions_knn  = [], []
	predictions_mnb,  predictions_rnf= [], []
	ground_truth = []

	train_time_list_svm, test_time_list_svm = [], []
	train_time_list_knn, test_time_list_knn = [], []
	train_time_list_mnb, test_time_list_mnb = [], []
	train_time_list_rnf, test_time_list_rnf = [], []
	
	#headline, content = 484, 500 celebrity
	ini = lim-100
	for n in range(1, 420):
		if(n % 100 == 0):
			print(n,strftime("%I:%M%p"))
		X_train, Y_train, X_test, Y_test = setup_features(directory, news_field, params, n)

		ground_truth.append(Y_test[0])

		# pred, time_train, time_test = classification(X_train, Y_train, X_test, Y_test, 'SVM')		
		# predictions_svm.append(pred[0])
		# train_time_list_svm.append(time_train)
		# test_time_list_svm.append(time_test)		
		
		pred, time_train, time_test = classification(X_train, Y_train, X_test, Y_test, 'KNN')		
		predictions_knn.append(pred[0])
		train_time_list_knn.append(time_train)
		test_time_list_knn.append(time_test)

		pred, time_train, time_test = classification(X_train, Y_train, X_test, Y_test, 'MNB')		
		predictions_mnb.append(pred[0])
		train_time_list_mnb.append(time_train)
		test_time_list_mnb.append(time_test)

		pred, time_train, time_test = classification(X_train, Y_train, X_test, Y_test, 'RNF')		
		predictions_rnf.append(pred[0])
		train_time_list_rnf.append(time_train)
		test_time_list_rnf.append(time_test)

	store_results(dataset, news_field, predictions_rnf, ground_truth,\
			train_time_list_rnf, test_time_list_rnf, params,'RNF')

	store_results(dataset, news_field, predictions_mnb, ground_truth,\
			train_time_list_mnb, test_time_list_mnb, params,'MNB')

	# store_results(dataset, news_field, predictions_svm, ground_truth,\
	# 		train_time_list_svm, test_time_list_svm, params,'SVM')

	store_results(dataset, news_field, predictions_knn, ground_truth,\
			train_time_list_knn, test_time_list_knn, params, 'KNN')

	# print('SVM -- results')
	# recall = recall_score(ground_truth, predictions_svm, average=None)
	# precision = precision_score(ground_truth, predictions_svm, average=None)    
	# f1 = f1_score(ground_truth, predictions_svm, average=None)
	# accuracy = accuracy_score(ground_truth, predictions_svm)

	# text_precision = str(round(precision[0],2))+'\t'+str(round(precision[1],2))
	# text_recall = str(round(recall[0],2))+'\t'+str(round(recall[1],2))
	# text_f1 = str(round(f1[0],2))+'\t'+str(round(f1[1],2))


	#print('KNN -- results')
	#print(ground_truth, predictions_knn)
	recall = recall_score(ground_truth, predictions_knn, average=None)
	precision = precision_score(ground_truth, predictions_knn, average=None)    
	f1 = f1_score(ground_truth, predictions_knn, average=None)
	accuracy = accuracy_score(ground_truth, predictions_knn)
	print(recall)
	print(precision)
	print(f1)

	text_precision = '\t' + str(round(precision[0],2))+'\t'+str(round(precision[1],2))
	text_recall = '\t' + str(round(recall[0],2))+'\t'+str(round(recall[1],2))
	text_f1 = '\t' + str(round(f1[0],2))+'\t'+str(round(f1[1],2))

	#print('MNB -- results')
	recall = recall_score(ground_truth, predictions_mnb, average=None)
	precision = precision_score(ground_truth, predictions_mnb, average=None)    
	f1 = f1_score(ground_truth, predictions_mnb, average=None)
	accuracy = accuracy_score(ground_truth, predictions_mnb)

	text_precision += '\t' + str(round(precision[0],2))+'\t'+str(round(precision[1],2))
	text_recall += '\t' + str(round(recall[0],2))+'\t'+str(round(recall[1],2))
	text_f1 += '\t' + str(round(f1[0],2))+'\t'+str(round(f1[1],2))

	#print('RNF -- results')
	recall = recall_score(ground_truth, predictions_rnf, average=None)
	precision = precision_score(ground_truth, predictions_rnf, average=None)    
	f1 = f1_score(ground_truth, predictions_rnf, average=None)
	accuracy = accuracy_score(ground_truth, predictions_rnf)

	text_precision += '\t' + str(round(precision[0],2))+'\t'+str(round(precision[1],2))
	text_recall += '\t' + str(round(recall[0],2))+'\t'+str(round(recall[1],2))
	text_f1 += '\t' + str(round(f1[0],2))+'\t'+str(round(f1[1],2))

	print(text_recall)
	print(text_precision)
	print(text_f1)




def get_features(interval, X_train, X_test):
	train, test = [], []
	a,b = interval

	for i in X_train:
		train.append(i[a:b])

	test  = X_test[a:b]

	return train, test

def setup_features(directory, news_field, features_list,n):
	X_train_ngrams, Y_train_ngrams, X_test_ngrams, Y_test_ngrams = [], [], [], []
	X_train_postag, Y_train_postag, X_test_postag, Y_test_postag = [], [], [], []
	X_train_readby, Y_train_readby, X_test_readby, Y_test_readby = [], [], [], []
	X_train_cfg, Y_train_cfg, X_test_cfg, Y_test_cfg, X_train_liwc, X_test_liwc  = [], [], [], [], [], []

	X_train, Y_train, X_test, Y_test = read_traintest_folds(directory,\
		news_field, n)
	
	for feature in features_list:
		if('NGRAMS'  == feature):
			X_train_ngrams, X_test_ngrams  = loading_ngrams(X_train,X_test)	
		
		elif(feature.startswith('TF')):
			X_train_ngrams, X_test_ngrams  = loading_features(dataset, news_field, feature,n)

		elif('POSTAG' == feature):

			X_train_postag, X_test_postag = get_features((201,len(X_train[0])), X_train, X_test)    
		
		elif('LIWC' == feature):
			if(news_field == 'HEADLINE'):		
				X_train_liwc, X_test_liwc  = get_features((1,94), X_train, X_test)
			else:
				X_train_liwc, X_test_liwc  = get_features((94,188), X_train, X_test)

		elif('READABILITY' == feature):
			X_train_readby, X_test_readby = get_features((187,201), X_train, X_test)				

		else:
			X_train_cfg, X_test_cfg  = loading_features(dataset, news_field, feature,n)	

	#print(n,len(X_train_postag), len(X_train_liwc), len(X_train_cfg), len(X_train_readby))
	# print(len(X_train_postag[0]), len(X_train_liwc[0]), len(X_train_cfg[0]), len(X_train_readby[0]))

	X_train = combine_features(X_train_ngrams, X_train_postag, X_train_readby, X_train_liwc, X_train_cfg, Y_train)
	X_test = [X_test_ngrams[0] + X_test_postag + X_test_readby + X_test_liwc ]#+ X_test_cfg[0]] 

	return X_train, Y_train, X_test, Y_test 

if __name__ == '__main__':
	dataset = argv[1]
	news_field = argv[2].upper().strip()
	#lim = int(argv[3])
	#params = argv[3].upper().split('|')
	#print(params)

	directory = 'datasets/' + dataset +'/train_test_folds/'

	#Storing folds
	if(dataset.upper().strip() == 'SILVERMAN'):
		if(check_directory(directory)):
			print('Storing folds')
			fake_docs  = read_dataset(dataset, 'fake')
			real_docs  = read_dataset(dataset, 'real')
			
			X = fake_docs + real_docs
			Y = ['0' for i in range(len(fake_docs))] +\
			    ['1' for i in range(len(real_docs))]		
			
			store_folds(directory, X, Y)

	elif(dataset.upper().strip() == 'CELEBRITY'):
		if(check_directory(directory)):
			print('Storing folds')
			fake_docs  = read_dataset(dataset, 'fake')
			real_docs  = read_dataset(dataset, 'real')
			
			X = fake_docs + real_docs
			Y = ['0' for i in range(len(fake_docs))] +\
			    ['1' for i in range(len(real_docs))]		
			print('|',strftime("%I:%M%p"),'|')
			print(len(X), len(Y))
			store_folds(directory, X, Y)
	elif(dataset.upper().strip() == 'NEWSNET'):
		if(check_directory(directory)):
			print('Storing folds')
			fake_docs  = read_dataset(dataset, 'fake')
			real_docs  = read_dataset(dataset, 'real')
			
			X = fake_docs + real_docs
			Y = ['0' for i in range(len(fake_docs))] +\
			    ['1' for i in range(len(real_docs))]		
			print('|',strftime("%I:%M%p"),'|')
			print(len(X), len(Y))
			store_folds(directory, X, Y)


	#################################### Saving features ####################################


	
	params_list = [['TF','(1,1)'], ['TF','(2,2)'], ['TF','(1,2)'],
			  ['TF-IDF','(1,1)'], ['TF-IDF','(2,2)'], ['TF-IDF','(1,2)']]
	for params in [['CFG']]:
		print(params, strftime("%I:%M%p"))
		range_size = 0
		if(directory.find('celebrity') >=0 and news_field == 'HEADLINE'):
			range_size = 483
		elif(directory.find('celebrity')>=0 and news_field == 'CONTENT'):
			range_size = 499
		elif(directory.find('newsnet')>=0 and news_field == 'HEADLINE'):
			range_size = 422
		elif(directory.find('newsnet')>=0 and news_field == 'CONTENT'):	
			range_size = 400


		for n in range(0, range_size):	
			if (n % 100 == 0):
				print(n,strftime("%I:%M%p"))

			X_train, Y_train, X_test, Y_test = read_traintest_folds(directory,\
				news_field, n,'CFG')

			extract_store_feature(X_train, Y_train, X_test, Y_test, directory, news_field, params,n)


	
	# params = ['TF(1,1)']

	#################################### Classification ####################################


	features_list = [['TF(1,1)'], ['TF(2,2)'],['TF(1,2)'],['TF-IDF(1,1)'], ['TF-IDF(2,2)'],['TF-IDF(1,2)'],
 					['POSTAG'], ['READABILITY'], ['CFG'], ['LIWC'],
 					['TF(1,1)', 'POSTAG'],['TF(1,1)', 'READABILITY'], ['TF(1,1)', 'CFG'], ['TF(1,1)', 'LIWC'],
 					['TF(2,2)', 'POSTAG'], ['TF(2,2)', 'READABILITY'], ['TF(2,2)', 'CFG'], ['TF(2,2)', 'LIWC'],					
 					['TF(1,2)', 'POSTAG'], ['TF(1,2)', 'READABILITY'], ['TF(1,2)', 'CFG'], ['TF(1,2)', 'LIWC'],
 					['TF-IDF(1,1)', 'POSTAG'],['TF-IDF(1,1)', 'READABILITY'], ['TF-IDF(1,1)', 'CFG'], ['TF-IDF(1,1)', 'LIWC'],
 					['TF-IDF(2,2)', 'POSTAG'], ['TF-IDF(2,2)', 'READABILITY'], ['TF-IDF(2,2)', 'CFG'], ['TF-IDF(2,2)', 'LIWC'],					
 					['TF-IDF(1,2)', 'POSTAG'], ['TF-IDF(1,2)', 'READABILITY'], ['TF-IDF(1,2)', 'CFG'], ['TF(-IDF1,2)', 'LIWC'],

					['TF-IDF(1,1)', 'POSTAG','READABILITY'], ['TF-IDF(1,1)', 'POSTAG','CFG'],
					['TF-IDF(1,1)', 'POSTAG','LIWC'], ['TF-IDF(1,1)', 'READABILITY','CFG'],
					['TF-IDF(1,1)', 'READABILITY','LIWC'], ['TF-IDF(1,1)', 'CFG','LIWC'],
					['TF-IDF(1,1)', 'CFG','LIWC','POSTAG','READABILITY'],

					['TF-IDF(2,2)', 'POSTAG','READABILITY'], ['TF-IDF(2,2)', 'POSTAG','CFG'],
					['TF-IDF(2,2)', 'POSTAG','LIWC'], ['TF-IDF(2,2)', 'READABILITY','CFG'],
					['TF-IDF(2,2)', 'READABILITY','LIWC'], ['TF-IDF(2,2)', 'CFG','LIWC'],
					['TF-IDF(2,2)', 'CFG','LIWC','POSTAG','READABILITY'],

					['TF-IDF(1,2)', 'POSTAG','READABILITY'], ['TF-IDF(1,2)', 'POSTAG','CFG'],
					['TF-IDF(1,2)', 'POSTAG','LIWC'], ['TF-IDF(1,2)', 'READABILITY','CFG'],
					['TF-IDF(1,2)', 'READABILITY','LIWC'], ['TF-IDF(1,2)', 'CFG','LIWC'],
					['TF-IDF(1,2)', 'CFG','LIWC','POSTAG','READABILITY'],

					['TF(1,1)', 'POSTAG','READABILITY'], ['TF(1,1)', 'POSTAG','CFG'],
					['TF(1,1)', 'POSTAG','LIWC'], ['TF(1,1)', 'READABILITY','CFG'],
					['TF(1,1)', 'READABILITY','LIWC'], ['TF(1,1)', 'CFG','LIWC'],
					['TF(1,1)', 'CFG','LIWC','POSTAG','READABILITY'],

					['TF(2,2)', 'POSTAG','READABILITY'], ['TF(2,2)', 'POSTAG','CFG'],
					['TF(2,2)', 'POSTAG','LIWC'], ['TF(2,2)', 'READABILITY','CFG'],
					['TF(2,2)', 'READABILITY','LIWC'], ['TF(2,2)', 'CFG','LIWC'],					
					['TF(2,2)', 'CFG','LIWC','POSTAG','READABILITY'],

					['TF(1,2)', 'POSTAG','READABILITY'], ['TF(1,2)', 'POSTAG','CFG'],
					['TF(1,2)', 'POSTAG','LIWC'], ['TF(1,2)', 'READABILITY','CFG'],
					['TF(1,2)', 'READABILITY','LIWC'], ['TF(1,2)', 'CFG','LIWC'],
					['TF(1,2)', 'CFG','LIWC','POSTAG','READABILITY']]


	features_list = ['TF(1,2)']
	# print(features_list, strftime("%I:%M%p"))
	# model_evaluation(directory, news_field, features_list, 500,lim)





	# log_results = ''

	# print('+++++++++++++++++++ READING FOLDS +++++++++++++++++++')
	# doc_train, lbl_train, doc_test, lbl_test = read_traintest_folds(directory, news_field)

	# print('+++++++++++++++++++ DATA CLEANING +++++++++++++++++++')
	# X_train, Y_train = data_cleaning(doc_train, lbl_train,  ['stopwords','digits','punct'])
	# X_test, Y_test   = data_cleaning(doc_test, lbl_test, ['stopwords','digits','punct'])


	# features_list = [['TF(1,2)','READABILITY']]

	# features_list = [['TF(1,1)'], ['TF(2,2)'],['TF(1,2)'],['TF-IDF(1,1)'], ['TF-IDF(2,2)'],['TF-IDF(1,2)'],
 # 					['POSTAG'], ['READABILITY'], ['CFG-H+JSD'],
 # 					['TF(1,2)', 'POSTAG'],
 # 					['TF(1,2)', 'READABILITY'],
 # 					['TF(1,2)', 'CFG-H+JSD'],
 # 					['TF-IDF(1,2)', 'POSTAG'],
 # 					['TF-IDF(1,2)', 'READABILITY'],
 # 					['TF-IDF(1,2)', 'CFG-H+JSD'], 					
	# 				['TF(1,2)', 'READABILITY','POSTAG', 'CFG-H+JSD'],
	# 				['TF-IDF(1,2)', 'READABILITY','POSTAG', 'CFG-H+JSD']]

	# features_list = [['CFG'],
	# 				['TF-IDF(1,2)', 'POSTAG','READABILITY'],
	# 				['TF-IDF(1,2)', 'POSTAG','CFG'],
	# 				['TF-IDF(1,2)', 'POSTAG','LIWC'],
	# 				['TF-IDF(1,2)', 'POSTAG','CFG-H+JSD'],					
	# 				['TF-IDF(1,2)', 'READABILITY','CFG'],
	# 				['TF-IDF(1,2)', 'READABILITY','LIWC'],
	# 				['TF-IDF(1,2)', 'READABILITY','CFG-H+JSD'],					
	# 				['TF-IDF(1,2)', 'CFG','LIWC'],
	# 				['TF-IDF(1,2)', 'LIWC','CFG-H+JSD'],
	# 				['TF(1,2)', 'POSTAG','READABILITY'],
	# 				['TF(1,2)', 'POSTAG','CFG'],
	# 				['TF(1,2)', 'POSTAG','LIWC'],
	# 				['TF(1,2)', 'POSTAG','CFG-H+JSD'],					
	# 				['TF(1,2)', 'READABILITY','CFG'],
	# 				['TF(1,2)', 'READABILITY','LIWC'],
	# 				['TF(1,2)', 'READABILITY','CFG-H+JSD'],					
	# 				['TF(1,2)', 'CFG','LIWC'],
	# 				['TF(1,2)', 'LIWC','CFG-H+JSD']]	
	#features_list = [['TF(1,2)', 'POSTAG','READABILITY','CFG-H+JSD','LIWC']]

	# print('+++++++++++++++++++ CLASSIFICATION +++++++++++++++++++')
	# for params in features_list:
	# 	print('Setup:', params, strftime("%I:%M%p"))
	# 	X_train_final, X_test_final = setup_features(dataset, news_field, doc_train, Y_train, doc_test, Y_test , params)
	# 	#fake_class, real_class = classification(X_train_final, lbl_train, X_test_final, lbl_test)
	# 	fake_class, real_class = classification(X_train_final, Y_train, X_test_final, Y_test)
	# 	store_results(dataset, fake_class, real_class, params)
		
	# 	log_results += fake_class+'\n'
	# 	log_results += real_class+'\n'
	# print(log_results)	

	##BASELINE - MICHIGAN


	# print('Reading folds...')
	# doc_train, lbl_train, doc_test, lbl_test = read_traintest_folds(directory, news_field)

	# print('\nBASELINE',strftime("%I:%M%p"))
	# features_list = ['NGRAMS', 'CFG', 'LIWC', 'READABILITY']
	# print('Setup:', features_list)
	# X_train, Y_train = data_cleaning(doc_train, lbl_train, ['digits','punct'])
	#X_test, Y_test   = data_cleaning(doc_test, lbl_test, ['digits','punct'])
	#doc_train, lbl_train, doc_test, lbl_test = None, None,None, None

	# X_train, X_test  = setup_features(dataset, news_field, X_train, Y_train, X_test, Y_test, features_list)
	# print('+++++++++++++++++++ CLASSIFICATION +++++++++++++++++++')
	
	# fake_class, real_class = classification(X_train, Y_train, X_test, Y_test)


	# print(fake_class)
	# print(real_class)


















