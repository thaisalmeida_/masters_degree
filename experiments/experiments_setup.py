from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from numpy import asarray, sum, arange, array,empty
from numpy import zeros, log2, nditer, isnan
from numpy import asarray, sum, arange, nditer, isnan
from sklearn.model_selection import LeaveOneOut
from sklearn.naive_bayes import MultinomialNB, GaussianNB
from sklearn import svm, linear_model
from csv import reader,writer,QUOTE_ALL
from csv import field_size_limit
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import MinMaxScaler

from re import sub, findall, match, MULTILINE
from textstat.textstat import textstat
from nltk.parse.stanford import StanfordParser
from nltk.tokenize import sent_tokenize
from nltk import Tree
from sklearn import tree
from sys import maxsize,argv


from sklearn.decomposition import PCA
from linguistic_module import *
from math import log
from os import listdir
from time import strftime,time
from json import loads
import sys

def data_cleaning(directory, data, labels, news_field, params):
	list_docs_3, labels_docs = [], []
	list_docs_2, list_docs_1 = [], []

	for i in range(len(data)):
		if(news_field == 'HEADLINE'):
			text_field = data[i][0]
		else:
			text_field = data[i][1]

		aux1, aux2 = [], []
		if('punct' in params):
			text = sub(r'[^\w\s]',' ', text_field).strip()    
		if('digits' in params):
			text = sub(r'[^\D]',' ',text).strip()
			aux1 = [text] + data[i][3:] #punct + digits
			aux2 = [sub(r'[^\D]',' ',text_field).strip()] + data[i][3:] #digits    			
		if('stopwords' in params):
			text = tokenizer(text)
			text = remove_stopwords_corpus(text)
			text = ' '.join(text)

		if(text != ''):
			list_docs_3.append([text] + data[i][3:])
			list_docs_2.append(aux1)
			list_docs_1.append(aux2)						
			labels_docs.append(labels[i])

	
	return list_docs_3, list_docs_2, list_docs_1, labels_docs 
def get_postag(X):	
	X_postag = []
	for item in X:
		text = item[0]
		tokens = tokenizer(text)
		X_postag.append(part_of_speech_tagger(tokens))	

	return X_postag

def get_readability(X):
	X_readaby = []
	for item in X:
		text = item[0]
		X_readaby.append([characters_stat(text), complexWords_stat(text), longWords_stat(text),
					numberSyllables_stat(text), lexicon_count_stat(text), sentence_count_stat(text),
					flesch_reading_ease_stat(text), smog_index_stat(text), flesch_kincaid_grade_stat(text),
					coleman_liau_index_stat(text), automated_readability_index_stat(text),
					difficult_words_stat(text), linsear_write_formula_stat(text), gunning_fog_stat(text)]) 	
	return X_readaby

def get_cfg_productions(X):
	directory = './stanford-parser-full-2014-08-27/'
	english_parser = StanfordParser(directory+'stanford-parser.jar',\
	     directory+'stanford-parser-3.4.1-models.jar')    
	
	sintax_trees = []
	sample = 0
	for item in X:
		if(sample % 10 == 0):
			print(sample)
		text = item[0]
		sentences = sent_tokenize(text)
		clean_sentences = []
		for i in sentences:
			text = sub(r'[^\w\s]',' ', i).strip() #remove punct    
			clean_sentences.append(text)
		try:
			parser = english_parser.raw_parse_sents(clean_sentences)
			parser = list(parser)
			cfg_productions = ''
		except:
			print(text)
			print(clean_sentences)

		for sent in parser:
			sent = list(sent)
			for j in sent:
				cfg = Tree.fromstring(str(j))
				cfg_productions += transform_into_tokens(cfg.productions())

		sintax_trees.append([cfg_productions])
		sample += 1
	return sintax_trees
def characters_stat(text):
    try:
        return character_count(text)
    except ValueError:
        return 0
     
def complexWords_stat(text):
    try:
        return textstat.dale_chall_readability_score(text)
    except ValueError:
        return 0
    
def longWords_stat(text):
    try:
        return avg_sentence_length(text)
    except ValueError:
        return 0
    
def numberSyllables_stat(text):
    try:
        return textstat.syllable_count(text, lang='en_US')
    except ValueError:
        return 0
    
def lexicon_count_stat(text):
    try:
        return textstat.lexicon_count(text, removepunct=True)
    except ValueError:
        return 0

def sentence_count_stat(text):
    try:
        return textstat.sentence_count(text)
    except ValueError:
        return 0
    

def flesch_reading_ease_stat(text):
    try:
        return textstat.flesch_reading_ease(text)
    except ValueError:
        pass
        return 0
    
def smog_index_stat(text):
    try:
        return textstat.smog_index(text)
    except ValueError:
        return 0
    
def flesch_kincaid_grade_stat(text):
    try:
        return textstat.flesch_kincaid_grade(text)
    except ValueError:
        return 0
    
def coleman_liau_index_stat(text):
    try:
        return textstat.coleman_liau_index(text)
    except ValueError:
        return 0
    
def automated_readability_index_stat(text):
    try:
        return textstat.automated_readability_index(text)
    except ValueError:
        return 0
    
def difficult_words_stat(text):
    try:
        return textstat.difficult_words(text)
    except ValueError:
        return 0
    
def linsear_write_formula_stat(text):
    try:
        return textstat.linsear_write_formula(text)
    except ValueError:
        return 0
    
def gunning_fog_stat(text):
    try:
        return textstat.gunning_fog(text)
    except ValueError:
        return 0

def store_silverman_files(X , label):

	directory = ''
	label_name = 'fake' if label == 0 else 'real'
	filename = label_name+'news_silverman.csv' 

	file_out = open(directory+filename,'w')
	writer_out = writer(file_out, delimiter = ',', quoting=QUOTE_ALL)
	for doc in X:
		headline, content =  doc[0], doc[1]
		writer_out.writerow(tuple([headline, content, label]))

	file_out.close()  


def read_dataset(dataset, label):
	X = []
	directory = 'datasets/'+ dataset + '/'
	with open(directory+label+'news_'+dataset+'_liwc.csv') as file_in:
		reader_in = reader(file_in, delimiter = ',', quoting=QUOTE_ALL)
		for doc in reader_in:
			X.append(doc)	
	return X						


def read_traintest_folds(directory, news_field,n,cfg=None):
	X_train, Y_train, X_test, Y_test = [], [], [], []
	field_size_limit(maxsize)	

	doc, label = [], []
	if(cfg):
		file_train = 'clean_train_' + news_field + '_cfgprod_' + str(n) + '.csv'
		file_test  = 'clean_test_'  + news_field + '_cfgprod_' + str(n) + '.csv'	
	else:
		file_train = 'clean_train_' + news_field + '_' +  str(n) + '.csv'
		file_test  = 'clean_test_'  + news_field + '_' +  str(n) + '.csv'	

	with open(directory + file_train, "r") as file_train_in,\
		open(directory + file_test, "r") as file_test_in:
		reader_train = reader(file_train_in, delimiter = ',', quoting = QUOTE_ALL)
		reader_test  = reader(file_test_in, delimiter = ',', quoting = QUOTE_ALL)
		for i in reader_train:
			
			if(cfg):
				X_train.append(i[0])								
			else:        		
				aux = [i[0]]
				for j in i[1:-1]:
					aux.append(float(j))
				X_train.append(aux)				
			Y_train.append(int(i[-1]))

		for i in reader_test:	     
			if(cfg):
				X_test.append(i[0])								
			else:        		
				aux = [i[0]]
				for j in i[1:-1]:
					aux.append(float(j))
				X_test.append(aux)				
			Y_test.append(int(i[-1]))
	return X_train, Y_train, X_test, Y_test 

def build_silverman(news_field):    
    #This function returns all the articles versions where 
    #headline and body share the same stance
    #for, ignoring, against, observing check the body text (page 101-103)
    #340 claims -> True 71, False 130, Unknown, 139
    #True news -> 1789, False news -> 1032
    #Total number of lines = 7112
    
    claims, articles, fake_news, real_news = {}, {}, [], []
    HEADLINE, CONTENT, BOTH = 1, 2, 3
    directory = 'datasets_public_available/emergent/'
    filename  = 'url-versions-2015-06-14.csv'
    keys = {'claimId':0, 'claimSlug':1, 'claimHeadline':2, 'claimTruthiness':3, 'articleId':4,
    'articleUrl':5, 'articleVersion':6, 'articleVersionId':7, 'articleHeadline':8, 'articleByline':9,
    'articleStance':10, 'articleHeadlineStance':11, 'articleBody':12} 
    
    with open(directory+filename, "r") as arq_in:
        reader_in = reader(arq_in, delimiter=',', quoting=QUOTE_ALL)
        next(reader_in)
        for t in reader_in:
            key, clstance = t[keys['claimId']], t[keys['claimTruthiness']]
            hstance, bstance = t[keys['articleHeadlineStance']], t[keys['articleStance']]
            if((hstance == bstance) and (hstance in ['for','against','observing'])):
                articles[t[keys['articleVersionId']]] = [t[keys['articleHeadline']],\
                         t[keys['articleBody']], hstance, key]
            if(clstance in ['false','true']):
                claims[key] = clstance

    for news in articles:
        id = articles[news][3]
        if(claims.get(id) != None):
            claims_stance, news_stance = claims[id], articles[news][2]
            if(news_stance == 'for' or news_stance == 'observing'):
                articles[news].append(claims_stance)
            elif(news_stance == 'against'):
                if(claims_stance =='false'):
                    articles[news].append('true')
                else:
                    articles[news].append('false')
            
            if(news_field == HEADLINE):
                text = articles[news][0]
            elif(news_field == CONTENT):
                text = articles[news][1]
            else:
                text = [articles[news][0], articles[news][1]]
            
            if(articles[news][4] == 'true'):
                real_news.append(text)
            else:
                fake_news.append(text)
                    
    return fake_news, real_news
    
def tfidf_average_values(X):
	#sum of tfidf values of a term in all documents.
	nro_docs, nro_terms = X.shape
	tfidf_sum_list = sum(X, axis=0)

	tfidf_avg_val = tfidf_sum_list/float(nro_docs)
	total = tfidf_avg_val.sum()

	return tfidf_avg_val, total


def build_reference_histogram(X):
	tfidf_avg_val, tfidf_sum = tfidf_average_values(X)
	word_avg_prob = zeros((tfidf_avg_val.size,), dtype=float)    

	index = 0
	#improve	
	for tfidf in nditer(tfidf_avg_val):
		if(tfidf_sum > 0):
			word_avg_prob[index] = tfidf/tfidf_sum
		else:
			word_avg_prob[index] = 0.0
		index += 1
	return word_avg_prob

def build_individual_histogram(X, vectorizer, new_doc, setup):
	total = 0.0
	nro_docs, nro_terms = X.shape
	word_prob = zeros((nro_terms,), dtype=float)    	 
	word_prob_pdf = zeros((nro_terms,), dtype=float)    


	if(vectorizer != None):
		X_new = vectorizer.transform([new_doc])
		rows, cols = X_new.nonzero()

		for row, col in zip(rows, cols):
			word_prob[col] = X_new[row,col]/float(nro_docs)
		total = word_prob.sum()

	# else:
	# 	if('CFG-H+JSD' not in setup):
	# 		tokens = tokenizer(new_doc)
	# 		X_new = part_of_speech_tagger(tokens) 
	# 	else:
	# 		X_new = new_doc
	# 	for index in range(nro_terms):
	# 		value = X_new[index]/nro_docs
	# 		word_prob.append(value)
	# 		total += value        

	index = 0		
	for i in nditer(word_prob):    
		if(total > 0):
			word_prob_pdf[index] = i/total
		else:
			word_prob_pdf[index] = 0.0
		index += 1

	return word_prob_pdf         

def entropy(p, base):
    p_nzero  = p[ p != 0 ] 
    p_log = log2(p_nzero)
    product = p_nzero * p_log
    sum_p = product.sum() 
    entropy = ((-1) * sum_p) if sum_p != 0.0 else 0.0        

    return entropy 


def calculate_jsd(p_ref, q_ind, S_ref):
    mean_hist = (p_ref/2) + (q_ind/2)

    jsd = entropy(mean_hist,2) - (S_ref + entropy(q_ind,2))/2

    return jsd       

def build_fvector_postag(X_train, Y_train, docs_test, setup):
	train_vector, test_vector = [], []
	vect_fake, vect_msm = [], []
	fake_train, msm_train = [], []
	FAKE, REAL = 0, 1

	if('POSTAG' in setup):
		for text in X_train:
			tokens = tokenizer(text)
			train_vector.append(part_of_speech_tagger(tokens))

		for text in docs_test:
			tokens = tokenizer(text)
			test_vector.append(part_of_speech_tagger(tokens))      	    	
	else:   
		#spliting per class
		for i in range(len(Y_train)): 
			if Y_train[i] == FAKE:
				fake_train.append(X_train[i])
			else:
				msm_train.append(X_train[i])

		#building the postag matrix for each class
		for text in fake_train:
			tokens = tokenizer(text)
			vect_fake.append(part_of_speech_tagger(tokens))
		for text in msm_train:
			tokens = tokenizer(text)
			vect_msm.append(part_of_speech_tagger(tokens))   

		size_fake_voc, size_msm_voc = len(vect_fake[0]), len(vect_msm[0])
		vect_fake, vect_msm = asarray(vect_fake), asarray(vect_msm)
		#calculating reference histogram
		fake_href = build_reference_histogram(vect_fake)
		msm_href  = build_reference_histogram(vect_msm)
	    
		#calculating features
		for doc in X_train:
			fake_hind = build_individual_histogram(vect_fake, None, doc, setup)
			msm_hind  = build_individual_histogram(vect_msm, None, doc, setup)
			H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
			H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
			JSD_fake = calculate_jsd(fake_href, fake_hind)
			JSD_msm  = calculate_jsd(msm_href,  msm_hind)

			train_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])

		for doc_test in docs_test:
			fake_hind = build_individual_histogram(vect_fake, None, doc_test, setup)
			msm_hind  = build_individual_histogram(vect_msm, None, doc_test, setup)
			H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
			H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
			JSD_fake = calculate_jsd(fake_href, fake_hind)
			JSD_msm  = calculate_jsd(msm_href,  msm_hind)

			test_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])

	return train_vector, test_vector



def build_fvector_words(X_train, Y_train, docs_test, setup):
	train_vector, test_vector = [], []
	fake_train, msm_train = [], []
	FAKE, REAL = 0, 1

	#spliting per class
	for i in range(len(Y_train)): 
		if Y_train[i] == FAKE:
			fake_train.append(X_train[i][0])
		else:
			msm_train.append(X_train[i][0])    

	#building the term matrix for each class
	if(setup[-1].find('CHAR') >= 0):
	
		if('TF' in setup):
			vect_fake = CountVectorizer(analyzer='char')
			vect_msm  = CountVectorizer(analyzer='char')
		elif('TF-IDF' in setup):
			vect_fake = TfidfVectorizer(use_idf=True, sublinear_tf=False,analyzer='char')
			vect_msm  = TfidfVectorizer(use_idf=True, sublinear_tf=False,analyzer='char')	
	else:
		ngram_range = (int(setup[-1].strip()[1]), int(setup[-1].strip()[3]))

		if('TF' in setup):
			vect_fake = CountVectorizer(ngram_range = ngram_range)
			vect_msm  = CountVectorizer(ngram_range = ngram_range)
		elif('TF-IDF' in setup):
			vect_fake = TfidfVectorizer(use_idf=True, sublinear_tf=False, ngram_range = ngram_range)
			vect_msm  = TfidfVectorizer(use_idf=True, sublinear_tf=False, ngram_range = ngram_range)    
     
        
	X_fake_train = vect_fake.fit_transform(fake_train)
	X_msm_train  = vect_msm.fit_transform(msm_train)
	size_fake_voc = len(vect_fake.vocabulary_.keys())
	size_msm_voc  = len(vect_msm.vocabulary_.keys())
 	

	#calculating reference histogram
	fake_href = build_reference_histogram(X_fake_train)
	msm_href  = build_reference_histogram(X_msm_train)

	S_fake = entropy(fake_href, 2)
	S_msm  = entropy(msm_href, 2)

	#train docs
	for doc in X_train:
		fake_hind = build_individual_histogram(X_fake_train, vect_fake, doc[0], setup)
		msm_hind  = build_individual_histogram(X_msm_train, vect_msm, doc[0], setup)
		H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
		H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
		JSD_fake = calculate_jsd(fake_href, fake_hind, S_fake)
		JSD_msm  = calculate_jsd(msm_href,  msm_hind, S_msm) 

		train_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])

	# #test docs
	for doc_test in docs_test:
		fake_hind = build_individual_histogram(X_fake_train, vect_fake, doc_test[0], setup)
		msm_hind  = build_individual_histogram(X_msm_train, vect_msm, doc_test[0], setup)
		H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
		H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
		JSD_fake = calculate_jsd(fake_href, fake_hind, S_fake)
		JSD_msm  = calculate_jsd(msm_href,  msm_hind, S_msm) 

		test_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])


	return train_vector, test_vector

def training_classifier_loo(X_fake, X_msm, Y, classifier):
    predict_list, grndtruth_list = [], []
    FAKE, REAL = 0, 1
    fake_train, msm_train, doc_test = [], [], []
    count = 0
    for i in Y: 
        if i == FAKE:
            fake_train = X_fake[0:i] + X_fake[i+1:]
            doc_test = X_fake[i]
            msm_train = X_msm
        else:
            msm_train  = X_msm[0:i]  + X_msm[i+1:]
            doc_test = X_msm[i]
            fake_train = X_fake

        X_train, Y_train, X_test = build_feature_vector(fake_train, msm_train, doc_test)
        
        classifier.fit(X_train, Y_train)
        predict_list.append(classifier.predict([X_test]))
        grndtruth_list.append(i)
        print(count,strftime("%I:%M%p"))
        
        count +=1
    
    recall = recall_score(grndtruth_list, predict_list, average=None)
    precision = precision_score(grndtruth_list, predict_list, average=None)    
    f1 = f1_score(grndtruth_list, predict_list, average=None)
    accuracy = accuracy_score(grndtruth_list, predict_list)

    print("Precision: ", precision)
    print("Recall: ", recall)
    print("F1: ", f1)


def show_results(precision, recall, f1, f1_macro):
	precision_text_0 = precision[0][0] + '+/-' + precision[0][1]
	recall_text_0    = recall[0][0] + '+/-' + recall[0][1]
	f1_text_0 	   = f1[0][0] + '+/-' + f1[0][1]
	
	precision_text_1 = precision[1][0] + '+/-' + precision[1][1]
	recall_text_1    = recall[1][0] + '+/-' + recall[1][1]
	f1_text_1 	   = f1[1][0] + '+/-' + f1[1][1]	
	
	precision_final = precision_text_0 + '\t' + precision_text_1
	recall_final = recall_text_0 + '\t' + recall_text_1
	f1_final = f1_text_0 + '\t' + f1_text_1
	f1_macro_final = f1_macro[0] + '+/-' + f1_macro[1]
	# print(precision_final)
	# print(recall_final)

	return precision_final+' | '+ recall_final +' | '+ f1_final +' | ' + f1_macro_final 	


def evaluate_effectiveness(ground_truth, predict_list):
	precision_list,recall_list,confusion_matrix_list = [],[],[]
	f1_list,accuracy_list,f1_macro_list = [],[],[]
	for i in range(len(predict_list)):
		precision = precision_score(ground_truth[i], predict_list[i], average=None)
		recall = recall_score(ground_truth[i], predict_list[i], average=None)
		f1 = f1_score(ground_truth[i], predict_list[i], average=None)
		accuracy = accuracy_score(ground_truth[i], predict_list[i])

		precision_list.append(precision)
		recall_list.append(recall)
		f1_list.append(f1)
		accuracy_list.append(accuracy)
		f1_macro = (f1[0] + f1[1])/2
		f1_macro_list.append(f1_macro)
				
		confusion_matrix_list.append(confusion_matrix(ground_truth[i], predict_list[i]))

	precision_final = statistics_metric_value(precision_list)
	recall_final = statistics_metric_value(recall_list)
	f1_final = statistics_metric_value(f1_list)
	
	# accuracy_final = statistics_metric_value(accuracy_list)
	f1_macro_final = statistics_metric_value(f1_macro_list)
	

	result_text = show_results(precision_final, recall_final, f1_final, f1_macro_final)
	cm_text = show_confusion_matrix(calculate_mean_confmatrix(confusion_matrix_list))
	return result_text, cm_text

    
    
def build_fvector_readaby(X_train, X_test):
    readby_train, readby_test = [], []
    for i in range(len(X_train)):
        readby_train.append([characters_stat(X_train[i]), complexWords_stat(X_train[i]), longWords_stat(X_train[i]),
                            numberSyllables_stat(X_train[i]), lexicon_count_stat(X_train[i]), sentence_count_stat(X_train[i]),
                            flesch_reading_ease_stat(X_train[i]), smog_index_stat(X_train[i]), flesch_kincaid_grade_stat(X_train[i]),
                            coleman_liau_index_stat(X_train[i]), automated_readability_index_stat(X_train[i]),
                            difficult_words_stat(X_train[i]), linsear_write_formula_stat(X_train[i]), gunning_fog_stat(X_train[i])]) 
        
    for i in range(len(X_test)):
        readby_test.append([characters_stat(X_test[i]), complexWords_stat(X_test[i]), longWords_stat(X_test[i]),
                            numberSyllables_stat(X_test[i]), lexicon_count_stat(X_test[i]), sentence_count_stat(X_test[i]),
                            flesch_reading_ease_stat(X_test[i]), smog_index_stat(X_test[i]), flesch_kincaid_grade_stat(X_test[i]),
                            coleman_liau_index_stat(X_test[i]), automated_readability_index_stat(X_test[i]),
                            difficult_words_stat(X_test[i]), linsear_write_formula_stat(X_test[i]), gunning_fog_stat(X_test[i])])   

    return readby_train, readby_test

def combine_features(X_words, X_postag, X_readby, X_liwc, X_cfg, Y):
	X_fold = []

	for i in range(len(Y)):
		fvector = []
		if(X_words  != []):
			fvector += X_words[i]
		if(X_postag != []):
			fvector += X_postag[i]        		
		if(X_readby != []):

			fvector += X_readby[i]      		
		if(X_liwc != []):
			fvector += X_liwc[i]        		
		if(X_cfg != []):      
			fvector += X_cfg[i]  				
		X_fold.append(fvector)
	return X_fold




def store_folds(directory, X, Y):

	# news_field = 'HEADLINE'
	# print('news field:', news_field)
	# X_h3clean, X_h2clean, X_h1clean, Y_hclean  = data_cleaning(directory, X, Y, news_field, ['stopwords','digits','punct'])

	# X_readaby = get_readability(X_h1clean)
	# X_postag = get_postag(X_h2clean)
	
	# print(len(X_readaby[0]), len(X_postag[0]), len(X_h1clean[0]))


	# X = []
	# for i in range(len(Y_hclean)):

	# 	X.append(X_h3clean[i] + X_readaby[i] + X_postag[i])

	# print('data cleaning finished')
	# split_kfold(X, Y_hclean, news_field, None)
	
	# X_cfg = get_cfg_productions(X_h1clean)
	# print('cfg_finished', strftime("%I:%M%p"))
	# split_kfold(X_cfg, Y_hclean, news_field, 'cfg')
	
	news_field = 'CONTENT'
	print('news field:', news_field)

	X_c3clean, X_c2clean, X_c1clean, Y_cclean  = data_cleaning(directory, X, Y, news_field, ['stopwords','digits','punct'])

	X_readaby = get_readability(X_c1clean)
	X_postag = get_postag(X_c2clean)

	print(len(X_readaby[0]), len(X_postag[0]), len(X_c3clean[0]))
	X = []
	for i in range(len(Y_cclean)):
		X.append(X_c3clean[i] + X_readaby[i] + X_postag[i])

	print('data cleaning finished')
	split_kfold(X, Y_cclean, news_field, None)

	X_cfg = get_cfg_productions(X_c1clean)
	print('cfg_finished', strftime("%I:%M%p"))
	split_kfold(X_cfg, Y_cclean, news_field, 'cfg')

	return True




def split_kfold(X, Y, news_field,cfg):
	Y = asarray(Y)

	kf = StratifiedKFold(n_splits=10, random_state=0, shuffle=True)
	yarray = array(Y)

	fold = 0
	for train, test in kf.split(empty((yarray.shape[0], 1)), Y):
		print('fold',fold,strftime("%I:%M%p"))
		# X_train, X_test = X[train_index].tolist(), X[test_index].tolist()
		# Y_train, Y_test = Y[train_index].tolist(), Y[test_index].tolist()
		index_train = train.tolist()
		index_test  = test.tolist()		


		if(cfg):
			filename_train = 'clean_train_'+ news_field + '_cfgprod_' 
			filename_test  = 'clean_test_' + news_field + '_cfgprod_' 		
		else:
			filename_train = 'clean_train_'+ news_field + '_'  
			filename_test  = 'clean_test_' + news_field + '_'

		print(directory + filename_train+ str(fold) + '.csv')
		print(directory + filename_test+ str(fold) + '.csv')
		print('\n')
		file_train_out = open(directory + filename_train + str(fold) + '.csv', 'w')
		file_test_out  = open(directory + filename_test + str(fold) + '.csv', 'w')		
		writer_train   = writer(file_train_out, delimiter = ',', quoting=QUOTE_ALL)
		writer_test    = writer(file_test_out, delimiter = ',', quoting=QUOTE_ALL)

		print('train',len(index_train))
		for i in index_train:
			writer_train.writerow(tuple(X[i] + [Y[i]]))

		print('test',len(index_test))
		for i in index_test:
			writer_test.writerow(tuple(X[i]+[Y[i]]))			

		file_train_out.close()
		file_test_out.close()  

		fold +=1


	return True


def transform_into_tokens(sentence_list):
    text = ""
    for sent in sentence_list:
        sent = str(sent).replace("'",'')
        text += "'"+str(sent) +"'"+ ' '
    return text.strip()


def calculate_sintax_trees(X_train, X_test):
	train_text,test_text = [], []

	pattern = "\'.*?\'"
	vect_train = TfidfVectorizer(use_idf = True, sublinear_tf = False, token_pattern = pattern)

	#print(len(X_train), len(X_test),len(X_train[0]), len(X_test[0]))
	X_train = vect_train.fit_transform(X_train)
	print('vocabulary size:',len(vect_train.vocabulary_.keys()))
	X_test  = vect_train.transform(X_test)

	#print(X_train.toarray().tolist()[0])
	#print(X_test.toarray().tolist()[0])
	#print(len(X_train.toarray().tolist()[0]), len(X_test.toarray().tolist()[0]))
	return X_train.toarray().tolist(), X_test.toarray().tolist()	     
    
    


def calculate_sintax_trees_hjsd(X_train, Y_train, X_test,setup):
	train_vector, test_vector = [], []
	vect_fake, vect_msm = [], []
	FAKE, REAL = 0, 1

	#spliting per class
	for i in range(len(Y_train)): 
		if Y_train[i] == FAKE:
			vect_fake.append(X_train[i])
		else:
			vect_msm.append(X_train[i])   

	size_fake_voc, size_msm_voc = len(vect_fake[0]), len(vect_msm[0])
	vect_fake, vect_msm = asarray(vect_fake), asarray(vect_msm)
	#calculating reference histogram
	fake_href = build_reference_histogram(vect_fake)
	msm_href  = build_reference_histogram(vect_msm)
    
	#calculating features
	for doc in X_train:
		fake_hind = build_individual_histogram(vect_fake, None, doc,setup)
		msm_hind  = build_individual_histogram(vect_msm, None, doc,setup)
		H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
		H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
		JSD_fake = calculate_jsd(fake_href, fake_hind)
		JSD_msm  = calculate_jsd(msm_href,  msm_hind)

		train_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])

	for doc_test in X_test:
		fake_hind = build_individual_histogram(vect_fake, None, doc_test,setup)
		msm_hind  = build_individual_histogram(vect_msm, None, doc_test,setup)
		H_fake = entropy(fake_hind,2)/log(size_fake_voc,2)
		H_msm  = entropy(msm_hind,2)/log(size_msm_voc,2)
		JSD_fake = calculate_jsd(fake_href, fake_hind)
		JSD_msm  = calculate_jsd(msm_href,  msm_hind)

		test_vector.append([H_fake, H_msm, JSD_fake, JSD_msm])

	return train_vector, test_vector

def build_fvector_sintax(X_train, X_test, dataset, news_field, setup, fold):
	if('CFG' in setup):
		train_vector, test_vector = calculate_sintax_trees(X_train, X_test)
	else:
		X_train, Y_train, X_test, Y_test = [], [], [], []
		directory = 'datasets/' + dataset +'/train_test_folds/' + news_field + '/'
		
		filename_train = news_field + '_CFG_' + 'train_fold_' + str(fold + 1) + '.csv'
		filename_test  = news_field + '_CFG_' + 'test_fold_'  + str(fold + 1) + '.csv'		
		with open(directory + filename_train) as file_train,\
			open(directory + filename_test) as file_test:
			reader_train = reader(file_train, delimiter = ',', quoting=QUOTE_ALL)
			reader_test = reader(file_test, delimiter = ',', quoting=QUOTE_ALL)
			for doc in reader_train:
				float_list = []
				for i in range(len(doc)-1):
					float_list.append(float(doc[i]))
				X_train.append(float_list)
				Y_train.append(int(doc[-1]))
			for doc in reader_test:
				float_list = []				
				for i in range(len(doc)-1):
					float_list.append(float(doc[i]))
				X_test.append(float_list)
				Y_test.append(int(doc[-1]))
			
			train_vector, test_vector  = calculate_sintax_trees_hjsd(X_train, Y_train, X_test,setup)


	return train_vector, test_vector


def extract_store_feature(X_train, Y_train, X_test, Y_test, directory, news_field, setup,n):
	X_train_fold, X_test_fold = [], []
	directory += news_field +'/' 

	filename_train = news_field + '_' + setup[0]
	filename_test  = news_field + '_' + setup[0]

	if('TF' in setup or 'TF-IDF' in setup):
		filename_train += setup[1] 
		filename_test  += setup[1] 
		X_train_fold, X_test_fold = build_fvector_words(X_train, Y_train, X_test, setup)

	if('POSTAG' in setup or 'POSTAG-H+JSD' in setup):
		X_train_fold, X_test_fold = build_fvector_postag(X_train, Y_train, X_test, setup)

	if('READABILITY' in setup):
		X_train_fold, X_test_fold = build_fvector_readaby(X_train, X_test)
	
	if('CFG' in setup or 'CFG-H+JSD' in setup):
		X_train_fold, X_test_fold = build_fvector_sintax(X_train, X_test, dataset, news_field, setup, n)			

	filename_train += '_train_fold_' + str(n) + '.csv'
	filename_test  += '_test_fold_'  + str(n) + '.csv'
	file_train_out = open(directory + filename_train, 'w')
	file_test_out  = open(directory + filename_test, 'w')		
	writer_train   = writer(file_train_out, delimiter = ',', quoting=QUOTE_ALL)
	writer_test    = writer(file_test_out, delimiter = ',', quoting=QUOTE_ALL)

	for i in range(len(Y_train)):
		writer_train.writerow(tuple(X_train_fold[i] + [Y_train[i]]))

	for i in range(len(Y_test)):
		writer_test.writerow(tuple(X_test_fold[i]+[Y_test[i]]))			

	file_train_out.close()
	file_test_out.close()



def loading_liwc(dataset, news_field):
	X_train, X_test = [], []
	field = 0 if (news_field == 'HEADLINE') else 1
	directory = 'datasets/' + dataset +'/train_test_folds/' + news_field + '/'
	for fold in range(0, 10):
		filename_train = news_field + '_LIWC_' + 'train_fold_' + str(fold + 1) + '.csv'
		filename_test  = news_field + '_LIWC_' + 'test_fold_'  + str(fold + 1) + '.csv'		
		X =  []
		with open(directory + filename_train) as file_train,\
			open(directory + filename_test) as file_test:			
			reader_train = reader(file_train, delimiter = ',', quoting=QUOTE_ALL)
			reader_test = reader(file_test, delimiter = ',', quoting=QUOTE_ALL)
			next(reader_train)
			next(reader_test)
			for doc in reader_train:
				float_list = []
				text = doc[field]
				text = sub(r'[^\w\s]',' ', text).strip()    
				text = sub(r'[^\D]',' ',text).strip()
				if(text != ''):
					for i in range(3,len(doc)):
						float_list.append(float(doc[i]))
					X.append(float_list)
			X_train.append(X)
			X = []
			for doc in reader_test:
				float_list = []
				text = doc[field]
				text = sub(r'[^\w\s]',' ', text).strip()    
				text = sub(r'[^\D]',' ',text).strip()
				if(text != ''):
					for i in range(3,len(doc)):
						float_list.append(float(doc[i]))
					X.append(float_list)
			X_test.append(X)

	return X_train, X_test


def loading_features(dataset, news_field, feature, n):
	X_train, Y_train, X_test, Y_test = [], [], [], []
	directory = 'datasets/' + dataset +'/train_test_folds/' + news_field + '/'
	filename_train = news_field + '_' + feature + '_' + 'train_fold_' + str(n) + '.csv'
	filename_test  = news_field + '_' + feature + '_' + 'test_fold_'  + str(n) + '.csv'		
	with open(directory + filename_train) as file_train,\
		open(directory + filename_test) as file_test:
		
		reader_train = reader(file_train, delimiter = ',', quoting=QUOTE_ALL)
		reader_test = reader(file_test, delimiter = ',', quoting=QUOTE_ALL)
		for doc in reader_train:
			float_list = []
			for i in range(len(doc)-1):
				float_list.append(float(doc[i]))
			X_train.append(float_list)

		for doc in reader_test:
			float_list = []				
			for i in range(len(doc)-1):
				float_list.append(float(doc[i]))
			X_test.append(float_list)

	return X_train, X_test


def classification(X_train, Y_train, X_test, Y_test, algorithm):

	svm_rbf_res, svc_linear_res, mnb_res = [], [], []
	knn_res, rf_res, ground_truth = [], [], []
	train_time, test_time = 0.0, 0.0
	classifier = None

	if(algorithm   == 'SVM'):
		classifier = svm.LinearSVC(random_state=5, tol=0.001,
		 C=1.0,loss='hinge')

	elif(algorithm == 'KNN'):
		classifier = KNeighborsClassifier(n_neighbors=5)

	elif(algorithm == 'RNF'):		
		classifier = RandomForestClassifier(random_state=5)

	elif(algorithm == 'MNB'):
		classifier = GaussianNB()
	
	time_then = time()
	classifier.fit(X_train, Y_train)
	time_now = time()
	train_time = time_now - time_then

	time_then = time()
	prediction = classifier.predict(X_test)
	time_now = time()
	test_time = time_now - time_then

	return prediction, train_time, test_time



def feature_selection(X, attr_description):
	X_ranked = []
	index_toremove = []
	for i in attr_description:
		index = int(i.split('_')[1])
		index_toremove.append(index)
	for doc in X:
		attr = []
		for j in range(len(doc)):
			if(j not in index_toremove):
				attr.append(doc[j])

		X_ranked.append(attr)
	return X_ranked


def copy_list(X):
	X_new = []
	for i in X:
		X_new.append(i.copy())
	return X_new

def information_gain(X, Y, ratio_remove):

	#print(clf.feature_importances_,'\n')
	most_important = []
	attr_description = []
	a,b,c,d = 0,0,0,0
	for i in range(len(X[0])):
		if(i < 4):
			attr_description.append('hjsd_'+str(i))
		elif(i >=4 and i < 97):
			attr_description.append('lwic_'+str(i))
		elif(i >=97 and i < 111):
			attr_description.append('readaby_'+str(i))
		else:
			attr_description.append('postag_'+str(i))


	attr_size = len(X[0]) 
	nro_iteratons = int(round(attr_size - (attr_size * ratio_remove) ,2))


	for i in range(nro_iteratons):
		clf = tree.DecisionTreeClassifier(criterion = 'entropy',random_state=0)
		clf = clf.fit(X, Y)		
		ranking = clf.feature_importances_.tolist()
		max_ = [0,0]
		for i in range(len(ranking)):
			if(ranking[i] >= max_[0]):
				max_ = [ranking[i],i]

		most_important.append(max_[1])
		attr_description.pop(max_[1])
		for i in range(len(Y)):
			X[i].pop(max_[1])


	return attr_description



def model_evaluation(directory, news_field, params,lim):	
	predictions_svm, predictions_knn  = [], []
	predictions_mnb,  predictions_rnf= [], []
	ground_truth = []

	train_time_list_svm, test_time_list_svm = [], []
	train_time_list_knn, test_time_list_knn = [], []
	train_time_list_mnb, test_time_list_mnb = [], []
	train_time_list_rnf, test_time_list_rnf = [], []
	
	#headline, content = 484, 500 celebrity
	ini = lim-100
	for n in range(0, 10):
		#print(n,strftime("%I:%M%p"))

		X_train, Y_train, X_test, Y_test = setup_features(directory, news_field, params, n)
		X = copy_list(X_train)
		attr_description = information_gain(X_train, Y_train, lim)		
		
		X_train = feature_selection(X, attr_description)	
		X_test  = feature_selection(X_test, attr_description)
		ground_truth.append(Y_test)

		pred, time_train, time_test = classification(X_train, Y_train, X_test, Y_test, 'SVM')		
		predictions_svm.append(pred)
		train_time_list_svm.append(time_train)
		test_time_list_svm.append(time_test)		
		
		pred, time_train, time_test = classification(X_train, Y_train, X_test, Y_test, 'KNN')		
		predictions_knn.append(pred)
		train_time_list_knn.append(time_train)
		test_time_list_knn.append(time_test)

		pred, time_train, time_test = classification(X_train, Y_train, X_test, Y_test, 'MNB')		
		predictions_mnb.append(pred)
		train_time_list_mnb.append(time_train)
		test_time_list_mnb.append(time_test)

		pred, time_train, time_test = classification(X_train, Y_train, X_test, Y_test, 'RNF')		
		predictions_rnf.append(pred)
		train_time_list_rnf.append(time_train)
		test_time_list_rnf.append(time_test)




	rnf_results = store_results(dataset, news_field, predictions_rnf, ground_truth,\
			train_time_list_rnf, test_time_list_rnf, params,'RNF').split('|')

	mnb_results = store_results(dataset, news_field, predictions_mnb, ground_truth,\
			train_time_list_mnb, test_time_list_mnb, params,'MNB').split('|')

	svm_results = store_results(dataset, news_field, predictions_svm, ground_truth,\
			train_time_list_svm, test_time_list_svm, params,'SVM').split('|')

	knn_results = store_results(dataset, news_field, predictions_knn, ground_truth,\
			train_time_list_knn, test_time_list_knn, params, 'KNN').split('|')


	text_precision = svm_results[0].strip() + '\t' + knn_results[0].strip() + '\t' + mnb_results[0].strip() + '\t' + rnf_results[0].strip()
	text_recall = svm_results[1].strip() + '\t' + knn_results[1].strip() + '\t' + mnb_results[1].strip() + '\t' + rnf_results[1].strip()
	text_f1 = svm_results[2].strip() + '\t' + knn_results[2].strip() + '\t' + mnb_results[2].strip() + '\t' + rnf_results[2].strip()
	text_f1_macro = svm_results[3].strip() + '\t' + knn_results[3].strip() + '\t' + mnb_results[3].strip() + '\t' + rnf_results[3].strip()
	print(text_precision)
	print(text_recall)
	print(text_f1)
	print(text_f1_macro)

def check_directory(path):
	files = listdir('./' + path)
	for i in files:
		if(i.endswith('.csv')):
			return False
	return True

def store_results(dataset, news_field, predictions, ground_truth, time_train,\
					 time_test, features_list, classifier):		
	directory = 'datasets/' + dataset +'/results/' + news_field + '/'
	feature = ''

	result_text, cm_text = evaluate_effectiveness(ground_truth, predictions)		


	for i in features_list:
		feature += '_' + i		
	
	filename = news_field + feature + '_' + classifier + '_result.csv'

	file_out = open(directory+filename, 'w')
	writer_out = writer(file_out, delimiter = ',', quoting=QUOTE_ALL)

	writer_out.writerow(tuple(result_text))
	writer_out.writerow(tuple(cm_text))

	writer_out.writerow(tuple(time_train))
	writer_out.writerow(tuple(time_test))			
	
	file_out.close()

	return result_text


def loading_ngrams(docs_train, docs_test):
	X_train, X_test = [], []
	vect_train = TfidfVectorizer(use_idf=True, sublinear_tf=False, ngram_range = (1,2))
	
	for i in docs_train:
		X_train.append(i[0])
	for i in docs_test:
		X_test.append(i[0])		

	X_train_fold = vect_train.fit_transform(X_train).toarray().tolist()
	X_test_fold  = vect_train.transform(X_test).toarray().tolist()


	return X_train_fold, X_test_fold

def get_features(interval, X_train, X_test):
	train, test = [], []
	a,b = interval

	for i in X_train:
		train.append(i[a:b])

	for i in X_test:
		test.append(i[a:b])

	return train, test

def setup_features(directory, news_field, params, n):
	X_train_ngrams, X_train_ngrams, X_test_ngrams, Y_test_ngrams = [], [], [], []
	X_train_postag, Y_train_postag, X_test_postag, Y_test_postag = [], [], [], []
	X_train_readby, Y_train_readby, X_test_readby, Y_test_readby = [], [], [], []
	X_train_cfg, Y_train_cfg, X_test_cfg, Y_test_cfg, X_train_liwc, X_test_liwc  = [], [], [], [], [], []


	X_train, Y_train, X_test, Y_test = read_traintest_folds(directory,\
		news_field, n)
	
	for feature in features_list:
		if('NGRAMS'  == feature):
			X_train_ngrams, X_test_ngrams  = loading_ngrams(X_train,X_test)	
		
		elif(feature.startswith('TF')):
			X_train_ngrams, X_test_ngrams  = loading_features(dataset, news_field, feature,n)

		elif('POSTAG' == feature):

			X_train_postag, X_test_postag = get_features((201,len(X_train[0])), X_train, X_test)    
		
		elif('LIWC' == feature):
			if(news_field == 'HEADLINE'):		
				X_train_liwc, X_test_liwc  = get_features((1,94), X_train, X_test)
			else:
				X_train_liwc, X_test_liwc  = get_features((94,187), X_train, X_test)

		elif('READABILITY' == feature):
			X_train_readby, X_test_readby = get_features((187,201), X_train, X_test)				

		else:
			X_train_cfg, X_test_cfg  = loading_features(dataset, news_field, feature,n)	


	# print(X_train_ngrams[0])
	# print(X_train_postag[0])
	# print(X_train_readby[0])
	
	# print(X_train_liwc[0])
	# print(len(X_train_ngrams[0]),len(X_train_postag[0]),len(X_train_readby[0]),len(X_train_liwc[0]),len(Y_train))
	# print(len(X_train_ngrams[0]),len(X_test_postag[0]),len(X_test_readby[0]),len(X_test_liwc[0]),len(Y_test))

	X_train = combine_features(X_train_ngrams, X_train_postag, X_train_readby, X_train_liwc, X_train_cfg, Y_train)

	#print(len(X_test_postag),len(X_test_readby),len(X_test_liwc),len(Y_test),)
	X_test = combine_features(X_test_ngrams, X_test_postag, X_test_readby, X_test_liwc, X_test_cfg, Y_test)

	return X_train, Y_train, X_test, Y_test 

if __name__ == '__main__':
	dataset = sys.argv[1]
	news_field = sys.argv[2].upper().strip()
	#fold = int(sys.argv[3])
	#params = sys.argv[3].upper().split('|')
	#print(params)

	directory = 'datasets/' + dataset +'/train_test_folds/'

	# Storing folds
	# if(dataset.upper().strip() == 'RASHKIN'):
	# 	if(check_directory(directory)):
	# print('Storing folds')
	# fake_docs  = read_dataset(dataset, 'fake')
	# real_docs  = read_dataset(dataset, 'real')
	
	# X = fake_docs + real_docs
	# Y = ['0' for i in range(len(fake_docs))] +\
	#     ['1' for i in range(len(real_docs))]			
	
	# store_folds(directory, X, Y)






	
	# params_list = [['TF','(1,1)'], ['TF','(2,2)'], ['TF','(1,2)'],
	# 		  ['TF-IDF','(1,1)'], ['TF-IDF','(2,2)'], ['TF-IDF','(1,2)']]

	# range_size = 3			  
	# for params in [['TF','(1,2)']]:
	# 	#print(params, strftime("%I:%M%p"))
	# 	cfg = None
	# 	for fold in range(2, range_size):
	# 		print('fold',fold,strftime("%I:%M%p"))	

	# 		# if(params== ['CFG']):
	# 		# 	cfg = 'CFG'
		
	# 		X_train, Y_train, X_test, Y_test = read_traintest_folds(directory,\
	# 			news_field, fold,cfg)

	# 		extract_store_feature(X_train, Y_train, X_test, Y_test, directory, news_field, params, fold)


	# print(dataset, news_field, strftime("%I:%M%p"))
	# features_list = ['NGRAMS', 'POSTAG', 'READABILITY', 'LIWC','CFG']	
	# model_evaluation(directory, news_field, features_list, 0.10)
	# print(strftime("%I:%M%p"))

	# features_list = ['TF(1,1)']#, 'POSTAG', 'READABILITY', 'LIWC']
	
	# model_evaluation(directory, news_field, features_list, 0)

	# features_list = ['TF(2,2)']#, 'POSTAG', 'READABILITY', 'LIWC']
	# model_evaluation(directory, news_field, features_list, 0)

	# features_list = ['TF(1,2)']#, 'POSTAG', 'READABILITY', 'LIWC']	
	# model_evaluation(directory, news_field, features_list, 0)


	features_list = ['TF(1,2)', 'POSTAG', 'READABILITY', 'LIWC']
	model_evaluation(directory, news_field, features_list, 0.75)
	# model_evaluation(directory, news_field, features_list, 0.50)
	# model_evaluation(directory, news_field, features_list, 0.25)
	# model_evaluation(directory, news_field, features_list, 0)










