# -*- coding: utf-8 -*-
import numpy as np
from re import sub, findall, match, MULTILINE
from csv import reader,writer,QUOTE_ALL
from string import punctuation
from operator import itemgetter
import statistics
import scipy.stats as stats

#import matplotlib.pyplot as plt

from nltk import FreqDist, pos_tag
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, RegexpTokenizer
from textstat.textstat import textstatistics, easy_word_set, legacy_round
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split

from math import log, sqrt

def remove_ulr_corpus(text):
    new_text = sub(r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))', '', text, flags=MULTILINE)
    count_matches = len(findall(r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))',text))
    return new_text, count_matches

def remove_stopwords_corpus(text):
    content = []
    for w in text:
        if w.lower().strip() not in stopwords.words('english'):
            content.append(w.lower().strip())

    return content
    
def remove_mention_corpus(text):
    new_text = sub(r'@\w+ ?','',text)
    count_matches = len(findall(r'@\w+ ?',text))
    
    return new_text, count_matches

def tokenizer(text):
    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(text)
    
    return tokens

def part_of_speech_tagger(tokens):
    ''' 
    More details in nltk.help.upenn_tagset()
        CC, CD, DT, FW, IN, JJ, JJR, JJS, LS, MD, NN, NNP,
        NNPS, NNS, PDT, POS, PRP, PRP$, RB, RBR, RBS, RP, SYM,
        TO, UH, VB, VBD, VBG, VBN, VBP, VBZ, WDT, WP, WP$, WRB
    '''
    word_tagged_dict = {"CC":0, "CD":0, "DT":0, "FW":0, "IN":0, "JJ":0, "JJR":0,
            "JJS":0, "MD":0, "NN":0, "NNP":0, "NNPS":0, "NNS":0,
            "PDT":0, "POS":0, "PRP":0, "PRP$":0, "RB":0, "RBR":0, "RBS":0,
            "RP":0, "TO":0, "UH":0, "VB":0, "VBD":0, "VBG":0, "VBN":0,
            "VBP":0, "VBZ":0, "WDT":0, "WP":0, "WP$":0, "WRB":0}

    word_tagged_list, result = pos_tag(tokens), []
    for item in  word_tagged_list:
        if(item[1] in word_tagged_dict.keys()):
            word_tagged_dict[item[1]] += 1

    word_tagged_sorted = sorted(word_tagged_dict.items(), key=itemgetter(0))
    word_tagged_list = []
    for i in word_tagged_sorted:
        word_tagged_list.append(i[1])

    return word_tagged_list

def count_captalized_words(tokens):
    nro_upper_words = 0
    for i in tokens:
        if(i.isupper()):
            nro_upper_words += 1
    return nro_upper_words

def count_per_stopwords(tokens):
    total_words, words_per_sent = len(tokens), 0.0
    stopwords_nltk = stopwords.words('english')
    count = 0
    for w in tokens:
        if w.lower().strip() in stopwords_nltk:
            count += 1
    if(total_words > 0):
        words_per_sent = round(count/total_words,2)
    return words_per_sent

def count_punctuation(text, regular_exp):
    #r'[' + punctuation+ r']+' and r'["\']+'
    count = 0
    tokenizer = RegexpTokenizer(regular_exp)
    tokens = tokenizer.tokenize(text)
    count = len(tokens)

    return count

def count_quotes(text):
    count = 0
    tokenizer = RegexpTokenizer(r'["\']+')
    tokens = tokenizer.tokenize(text)
    count = len(tokens)

    return count

def sentence_level_attr(text, tokens):
    word_count, word_sent = 0, 0
    words_per_sent = 0.0

    #word count of the whole text
    vocabulary = FreqDist(tokens)
    word_count = len(vocabulary.keys())

    #word count by setence
    sent_tokenize_list = sent_tokenize(text)
    for i in sent_tokenize_list:
        tkn = tokenizer(i)
        vocabulary = FreqDist(tkn)
        word_sent += len(vocabulary.keys())

    if(len(sent_tokenize_list) > 0):
        words_per_sent = round(word_sent/len(sent_tokenize_list), 2)

    return word_count, words_per_sent


# Returns Number of Words in the text
def word_count(text):
    text = sub(r'[^\w\s]',' ',text).strip() #remove punct
    words = len(tokenizer(text))
    return words
 
def character_count(text):
    characters = 0 
    for token in tokenizer(text):
        token = sub(r'[^\w\s]','',token) #remove punct.
        for t in token.split():
            if(bool(match("^[A-Za-z0-9]*$", t)) and t != ''): 
                characters+= len(t)
    return characters

def letter_count(text):
    letters = 0 
    for token in tokenizer(text):
        token = sub(r'[^\D]',' ',token).strip() #remove digits
        token = sub(r'[^\w\s]',' ',token).strip() #remove punct
        for t in token.split():
            if(bool(match("^[A-Za-z0-9]*$", t)) and t != ''):
                letters+= len(t)
    return letters

# Returns average sentence length
def avg_sentence_length(text):
    words = word_count(text)
    sentences = len(sent_tokenize(text))
    if(sentences > 0):
        average_sentence_length = float(words / sentences)
    else:
        average_sentence_length = 0.0
    return average_sentence_length

def syllables_count(word):
    return textstatistics().syllable_count(word)
 
# Returns the average number of syllables per
# word in the text
def avg_syllables_per_word(text):
    syllable = syllables_count(text)
    words = word_count(text)
    if(float(words) > 0):
        ASPW = float(syllable) / float(words)
    else:
        ASPW = 0.0
    return legacy_round(ASPW, 1)
 
# Return total Difficult Words in a text
def difficult_words(text):
 
    # Find all words in the text
    words = []
    sentences = sent_tokenize(text)
    for sentence in sentences:
        words += [token for token in sentence]
 
    # difficult words are those with syllables >= 2
    # easy_word_set is provide by Textstat as 
    # a list of common words
    diff_words_set = set()
     
    for word in words:
        syllable_count = syllables_count(word)
        if word not in easy_word_set and syllable_count >= 2:
            diff_words_set.add(word)
 
    return len(diff_words_set)

# A word is polysyllablic if it has more than 3 syllables
# this functions returns the number of all such words 
# present in the text
def poly_syllable_count(text):
    count = 0
    words = []
    sentences = sent_tokenize(text)
    for sentence in sentences:
        words += [token for token in sentence]
     
 
    for word in words:
        syllable_count = syllables_count(word)
        if syllable_count >= 3:
            count += 1
    return count

def dale_chall_index(text):
    """
        Implements Dale Challe Formula:
        Raw score = 0.1579*(PDW) + 0.0496*(ASL) + 3.6365
        Here,
            PDW = Percentage of difficult words.
            ASL = Average sentence length
    """
    words = word_count(text)
    # Number of words not termed as difficult words
    count = words - difficult_words(text)
    if words > 0:
        # Percentage of words not on difficult word list
        per = float(count) / float(words) * 100
    else:
        per = 0.0

        
    # diff_words stores percentage of difficult words
    diff_words = 100 - per
    raw_score = (0.1579 * diff_words) + \
                (0.0496 * avg_sentence_length(text))
     
    # If Percentage of Difficult Words is greater than 5 %, then;
    # Adjusted Score = Raw Score + 3.6365,
    # otherwise Adjusted Score = Raw Score
 
    if diff_words > 5:       
        raw_score += 3.6365
         
    return legacy_round(raw_score, 2)

def gunning_fog_index(text):
    if(word_count(text) > 0):
        per_diff_words = (difficult_words(text) / word_count(text) * 100) + 5
    else:
        per_diff_words = 5
    score = 0.4 * (avg_sentence_length(text) + per_diff_words)
    return score
 
def smog_index(text):
    """
        Implements SMOG Formula / Grading
        SMOG grading = 3 + ?polysyllable count.
        Here, 
           polysyllable count = number of words of more
          than two syllables in a sample of 30 sentences.
    """
    sentence_count = len(sent_tokenize(text))
    if sentence_count >= 3:
        poly_syllab = poly_syllable_count(text)
        SMOG = (1.043 * (30*(poly_syllab / sentence_count))**0.5) \
                + 3.1291
        return legacy_round(SMOG, 1)
    else:
        return 0.0
    
def flesch_index(text):
    """
        Implements Flesch Formula:
        Reading Ease score = 206.835 - (1.015 × ASL) - (84.6 × ASW)
        Here,
          ASL = average sentence length (number of words 
                divided by number of sentences)
          ASW = average word length in syllables (number of syllables 
                divided by number of words)
    """
    score = 206.835 - float(1.015 * avg_sentence_length(text)) -\
          float(84.6 * avg_syllables_per_word(text))
    
    return legacy_round(score, 2) 


def automated_readby_index(text):
    """
        Automated Readability Index:
          ARI = 4.71×(characters/words) + 0.5×(words/sentences) - 21.43
        Here,
          characters =  number of letters and numbers  
    """
    part1,part2 = 0.0 , 0.0
    if(word_count(text)>0):
        part1  = (character_count(text)/word_count(text))
    else:
        part1  = 0.0
    
    if(len(sent_tokenize(text))>0):        
        part2 = (word_count(text)/len(sent_tokenize(text)))
    else:
        part2 = 0.0

    score = 4.71 * part1 + 0.5 * part2 - 21.43

    return score

def coleman_liau_index(text):
    """
        The Coleman–Liau Index:
          CLI = 0.0588×L -0.296×S - 15.8
        Here,
          L = Letters ÷ Words × 100 (average number of letters per 100 words)
          S = Sentences ÷ Words × 100 (average number of sentences per 100 words)
    """
    if(word_count(text) > 0):
        L = character_count(text) / word_count(text) * 100
        S = len(sent_tokenize(text))/ word_count(text) * 100
    else:
        L, S = 0.0, 0.0       
    score = 0.0588 * L - 0.296 * S - 15.8
    return score


def calculate_mean_confmatrix(confusion_list):
    result = [[0.0,0.0],[0.0,0.0]]
    for cm in confusion_list:
        for i in range(0,len(result)):
            for j in range(0,len(result)):
                result[i][j] += cm[i][j]
    for i in range(0,len(result)):
        for j in range(0,len(result)):
            result[i][j] = result[i][j]/len(confusion_list)
    return result

def show_confusion_matrix(cm):
    for i in cm:
        a = "{:.2f}".format(i[0])
        b = "{:.2f}".format(i[1])
        #print("["+a+" "+b+"]")

    return "["+a+" "+b+"]"


def read_dataset(filename, label, text_field):
    list_docs = []

    with open(filename, "r") as arq_in:
        reader_in = reader(arq_in, delimiter=',', quoting=QUOTE_ALL)
        for t in reader_in:
            if(len(t[text_field]) > 1):#tratando alguns missing data
                list_docs.append([t[text_field], int(t[9]), int(t[10]), int(t[11]),int(t[17])])

    return list_docs  

def classification2(classifier, X, Y):
    precision_list, recall_list, f1_list = [], [], []
    accuracy_list = []
    confusion_matrix_list = []
    
    #10 fold cross validation
    for fold in range(0,10):
        #Divide o conjunto de dados em treino e teste(20%)
        X_train, X_test, y_train, y_test = train_test_split(X, Y, \
                                          test_size=0.2, stratify=Y)
        #treino
        classifier.fit(X_train, y_train)
        #teste
        predict_list = classifier.predict(X_test)

        #avaliação
        precision = precision_score(y_test, predict_list, average=None)
        recall = recall_score(y_test, predict_list, average=None)
        f1 = f1_score(y_test, predict_list, average=None)
        accuracy = accuracy_score(y_test, predict_list)

        precision_list.append(precision)
        recall_list.append(recall)
        f1_list.append(f1)
        accuracy_list.append(accuracy)
        confusion_matrix_list.append(confusion_matrix(y_test, predict_list))

    print("Precision: ",statistics_metric_value(precision_list))
    print("Recall: ",statistics_metric_value(recall_list))
    f_scores =statistics_metric_value(f1_list)

    print("F1: ", f_scores)
    print("Accuracy: ",statistics_metric_value(accuracy_list))
    #print(show_confusion_matrix(calculate_mean_confmatrix(confusion_matrix_list)))
    return  classifier




def get_confidence_interval(data, confidence=0.95):
    #to large data sample
    #stats.norm.interval(0.95, loc=np.mean(a), scale=stats.sem(a)))

    #to small data sample
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), stats.sem(a)
    h = se * stats.t.ppf((1 + confidence) / 2., n-1)
    return h

def statistics_metric_value(metric_list):
    z_critical = stats.norm.ppf(q = 0.975) #1.959963984540054
    #print(isinstance(,array))
    if(not(isinstance(metric_list[0], np.ndarray))):
        result = 0.0
        mean_value = 0.0
        elements_class = []        
        for j in metric_list:
            mean_value += j
            elements_class.append(j)

        # standard_dev = statistics.stdev(elements_class) 
        # margin_of_error = z_critical * (standard_dev/sqrt(len(elements_class)))
        # confidence_interval = "{:4.2f}".format(margin_of_error)
        confidence_interval = "{:4.2f}".format(get_confidence_interval(elements_class))
        
        accuracy = mean_value/len(metric_list)

        #result[i] = ["{:2.4f}".format(mean_list[i]), confidence_interval]
        #print('\n',elements_class)        
        result = ["{:.2f}".format(accuracy),confidence_interval]

    else:

        mean_list = [0,0]
        elements_class = []        
        result = {0:[], 1:[]}

        for i in range(0,len(mean_list)):
            elements_class = []        
            for j in metric_list:
                mean_list[i] += j[i]
                elements_class.append(j[i])

            # standard_dev = statistics.stdev(elements_class) 
            # margin_of_error = z_critical * (standard_dev/sqrt(len(elements_class)))
            # confidence_interval = "{:4.2f}".format(margin_of_error)
            
            mean_list[i] = mean_list[i]/len(metric_list)

            confidence_interval = "{:.2f}".format(get_confidence_interval(elements_class))

            #print('\n',elements_class)
            result[i] = ["{:.2f}".format(mean_list[i]), confidence_interval]
            #result[i] = "{:2.4f}".format(mean_list[i])        
    
    return result

def calculate_mean_confmatrix(confusion_list):
    result = [[0.0,0.0],[0.0,0.0]]
    for cm in confusion_list:
        for i in range(0,len(result)):
            for j in range(0,len(result)):
                result[i][j] += cm[i][j]
    for i in range(0,len(result)):
        for j in range(0,len(result)):
            result[i][j] = result[i][j]/len(confusion_list)
    return result   


def classification_topic(labels_clusters,classifier, X, Y):
	precision_list, recall_list, f1_list = [], [], []
	confusion_matrix_list = []

	#10 fold cross topic validation
	for fold in range(0,10):
	    X_train, X_test, y_train, y_test = [], [], [], []
	    for i in range(0,len(labels_clusters)):
	        if(labels_clusters[i] != fold):
	            X_train.append(X[i])
	            y_train.append(Y[i])
	        else:
	            X_test.append(X[i])
	            y_test.append(Y[i])            

	    #treino
	    classifier.fit(X_train, y_train)
	    #teste
	    predict_list = classifier.predict(X_test)

	    #avaliação
	    precision = precision_score(y_test, predict_list, average=None)
	    recall = recall_score(y_test, predict_list, average=None)
	    f1 = f1_score(y_test, predict_list, average=None)

	    precision_list.append(precision)
	    recall_list.append(recall)
	    f1_list.append(f1)
	    confusion_matrix_list.append(confusion_matrix(y_test, predict_list))

	print("Precision: ",statistics_metric_value(precision_list))
	print("Recall: ",statistics_metric_value(recall_list))
	f_scores = statistics_metric_value(f1_list)

	return (float(f_scores[0])+float(f_scores[1]))/2.0

def cleaning_data(dataset_records):
    list_docs, list_labels = [], []
    for rec in dataset_records:
        text = tokenizer(rec[0])
        text = remove_stopwords_corpus(text)
        text = ' '.join(text)
        list_docs.append(text)
        list_labels.append(rec[1])
    return list_docs,list_labels


def clean_url(url):
    url = url.replace('http://','')
    url = url.replace('https://','')
    url = url.replace('www.','')
    return url

def get_url_domain(url):
    url = clean_url(url)
    pos_ini = 0
    if(url[pos_ini:].find('/') >=0):
        pos_end = pos_ini+url[pos_ini:].find('/')
    else:
        pos_end = len(url)
    url_domain = url[pos_ini:pos_end]

    return url_domain

def get_unique_domains(filename):
    pages_per_domain = {}
    with open(filename, "r") as arq_in:
        reader_in = reader(arq_in, delimiter=',', quoting=QUOTE_ALL)
        for t in reader_in:
            domain_name = get_url_domain(t[0])
            if domain_name not in pages_per_domain:
                pages_per_domain[domain_name] = [t[0]]
            else:
                pages_per_domain[domain_name].append(t[0])
    return pages_per_domain

if __name__ == '__main__':
    print('COMPILE!!!')

    
    # quality_index = [dale_chall_index(text),
    #                  gunning_fog_index(text),
    #                  smog_index(text),
    #                  automated_readby_index(text), 
    #                  flesch_index(text), 
    #                  coleman_liau_index(text)]  



