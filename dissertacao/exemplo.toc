\babel@toc {brazil}{}
\contentsline {chapter}{Resumo}{vii}{chapter*.1}
\contentsline {chapter}{Abstract}{ix}{chapter*.2}
\contentsline {chapter}{Lista de Figuras}{xi}{section*.3}
\contentsline {chapter}{Lista de Tabelas}{xiii}{section*.4}
\contentsline {chapter}{\chapternumberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Problema}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Organiza\IeC {\c c}\IeC {\~a}o da Disserta\IeC {\c c}\IeC {\~a}o}{3}{section.1.3}
\contentsline {chapter}{\chapternumberline {2}Theoretical Background}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Not\IeC {\'\i }cias Falsas}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Data representation}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Bag of Words}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Term Frequency and Inverse Document Frequency }{8}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Part of Speech Tagging}{9}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Machine Learning}{10}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}\textit {k-Nearest Neighbor}}{10}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}\textit {Gaussian Naive Bayes}}{11}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}\textit {Support Vector Machine}}{12}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}\textit {Random Forest}}{13}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Feature Selection}{13}{section.2.4}
\contentsline {section}{\numberline {2.5}Information Theory Quantifiers}{14}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Shannon Entropy}{14}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Jensen-Shannon Divergence}{15}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}Model validation techniques}{15}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}K-fold cross validation}{15}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Leave-one-out cross validation}{16}{subsection.2.6.2}
\contentsline {section}{\numberline {2.7}Effectiveness measures}{16}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Precision}{16}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Recall}{17}{subsection.2.7.2}
\contentsline {subsection}{\numberline {2.7.3}F-measure}{17}{subsection.2.7.3}
\contentsline {section}{\numberline {2.8}Considera\IeC {\c c}\IeC {\~o}es Finais}{17}{section.2.8}
\contentsline {chapter}{\chapternumberline {3}Related Work}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Fake News Datasets}{19}{section.3.1}
\contentsline {section}{\numberline {3.2}Fake News Detection}{22}{section.3.2}
\contentsline {section}{\numberline {3.3}Considera\IeC {\c c}\IeC {\~o}es Finais}{25}{section.3.3}
\contentsline {chapter}{\chapternumberline {4}Proposed Approach}{27}{chapter.4}
\contentsline {section}{\numberline {4.1}Data Pre-processing}{27}{section.4.1}
\contentsline {section}{\numberline {4.2}Feature Extraction}{27}{section.4.2}
\contentsline {section}{\numberline {4.3}Classification}{30}{section.4.3}
\contentsline {section}{\numberline {4.4}Considera\IeC {\c c}\IeC {\~o}es Finais}{30}{section.4.4}
\contentsline {chapter}{\chapternumberline {5}Experimentos e Resultados}{31}{chapter.5}
\contentsline {section}{\numberline {5.1}Materials and Methods}{32}{section.5.1}
\contentsline {section}{\numberline {5.2}Leave-one out: Celebrity and Fakenewsnet}{34}{section.5.2}
\contentsline {section}{\numberline {5.3}Cross validation: Emergent and Fake.br}{37}{section.5.3}
\contentsline {section}{\numberline {5.4}Considera\IeC {\c c}\IeC {\~o}es Finais}{42}{section.5.4}
