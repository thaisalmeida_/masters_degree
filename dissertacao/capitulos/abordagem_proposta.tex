\chapter{Proposed Approach}\label{chap:proposed_approach}


In this chapter, we introduce the proposed approach for identifying fake news. Our approach, which we call \textbf{TruthDetector}, consists of four main parts: (i) data pre-processing, where we submit documents to a data cleaning proccess; (ii) feature extraction, where we codify documents into numerical vectors; (iii) feature selection, where we pick the most discriminative set of attributes; and (iv) classification, where we identify whether a news articles is legitimate or false. %Figure \ref{fig:proposed_approach} shows an overview of our approach.

\section{Data Pre-processing}
In this first part, we start by removing stopwords, digits and punctuation of the news articles. After this, we submit the documents to a tokenization process and compute the frequency of each token in our corpus.

\section{Feature Extraction}
Recent works have argued that fake news articles are designed to induce affective and
inflammatory emotions in readers, and contain text patterns related to understandability that differ from legitimate news~\cite{horne2017just, perez@coling2018, bakir2018fake}. This motivated us to investigate linguistic-based features that captures morphological,  psychological, readability and stylometric patterns. These set of features are listed in Table~\ref{table:topic_agnostic_features} and we describe them below.


\begin{table*}[t]
	\centering
	\caption{Linguistic-based features used to represent news articles.}
	\begin{adjustbox}{width=1\textwidth}
		
		\begin{tabular}{clll}
			\toprule
			\multirow{14}{*}{\begin{tabular}[c]{@{}c@{}}\textbf{Morphological} \\ \textbf{Features}\end{tabular}}  & \textbf{Description}      & \textbf{Description}       & \textbf{Description}        \\
		                             & Conjunction, coordinating                                  & Pre-determiner                                                       & Interjection \\
			                           & Numeral, cardinal                                                    & Verb, past tense                                                         & Verb, base form    \\
		                           & Determiner                                                          & Noun, proper, plural                                              & Verb, present participle or gerund                  \\
		                                               & Foreign word                                                & Noun, common, plural                                              & Verb, past participle       \\
	                         & Preposition or conjunction, subordinating                         & Genitive marker                                  & Verb, present tense, not 3rd singular                  \\
                  & Adjective or numeral, ordinal                                      & Pronoun, personal                                             & Verb, present tense, 3rd singular                \\
                         & Adjective, comparative                               & Pronoun, possessive                                        & WH-determiner                    \\
                          & Adjective, superlative                             & Adverb                           & WH-pronoun                         \\
                            & Modal auxiliary                               & Adverb, comparative                                           & WH-pronoun, possessive                  \\
                          & Noun, common, singular or mass                                & Adverb  superlative                              & Wh-adverb             \\
                         & Noun, proper, singular                                    & "To" as preposition/infinitive                                 & Particle                       \\
			\midrule
			
			\multirow{5}{*}{\begin{tabular}[c]{@{}c@{}}\textbf{Psychological}\\   \textbf{Features}\end{tabular}}                             & Summary Dimensions (word tone)                                       & Biological Processes (ingest, health)                                & Affect (anger, sad, anxiety)           \\
		              & Function Words (pronoun, negations)                                  & Drives (power, risk)                                & Relativity   (space, time)               \\
	                            & Punctuation Marks (comma, semicolon)       & Other Gramar (quantifiers, interrogatives)                              & Personal Concerns (home, work)           \\
	                 & Perceptual Process (see, hear)                           &Time Orientation (focuspast, focuspresent)                            & Social (family, friend)                              \\
	                       & Cognitive Processes (insight, certainty)                               & Informal Language (netspeak, filler)                &                                    \\
			\midrule
			
			\multirow{7}{*}{\begin{tabular}[c]{@{}c@{}}\textbf{Readability}\\   \textbf{Features}\end{tabular}} &     Flesch Reading Ease                                          & Words per sentence       & Long words            \\
	          &     Flesch Kincaid Grade                          & Capitalized words                                 & Syllables                  \\
			                          &   McLaughlin’s SMOG                       &  Percentage of stopwords                                          & Lexicon           \\
                   &  Gunning Fog                          & Urls                                      & Sentences                               \\
                      &     Coleman-Liau                           & Difficult words                &               Words   \\
		             &    Automated Readability         &       Characters         &                     \\
	          &       Linsear Write     &                      Complex words &                                   \\
			\midrule
			
			\multirow{3}{*}{\begin{tabular}[c]{@{}c@{}}\textbf{Stylometric}\\   \textbf{features}\end{tabular}} &                              &                                 &                                      \\
&    Jensen Shannon divergence & Normalized Shannon entropy    &     \\
&                               &   &           \\

			
			\bottomrule
			
		\end{tabular}
	\end{adjustbox}
	\label{table:topic_agnostic_features}

\end{table*}

\paragraph{Morphological Features}. This set of features corresponds to the
frequency of morphological patterns in texts.We obtain these grammatical
patterns (e.g., prepositions, adjectives, nouns) through partof-
speech tagging, which assigns each word in a document to a
category based on both its definition and context.

\paragraph{Psychological Features.} Psychological features capture the frequency
of semantic patterns in texts. We obtain the words’ semantics
by using a dictionary that has lists of words that express
psychological processes (personal concerns, affection, perception).

\paragraph{Readability Features.} This set of features captures the ease or difficulty
of comprehending the sentences in the text.We obtain these
features through readability scores (e.g., Gunning-Fog, Coleman-
Liau, Flesch Reading Ease) and character, words, and sentences
usage.




\paragraph{Stylometric features.}
Based on the token frequencies obtained in data pre-processing step, we calculate two probability distribution functions: $p_{i}$ and $\left \langle p_{i} \right \rangle$. The former, which we call individual histogram, corresponds to the probability of a token $t$ appears in a news articles of the class $c$ $\epsilon$ $C$:

\begin{equation}
p_{i}^{(c,t)} = f_{i}^{(c,t)} \Big/ \sum_{i=1}^{N}f_{i}^{(c,t)},
\end{equation}where $N$ corresponds to the total number of tokens.
The latter, which we call reference histogram, is defined as the token average probability over news articles of a class $c$ considered:

\begin{equation}
\left \langle  p_{i}^{(c)}  \right \rangle = \left \langle  f_{i}^{(c)} \right \rangle \Big/ \sum_{i=1}^{N}  \left \langle  f_{i}^{(c)} \right \rangle , 
\end{equation}
with $\left \langle  f_{i}^{(c)} \right \rangle = \sum\limits_{i=1}^{M}f_{i}^{(c,t)}/M$, where $M$ is the total numbers of documents per class $c$.

We have a number of reference histograms equals to $\left | C \right |$ and, for each news articles, $|C|$ individual histograms. We note that we calculate the reference histograms using only the training set (see \ref{classification_def}).

We use the histograms calculated previously to codify each news articles by two information quantifiers: \textbf{entropy} and \textbf{divergence}. The first quantifier is a measure of uncertainty. When applied over words distributions, entropy reflects the spread of the total words of a text among the different words available~\cite{rosso2009shakespeare}. The intuition is that the more random or disordered a text, the richer the vocabulary. In this work, we use the normalized Shannon $H$. To obtain the entropy values for each document, we use the individual histograms. $H$ scores that are closer to $0$ indicate repetitive words' usage patterns and scores closer to $1$ reflect non-uniform patterns.

The second quantifier is Jensen-Shannon divergence which consists of a distance measure between two probability distributions. When applied over words distributions, $JSD$ indicates similarity/dissimilarity between them. Thus, divergence scores closer to $0$ indicates similarity and closer to $1$ dissimilarity. We calculate the divergence scores over the reference and individual histograms of each news articles.

As result of the feature extraction process, we will have each article represented by $33$ morphological, $14$ readability, $93$ psychological and $2\times\left | C  \right |$ stylometric features. Reggarding the stylometric features, they are calculated per class, that is, if the problem has two classes there will be four attributes representing each document in the corpus.


\section{Classification}
\label{classification_def}
We applied supervised learning algorithms to classify news articles. In supervised learning, a training set (documents with known class labels) is provided and used to learn a classification model. Formally, this resulting model corresponds to a function that takes as input a feature vector representing an article $a$ $\epsilon $ $\mathbb{R}^{d}$ and produces an output $\hat{y}$ $\epsilon$ $C$. Once this function is learned, it is used to classify documents with unknown class labels. Here we consider a binary classification problem: given and article $a$, our learning model has to predict whether $a$ is fake or real. %Each document in the corpus of news articles is represented by entropy and divergence values in the dimensional features space. 




\section{Considerações Finais}
Neste capítulo apresentamos a abordagem proposta neste trabalho para identificar \textit{fake news}. Destacamos que nossa abordagem é independente dos contextos/tópicos discutidos, pois considera somente a estrutura sintática do conteúdo das notícias e marcações presentes na estrutura HTML de \textit{webpages}. Nos capítulos seguintes, será apresentada a metodologia experimental que utilizamos para avaliar a nossa abordagem, bem como os resultados alcançados. 

