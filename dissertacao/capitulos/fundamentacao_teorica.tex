\chapter{Theoretical Background}\label{chap:fundamentacao}

In this chapter, we present the conceptual foundations needed for the understanding of our work. We start by discussing fake news definitions used in the literature, and then we decribe several representations methods that are often applied to map text documents to numerical values. We also discuss supervised machine learning algorithms that we employ in our experiments (see Chapter~\ref{chap:experimentos}), as well as evaluation measures.



\section{Notícias Falsas}\label{sec:fake_news}
Na literatura recente, notamos que não há uma definição formal amplamente aceita para o termo notícias falsas (\textit{fake news}). Os trabalhos que descrevem tal conceito adotam a seguinte definição~\citep{allcott2017social,shu2017fake}:

\newtheorem{fake_def}{Definição}

\begin{fake_def}
\label{def:fake_1}
: Notícias falsas são artigos de notícias cujo conteúdo é intencionalmente e verificadamente falso.
\end{fake_def}

Considerando a definição~\ref{def:fake_1}, as seguintes categorias de notícias não são consideradas como falsas: (i) notícias satíricas que não tem intensão de enganar seus leitores e que 
são erroneamente percebidas como factuais; (ii) rumores que não são originados por meio de notícias provenientes de eventos; (iii) teorias da conspiração, pois são difíceis de verificar como falsas ou verdadeiras; e (iv) desinformação que é criada involuntariamente. 


Embora tais categorias não estejam compreendidas na definição anterior, algumas delas promovem informações que tem o potencial de ocasionar males à sociedade. Um exemplo é a teoria da conspiração em que o movimento mundial de anti-vacinação se baseia. Adeptos desse movimento, deixam de vacinar seus filhos por alegarem, dentre outros motivos, que os efeitos colaterais das vacinas são piores do que a doença que elas previnem. Como resultado, países como o Itália, Romênia e Alemanha apresentaram recentemente surtos de doenças (e.g., sarampo) que haviam sido consideradas erradicadas~\citep{clarissa2018}.

Partindo da limitação da definição~\ref{def:fake_1}, elaboramos uma definição mais abrangente em termos de categorias de notícias compreendidas:

\begin{fake_def}
\label{def:fake_2}
: Notícias falsas são artigos de notícias cujo conteúdo promove desinformação independentemente da intenção implícita de seu veiculador, e que não é verificadamente verdadeiro.
\end{fake_def}

A definição~\ref{def:fake_2} engloba as categorias de desinformação \textit{ii}, \textit{iii}, \textit{iv} ao considerar a intenção do autor e a verificabilidade da notícia. Isto é, a notícia é falsa independentemente se foi criada de forma intencional ou não; a menos que esteja claro que a notícia publicada é fabricada. Neste último caso, o responsável pela desinformação é o próprio leitor ou consumidor da notícia. Quanto à verificabilidade, consideramos falsas as notícias em que não é possível checar sua credibilidade como verdadeira. 


Neste trabalho, nós consideramos falsas não apenas notícias provenientes de sites que publicam histórias fabricadas, mas também aquelas que provêm de sites que têm um padrão de manchetes enganosas e que promovem teorias da conspiração. Portanto, consideramos notícias falsas aquelas que promovem desinformação, com exceção de notícias provenientes de sites satíricos. Esta exceção é motivada pelo fato de sites satíricos deixarem claro em suas \textit{homepages} que conteúdos veiculados por eles possuem o objetivo de divertir seus leitores por meio de ironia, sarcasmo e outras figuras de linguagem. No restante deste trabalho, vamos utilizar a expressão \textbf{\textit{fake news}} para nos referirmos a notícias falsas.




\section{Data representation}\label{sec:feat_repr}


Machine learning algorithms take as input a set of data (e.g., images, audio, text) that has been maped into feature vectors in a a multidimensional space. In this section, we will discuss approaches commonly used in the literature for performing this mapping of textual data into numerical vectors. In each subsection, we will present the correspondent feature vectors of the documents in Figure \ref{fig:docs_example}, according with the data representation technique described.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{imagens/documentos_exemplo.pdf}
	\caption{Example of a data collection (adapted from~\cite{baeza2013recuperaccao}).}
	\label{fig:docs_example}
\end{figure} 

\subsection{Bag of Words}

In bag of words representation, documents are mapped to feature vectors belonging to a high-dimensional space, which is determined by the vocabulary size  considered. Each element of these vectors consists of the word frequency $w_{i}$ in document $d{j}$ (see Table~\ref{table:bow_representation}).
\begin{table}[!htb]
	
	\centering
	\footnotesize
	\caption{Feature vectors of documents in Figure~\ref{fig:docs_example} obtained through \textit{bag of words}. The numerical values correspond to word frequencies.}
	\label{table:bow_representation}
	\begin{adjustbox}{width=1\textwidth}
		\begin{tabular}{cccccccccccccc}
			\toprule
			\textbf{Doc.} & \multicolumn{13}{c}{\textbf{Vocabulary Words}}                         \\
			\midrule
			
			& am & be & da & do & is & it & let & not & or & to & what & think & therefore \\
			%\cline{2-14}           
			$d_{1}$  &   0 & 2   &  0  &  2  &  2  &  0   &  0   &  0  & 0  &  4  &  0  & 0  & 0 \\
			$d_{2}$  &   2 & 2   &  0  &  0  &  0  &  0   &  0   &  1  & 1  &  2  &  1  & 0  & 0 \\
			$d_{3}$  &   1 & 2   &  0  &  3  &  0  &  0   &  0   &  0  & 0  &  0  &  0  & 1  & 1 \\
			$d_{4}$  &   0 & 2   &  3  &  3  &  0  &  2   &  2   &  0  & 0  &  0  &  0  & 0  & 0 \\
			\bottomrule
		\end{tabular}
	\end{adjustbox}
\end{table}

If the elements of feature vectors represents the frequency of only one term, it is said that the bag of words is based on unigrams; if they represent two terms, bigramas and so on. This  data representation loses information about the order of terms in the documents, that is, spatial information about the relationship between terms are not captured.


\subsection{Term Frequency and Inverse Document Frequency }
Term Frequency (TF) and Inverse Document Frequency (IDF) are the key concepts of the most popular weighting technique in the field of Information Retrieval, called TF-IDF~\citep{baeza2013recuperaccao}. Since in a text some terms have a greater importance than others, the TF-IDF weights help to define terms that are relevant in a document.


\begin{table}[!hb]
	\centering
	\footnotesize
	\caption{Feature vectors of documents in Figure~\ref{fig:docs_example} obtained through \textit{bag of words}. The numerical values correspond to TF-IDF weights.}
	\label{table:tfidf_representation}
	\begin{adjustbox}{width=1\textwidth}
		
		\begin{tabular}{cccccccccccccc}
			\toprule
			\textbf{Doc.} & \multicolumn{13}{c}{\textbf{Vocabulary Words}}                         \\
			\midrule
			
			& am & be & da & do & is & it & let & not & or & to & what & think & therefore \\
			
			$d_{1}$  & 0.00 & 0.25 & 0.00 & 0.31 & 0.48 & 0.00 & 0.00 & 0.00 & 0.00 & 0.77 & 0.00 & 0.00 & 0.00 \\
			$d_{2}$  & 0.52 & 0.34 & 0.00 & 0.00 & 0.00 & 0.00 & 0.00 & 0.33 & 0.33 & 0.52 & 0.33 & 0.00 & 0.00 \\
			$d_{3}$  & 0.29 & 0.38 & 0.00 & 0.70 & 0.00 & 0.00 & 0.00 & 0.00 & 0.00 & 0.00 & 0.00 & 0.36 & 0.36 \\
			$d_{4}$  & 0.00 & 0.22 & 0.64 & 0.41 & 0.00 & 0.42 & 0.42 & 0.00 & 0.00 & 0.00 & 0.00 & 0.00 & 0.00 \\
			\bottomrule
			
		\end{tabular}
	\end{adjustbox}
	
\end{table}


Analogously to the BOW representation,  TF-IDF projects documents in a mulidimensional space proportional to the vocabulary size. However, in TF-IDF representation documents are mapped into feature vectors, where the elements consist of the multiplication product of TF and IDF. Thus, each document $d$ corresponds to a vector $c_{1}$,...$c_{m}$, where $c_{1}$ is the weighted frequency of a term $i$ in documet $d$, normalized by the frequency of term $i$ in the data set: 


%======= Equação =======
\begin{equation}
\mathbf{c_{i}} = \begin{cases}
\left ( 1 +\log f_{i,d} \right)\times \log\frac{N}{n_{i}}  &, \text{ se }  f_{i,d} > 0\\ 
0 &,  \text{ se } f_{i,d} \leq  0
\end{cases}.
\end{equation}
%========================

In the above equation, the term frequency (first element of the product) is in the logarithmic expression because it makes the weights directly comparable to the weights of the IDF measure. Table \ref{table:tfidf_representation} shows examples of feature vectors that were maped by TF-IDF weighting scheme.


\subsection{Part of Speech Tagging}\label{subsec:postag}

A part of speech is a homogeneous category of words that presents similar properties in gramatical sentences~\citep{fisicaro2018part}. The process of assign a part of speech category (POSTAG) to an word is called \textit{part of speech tagging}. This assignment considers both the word definition and context (relationship between adjascents words in a sentence). In POSTAG representation, each document $d_{j}$ is mapped to a feature vector where the elements are frequencies of a particular part of speech tag (see Table~\ref{table:postag_representation}).

\begin{table}[!htb]
	\centering
	\footnotesize
	\caption{Feature vectors of documents in Figure~\ref{fig:docs_example} obtained through \textit{bag of words}. The numerical values correspond to POSTAG frequency in the documents.}
	\label{table:postag_representation}
	%\begin{adjustbox}{width=0.9\textwidth}
	
	\begin{tabular}{ccccccccccc}
		\toprule
		
		\textbf{Doc.}  & \multicolumn{10}{c}{\textbf{Part of speech category}} \\
		\midrule
		& TO & VB & VBZ & CC & PRP & RB & VBP & WP & NN & VBN \\
		%"to" como preposição ou infinitivo; verbo no infinitivo; 
		%verbo no presente,3ª pessoa, singular; conjunção coordenativa;
		%pronome pessoal, advérbio; verbo no presente, 1ª/2ª pessoa, singular
		%wh-pronome; substantivo comum, singular; verbo no particípio, passado
		
		$d_{1}$  & 4  & 4 & 2 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
		$d_{2}$  & 2  & 2 & 0 & 1 & 2 & 1 & 2 & 1 & 0 & 0\\
		$d_{3}$  & 0  & 4 & 0 & 0 & 2 & 2 & 2 & 0 & 0 & 0\\
		$d_{4}$  & 0  & 8 & 0 & 0 & 2 & 0 & 0 & 0 & 1 & 1\\
		\bottomrule
	\end{tabular}
	%\end{adjustbox}
\end{table}

In Table \ref{table:postag_representation}, TO corresponds to the part of speech category~\textit{``To'' as preposition/infinitive, superlative}; VB to \textit{verb, base form}; VBZ to \textit{verb, present tense, 3rd singular} ; CC as \textit{conjunction, coordinating}; PRP as \textit{personal pronoun}; RB as \textit{adverb}; VBP as \textit{verb, present tense, not 3rd sing}; WP as \textit{wh-pronoun}; NN as \textit{noun, common, singular or mass}; and VBN as \textit{verb, past participle}.


\section{Machine Learning}\label{sec:machine_learning}
Machine Learning (ML) is defined as the set of computational methods that use experience (past input information) to improve the effectiveness of predictions ~\citep{mohri2012foundations}. In \textit{unsupervised} learning, the ML models have as input a set of unlabed data and they must perform predicitons considering all the data set. There is no distinction between training and test data. The goal of this models often is reduce the dimensionality of the ML problem or clustering documents based on similar patterns~\citep{shalev2014understanding}. On the other hand, \textit{supervised} learning models take as input an annotaded set of training samples and they must perform inferences about samples with unknow labels. This type of learning is commonly associated with classification, regression, and ranking problems~\citep{mohri2012foundations} In this work, we consider the automatic fake news detection as 
a supervised learning problem, more specifically, a classification problem (the output is discrete). In what follows, we describe the following supervised ML algorithms: K-Nearest Neighbor, Gaussian Naive Bayes, Support Vector Machine and Random Forest.

\subsection{\textit{k-Nearest Neighbor}}

The k-Nearest Neighbor (kNN) is a classification algorithm on-demand or lazy~\citep{baeza2013recuperaccao}. Lazzy learnign algorithms do not build a classification model \textit{a priori}, thus the inference is performed only when a new document $d_{j}$ is submited to the algorithm.  To classify a class-unknown document $d_{j}$, the kNN classifier algorithm ranks the document’s neighbors among the training document vectors, and uses the class
labels of the $k$ most similar neighbors to predict the class of the new document. The classes of these neighbors are weighted using the similarity of each neighbor to $d_{j}$. Commonly, the Euclidean or Manhattan distance is used as similarity measure~\citep{liao2002use}.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.40\linewidth]{imagens/knn_example.pdf}
	\caption{kNN classification example.}
	\label{fig:knn_example}
\end{figure} 

Figure \ref{fig:knn_example} illustrate an example of a kNN classification decision, when a number of $k=3$ and $k=5$ neighbors are considered. Given a new document $d_{j}$ (green point) and the number of neighbours to be ranked $k=3$, the classifier will assign the red class to $d_{j}$, since two of the 3 nearest points are red. If it considers $k=5$, kNN will assign the blue class.

\subsection{\textit{Gaussian Naive Bayes}}
The Naive Bayes classifier is a simple bayesian network with one root node that
represents the class and n leaf nodes that represent the attributes.
The probabilistic model of naive Bayes classifiers is based on
Bayes’ theorem, and the adjective naive comes from the assumption that the
features in a dataset are mutually independent. In practice, the independence
assumption is often violated, but naive Bayes classifiers still tend to perform
very well~\cite{langley1992analysis}. Especially for small sample
sizes, naive Bayes classifiers can outperform the more powerful alternatives~\cite{lewis1998naive}. 

Let $x_{i}$  be the feature vector of a sample $i$, $i \in \left \{1,2,..,n \right \}$, $c_{j}$ be the notation of a class $j$, $j \in \left \{1,2,..,m \right \}$, and $P \left ( x_{i} | c_{j} \right )$ be the probability of an observing sample $x_{i}$ belongs to class $c_{j}$. The objective function in the naive bayes probability is to maximize the posterior probability over the training data in order to formulate the decision rule:

\begin{equation}
NaiveBayes\left ( x_{i}\right ) \, = \, \arg \max P\left ( c_{j} | x_{i} \right )
\end{equation},where the posterior probability is defined as:

\begin{equation}
P\left ( c_{j} | x_{i} \right ) \, = \, \frac{P\left ( x_{i} | c_{j} \right ) \cdot P\left (c_{j} \right )}{ P\left ( x_{i} \right )}
\end{equation}
 
One typical way to handle continuous attributes in the Naive Bayes classification
is to use Gaussian distributions to represent the likelihoods of the features
conditioned on the classes. Thus each attribute is defined by a Gaussian probability density function (PDF) as:

\begin{equation}
a_{i}  \sim N\left( \mu ,\sigma^{2} \right ) = \frac{1}{\sqrt{2\pi \sigma^{2}} } e^{\frac{a_{i} -\mu }{2\sigma^{2}}}
\end{equation}


\subsection{\textit{Support Vector Machine}}

The Support Vector Machine (SVM) algorithm projects each document in a vector space, where marginal vectors are used to determine the separation space between classes. The basic idea behind the training procedure is to find a hyperplane, represented by vector  $\overrightarrow{w}$, that not only separates
the document vectors in one class from those in the other, but for which the separation, or margin, is as large as possible~\citep{pang2002thumbs}. This search corresponds to a constrained optimization problem. Let $c_{j}$  be the correct class of document $d_{j}$, the solution can be written as: 

\begin{equation}
\overrightarrow{w} =\sum_{j}^{\,}\alpha_{j}c_{j}\overrightarrow{d_{j}} \hspace{1em} \alpha_{j}\geq0,
\end{equation},
where $\alpha_{j}$'s are obtained by solving a dual optimization problem. Those $\overrightarrow{d}_{j}$ such that $\alpha_{j}$ is greater than zero are called support vectors, since they are the only document vectors contributing to $\overrightarrow{w}$. Classification of test instances consists of determining which side of $\overrightarrow{w}$’s hyperplane they fall on.





\subsection{\textit{Random Forest}}

Random Forest (RNF) is an ensemble learning algorithm, i.e., methods that apply some randomness heuristic to generate many learning models and aggregate their results.  In addition to constructing each tree using a different bootstrap sample of the data (bagging heuristic), RNF changes how the classification or regression trees are constructed: each node is split using the best among a subset of predictors randomly chosen at that node~\cite{liaw2002classification}.

Let $\left(X,Y \right),\left ( X_{1},Y_{1} \right),..., \left(X_{n},Y_{n} \right)$ be i.i.d. pairs of random variables such that X (feature
vector) takes its values in $\mathbb{R}^{d}$ while $Y$ (the label) is a binary {0,1}-valued random variable; The collection $\left(X_{1},Y_{1} \right),..., \left(X_{n},Y_{n} \right)$ is called the training data, and is denoted by $D_{n}$. A RNF classifier is given by~\cite{biau2008consistency}:

\begin{equation}
RNF \left(X, Z, D_{n}\right) \, = \,  \begin{cases}
1 &, \text{ if }   \frac{1}{m}\sum_{j=1}^{m}g_{n}\left( X, Z{j}, D_{n}\right )  \geq  \frac{1}{2} \\ 
0 &, \text{otherwise}
\end{cases}.
\end{equation},
where $n$ corresponds to the number of base classifiers, $m$ to the number of features to split each node, $Z$ to a randomized set of feature space; and $gn$ to a base predictor.


\section{Feature Selection}

In a complex classification domain, some features may be irrelevant and others may be redundant ~\cite{chandrashekar2014survey}. These extra features can increase computational time and can have an impact on the system accuracy~\cite{bolon2011feature}. Therefore, selecting important features from input data leads to the simplification of a problem, and faster and more accurate detection rates~\cite{zainal2006feature}. In what follows, we describe a feature selection model called \textit{Information Gain}.


Information gain is frequently employed as term-goodness criterion in the field of machine learning~\cite{mitchell1990machine,quinlan1986induction}. The information gain of a feature t is defined in Eq.~\ref{eq_ig}

\begin{equation}
\label{eq_ig}
\begin{split}
IG(t) = -\sum_{i=1}^{\left | C\right |} P\left(c_{i} \right)logP\left(c_{i} \right) + P\left(t \right) \sum_{i=1}^{\left | C\right |} P\left(c_{i}|t \right)logP\left(c_{i}|t \right)
+  \\ P\left(t' \right) \sum_{i=1}^{\left | C\right |} P\left(c_{i}|t' \right)logP\left(c_{i}|t' \right)
\end{split}
\end{equation}
where $c_{i}$ represents the $i$th category, $P\left(c_{i} \right)$ is the probability of the $i$th category, $P\left(t \right)$ and $P\left(t' \right)$ are the probabilities that the feature t appears or not in the documents, respectively, $P\left(c_{i}|t \right)$ is the conditional probability of the $i$th category given that feature t appeared, and  $P\left(c_{i}|t' \right)$ is the conditional probability of the ith category given that feature t does not appeared. The information gain algorithm produces as output a ranking of features in decreasing order. 

\section{Information Theory Quantifiers}
Quantifiers associated with first-order word statistics and other linguistic elements have been employed to quantify the size, coherence, and distribution of vocabularies in language samples of various types~\cite{rosso2009shakespeare}. Following we describe two information theory quantifiers:

\subsection{Shannon Entropy}
The Shannon entropy can be defined as a measure to quantify the uncertainty of a $p$ distribution~\cite{lesne2014shannon}. Let $x$ be a random variable, with values belonging to a finite set $\chi$, the normalized entropy of $x$ is formulated as:

\begin{equation}
H\left( P \right) = S \left (P \right) / S_{max} = \left ( -\sum_{p_{i}\epsilon P }^{ }p_{i} \log \left ( p_{i}  \right )  \right ) \Big/ S_{max},
\end{equation}where $P= \left  \{ p_{i};i=1,...,N\right \} $, $S \left (P \right)$ denotes the Shannon's entropy and $S_{max}= \log \left(N \right)$ corresponds to the maximum entropy score.

\subsection{Jensen-Shannon Divergence}
This divergence is defined as a measure of distance between two probability distributions~\cite{mimno2009polylingual}. Let $p$ and $q$ be probability distributions, and $S$ be the Shannon entropy, then the Jensen-Shannon divergence is calculated as follows:


\begin{equation}
JSD\left(P,Q\right) = S\left(\frac{P+Q}{2}\right) - \frac{S(P) + S(Q)}{2}.
\end{equation}



\section{Model validation techniques}
To assess the effectiveness of classification models over unseen data, i.e., their generalization ability, we must use a model validation technique. These techniques help to avoid two types of situations: \textit{overfitting} and \textit{underfitting}. The first occurs when the predictive model fits the training data to well, and the second occurs when the predictive model is not able to capture any trend patterns over the trainind data. Both situations leads to poor effectiveness when applied to new data. In this section, we will describe two model validation techniques: leave-one-out and k-fold cross-validation.

\subsection{K-fold cross validation} 
In k-fold cross-validation, sometimes called rotation estimation, the dataset $D$ is randomly split into k mutually exclusive subsets (the folds) $D_{1}$, $D_{2}... D_{k}$ of approximately equal size~\cite{kohavi1995study}. The predictive model is trained and tested $k$ times; each time $t \in {1, 2,..., k}$, it is trained on $D \setminus D_{t}$ and tested on $D_{t}$.  In stratifed cross-validation, the folds are stratified so that they contain approximately the same proportions of labels as the original dataset. 
%

When the amount of data is large, k-fold cross validation should be employed to estimate the accuracy of the model induced from a classification algorithm, because the accuracy resulting from the training data of the model is generally too optimistic~\cite{witten2016data}

\subsection{Leave-one-out cross validation} 
Leave-one-out cross validation (LOOCV) is a special case of k-fold cross validation, in which the
number of folds equals the number of instances~\cite{wong2015performance}. 
LOOCV is normally restricted to applications where the amount of training data available is
severely limited, such that even a small perturbation of the training data is likely to result in a substantial change in
the fitted model~\cite{cawley2003efficient}. Due to be computationally expensive, LOOCV is rarely adopted in large-scale applications.

%Unlike k-fold cross validation, there is no randomness mechanism in the LOOCV.


\section{Effectiveness measures}\label{sec:eval_metrics}

There are several ways of evaluating the effectiveness of learning algorithms. Measures of the quality of classification are built from a confusion matrix which records correctly and incorrectly recognized examples for each class~\cite{sokolova2006beyond}:


\begin{table}[]
	\centering
	\caption{Confusion matrix for binary classification.}
	\label{tab:confusion_matrix}
	\begin{tabular}{c|c|c}
		\toprule
		Real class \textbackslash Recognized as & Positive & Negative \\ \midrule
		Positive                                & TP       & FN       \\\
		Negative                                & FP       & TN       \\ 
		\bottomrule		
	\end{tabular}
\end{table}

Table \ref{tab:confusion_matrix} presents a confusion matrix for binary classification, where \textit{tp} are true positive, \textit{fp} – false positive, \textit{fn} -- false negative, and \textit{tn} -- true negative counts. In this section, we describe three measures commonly use in literature to evaluate the effectiveness of predictive models: precision, recall and f1 scores.

\subsection{Precision}

This metric corresponds to the percentage of items classified as positive that actually are positive~\citep{baeza2013recuperaccao}. It is given by the ratio between the number of true positives and the sum of true and false positives:

\begin{equation}
PR = \frac{\left | TP \right |}{\left | TP \right | + \left | FP \right |}
\end{equation}

\subsection{Recall}
This metric consists of the percentage of positives that are classified as positive. It is formulated as the ration between the true positives and the sum of true positives and false negatives: 

\begin{equation}
RE = \frac{\left | TP \right |}{\left | TP \right | + \left | FN \right |}
\end{equation}

\subsection{F-measure}
 F-measure is the harmonic mean of the precision and recall. It seeks to relate precision and recall metrics in order to obtain a quality measure that balances the relative importance of these two metrics~\citep{baeza2013recuperaccao}. 

\begin{equation}
F1 = \frac{2 \times \left ( PR \times RE \right )}{PR + RE}
\end{equation}

\
\section{Considerações Finais}\label{sec:conclusion}
Neste capítulo foram apresentados o referencial teórico necessário para o desenvolvimento deste trabalho. A abordagem de detecção de \textit{fake news}, bem como os \textit{baselines} utilizados para fins de comparação, foram definidos a partir desses conceitos. 


% \section{Considerações Finais}
% Neste Capítulo foi apresentado um resumo dos conceitos teóricos necessários para entender o desenvolvimento de nosso trabalho. As diversas escolhas realizadas para definir os métodos baseiam-se nestes conceitos. O ferramental teórico estudado possibilita entender os trabalhos relacionados com nossa pesquisa (Capítulo 3).

% Nos próximos Capítulos são apresentados os trabalhos relacionados, nosso método, os resultados da experimentação, os estudos de casos e as conclusões.
