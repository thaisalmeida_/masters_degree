\chapter{Experimentos e Resultados}\label{chap:experimentos}

In this chapter, we give details about the experimental evaluation we conducted 
and discuss the results achieved. Our main goals are to verify the suitability of our content-based features and assess how effective our classifier is at distinguishing fake from real news.
We start by describing the materials and methods we use -- datasets, third party libraries, algorithms and metrics -- , and then, we report results -- using multiple datasets and distinct algorithms
- of our approach in comparison with the baseline.


The experiments were grouped into two sections according with the model evaluation technique we applied. In the first section, we report results obtained over the \textit{Celebrity} and \textit{FakeNewsnet} dataset through \textit{leave-one-out} technique. On the other hand, in the second section, we describe results attained over the \textit{Emergent} and \textit{Fake.br} datasets by using \textit{10-fold cross validation}. 


Previous works have found that fake news often displays a divergence
between the news headline and the body text~\cite{horne2017just,silverman2015lies}: (i) a headline declares a piece of information to be
false and the body text declares it to be true (or vice-versa); and
(ii) fake news packs the main claim of the article into its title,
allowing the reader to skip reading the body article, which tends to
be short, repetitive, and less informative when compared with real
news. These divergences between the textual pieces of news articles
motivated us to apply the classification models at different granularities:
considering only the news headline and only the body text (content).



\section{Materials and Methods}
In this section, we describe the datasets, third party libraries, machine learning algorithms and metrics that we use in our experimental evaluation.


\paragraph{Data.} We evaluate our approach over four datasets that have been used in previous works: \textbf{Celebrity}~\cite{perez@coling2018}, \textbf{Fakenewsnet}~\citep{?}, \textbf{Emergent}~\cite{silverman2015lies} and \textbf{Fake.br}~\cite{?}. For the Fake.br dataset, we translate all the samples from Portuguese to the English language. This was need because to obtain the psycholinguistic features we only have a dictionary with lexicons in English.
Regarding the Emergent dataset, we only consider samples that have the same label for both news headline and content. We also consider that articles labeled as ``reporting'' false rumors are false as well. Table \ref{tab:dist_datasets} shows the class distribution of news articles per dataset.
\begin{table}[h]
	\centering
	\caption[]{Class distribution and statistics for Celebrity and Emergent datasets.}
	\begin{tabular}{l|lccc}
		\toprule
		Dataset                    & Class & \multicolumn{1}{l}{\#samples} & Avg. Sentences &  Words \\ \midrule
		\multirow{2}{*}{Celebrity}  
		& Fake  & 240  &     16        &    8$,$4480     \\
		& Real  & 240   &   19          & 11$,$566     \\
		\midrule
		\multirow{2}{*}{Fakenewnet} 
		& Fake  & 211  &          &        \\
		& Real  & 211   &          &   \\    
		\midrule		
		\multirow{2}{*}{Emergent} 
		& Fake  & 468  &      17    &  12$,$621       \\
		& Real  & 468   &     26     & 12$,$252  \\    
		\multirow{2}{*}{Fake.br} 
		& Fake  & 3,600  &          &       \\
		& Real  & 3,600   &          &  \\    		
		\bottomrule		
	\end{tabular}
	\label{tab:dist_datasets}
\end{table}

\paragraph{Baseline}.
We consider the classification model Fake news Detector (FNDetector), presented in \citet{perez@coling2018}, as our baseline. This model represents documents by using four sets of linguistic features: n-grams (unigrams + bigrams), psychological, readability and syntactical features. The readability and psychological features are the same as our approach uses (see Table \ref{?}). The syntactical features are TF-IDF values of production rules based on context free grammars, i.e., *NN\^{}NP->commission (NN -- a noun -- is the root, NP -- noun phrase -- is the parent node, and commission the child node.
We choose FNDetector because it achieved high effectiveness on fake news identification task; and it shares some additional features sets that we also study. 


\paragraph{Learning algorithms}. We use the following supervised learning algorithms to build the classification models: Support Vector Machine (SVM), K-Nearest Neighbor (KNN), Random Forest (RNF) and Gaussian Naive Bayes (MNB). Our goal is to show that our feature sets can lead to accurate results even with distincts learning strategies (support vectors, ensemble, probabilistic and on-demand).

\paragraph{Third party material}.
We applied the NLTK~\cite{bird2004nltk} part-of-speech tagger to compute morphological features; Stanford Parser~\cite{klein2003accurate} to extract syntactical features; Textstat\footnote{http://pypi.python.org/pypi/textstat/} library to obtain readability metrics, LIWC~\cite{pennebaker2015liwc} library to obtain semantic patterns; and Googletrans~\footnote{https://pypi.org/project/googletrans/} to translate the samples of Fake.br dataset. We use the implementations of machine learning algorithms of Scikit-learn\footnote{http://scikit-learn.org/} library with default parameters. 

\paragraph{Model evaluation.}
We perform our evaluations using stratified ten fold cross-validation and leave-one out techniques. Due to Fakenewsnet and Celebrity datasets have a relativetely small number of samples, we applied leave-one out in our classification proccess over these datasets. On the second hand, we use stratified ten fold cross-validation over the Fake.br and Emergente datasets because they present a lager number of samples.


\paragraph{Effectiveness Metrics}. To measure the quality of the classification models, we use the measures Precision (PR), Recall (RE), F-measure (F1) and F-measure macro (F1-macro). The collected results are complemented with confidence intervals of $\alpha = 95$\%.

\paragraph{Hardware setup}
We run our experiments in a MacOs (high sierra version) operation system with a Intel Core i7 processor of 2.5GHz and memory RAM of 16GB.
 


\section{Leave-one out: Celebrity and Fakenewsnet}
In this section, we report the classification results that we obtain over Celebrity and Fakenewsnet corpus by using leave-one out model evaluation technique. As we discuss in Chapter~\ref{} (Section~\ref{}), the loo does not introduce randomness, thus we can note complement the results with confidence intervals.

Before we compare LiarDetector with the baseline, we first verify which n-gram granularity is better to extract the stylometric component of our approach. Table~\ref{tab:celebrity_stylometric} and~\ref{tab:newsnet_stylometric} shows the F-measure values attained by unigrams, bigrams and both when the stylometric features are computed over the news headlines and contents of Celebrity and Fakenewsnet dataset, respectivetly.

%gain(a,b) = (a-b)/b *100

We can note in Table~\ref{tab:celebrity_stylometric} that, considering the \textbf{headline}, the unigrams representation achieves gains values of at least to 2.32\% for the real class when compared with bigrams and unigrams+bigrams. For the fake class, the bigrams representation attains superior values when combined with KNN and SVM -- gains of 4.6\% and 14.6\%, respectively. Thus, we choose the \textbf{unigrams} representaion to extract the stylometric features of the news \textbf{headline}, since it is presents a better compromise between the two classes. Analogously, we choose the \textbf{bigrams} representation to extract the stylometric features of the \textbf{news contents}. Nevetheless the unigrams representation achives higher F1 values for the fake class with all the algorithms, it presents poor F1 values with SVM and GNB for the real class.  

Reggarding the Fakenewsnet dataset, we selected the \textbf{unigrams+bigrams} and \textbf{unigrams} representation to extract the stylometric features for the \textbf{ headlines} and \textbf{contents} of news, respectively. We can observe in Table~\ref{tab:newsnet_stylometric} that, with respect to the headlines, the unigrams+bigrams representation achieves F1 values gains up to 31\% -- with GNB -- for the real class, and, although it attains F1 value of 11.6\% inferior to the bigrams representation -- with RFN --, it presents a better compromise between the two classes. For the news content, we can see that the unigrams+bigrams achieves higher F1 values -- gains of at least 2.7\%-- for both classes with SVM; the bigrams attains higher F1 values for the real class with KNN and GNB, and for the fake class with GNB and RNF; and the unigrams attains better F1-macro values for the KNN and RNF  -- 0.70\% and 69\% respectively.
  
  
 Once we have selected the n-grams granularaties for calculating the stylometric set of features, we are able to combine it with the morphological, psychological and readability sets to build our clasification approach. As describe in Chapter \ref{chap:proposed_approach}, to build our classifier with the most relevant features, we apply a variation of the Information Gain algorithm. Figure \ref{fig:celebrity_feature_selection} and \ref{fig:newsnet_feature_selection} show the results obtained by LiarDetector when we consider 25\%, 50\%, 75\% and 100\% of the most relevant features over Celebrity and Fakenewsnet datasets, respectively.
 
 We can see in Figure \ref{fig:celebrity_headline_feature_selection} that LiarDetector achieves and overall higher F1-macro values -- 0.61, 0.60, 0.63, 0.53 for SVM, KNN, GNB and RNF-- when we consider \textbf{100\%} of the most relevant features extracted from the news headline. With respect to the news content (Figure \ref{fig:celebrity_content_feature_selection}), LiarDetector is more accurate when it uses \textbf{25\%} of the most relevant features -- 0.64, 0.66, 0.72, 0.52 for SVM, KNN, GNB and RNF. 
 
 
 Table \ref{tab:celebrity_final_results} shows the results attained by the LiarDetector and FNDetector over the Celebrity dataset considering features extracted from the headline and content of news. For the headline, LiarDetector achieves gains of 74.3\%  and 73.5\% for the fake and real classes -- with GNB -- when compared to the baseline. On the other hand, when combined with RNF and SVM, FNDetector outperforms our approach -- 3.2\% of gains with the SVM and 6\% and 25\% of gains with the RNF. When the KNN is used, both classification models have the same effectiveness (in terms of F1 values). Concerning to the news content, our approach overperforms the baseline when it is combined with KNN and GNB. The F1 gains values are of at least 4.6\% for the real and 4.9\% for the fake class. Both approaches achieved equal effectiveness using the SVM algorithm. Regardding RNF, the baseline outperforms LiarDetector in both classes. Therefore, the \textbf{most accurate} classifiers is \textbf{LiarDetector+GNB} for both news headline and content. 
  
  
 We can see in Figure \ref{fig:newsnet_headline_feature_selection} that LiarDetector achieves and overall higher F1-macro values -- 0.62, 0.61, 0.74, 0.70 for SVM, KNN, GNB and RNF-- when we consider \textbf{75\%} of the most relevant features extracted from the news headline. With respect to the news content (Figure \ref{fig:newsnet_content_feature_selection}), LiarDetector is more accurate when it uses \textbf{25\% }of the most relevant features -- 0.63, 0.64, 0.70, 0.73 for SVM, KNN, GNB and RNF.   
  
Reggarding the Fakenewsnet dataset, the most effective models are \textbf{FNDetector+ RNF}, \textbf{LiarDetector+GNB}, for the news headline,  and \textbf{FNDetector+RNF} for the content. In Table~\ref{tab:newsnet_final_results}, we can notice that our approach attains higher F1 values than the baseline -- for the headlines-- when it uses the KNN and GNB algorithms, however the baseline outperforms LiarDetector when combined with SVM and RNF. Therefore, the combination FNDetector+ RNF and LiarDetector+GNB attain equals values of F1-macro 0.74\%. With respect to the news content, our approach outperforms the baseline with all algorithms, except RNF -- F1 gains values of at least 4.1\% and 5.2\% for the fake and real classes. The baseline combined with RNF attain a higher F1 value (0.78\%) for the fake class.
  
  
Figure~\ref{fig:loocv_time_execution} shows the execution time of training and testing FNDetector and LiarDetector classifiers over the Celebrity and Fakenewsnet datasets. We can note that in all scenarios, our approach is faster than the baseline. Considering the Celebrity corpus, LiarDetector achieve execution times up to 33 times faster than the baseline during the training -- with GNB --, and 9 times faster during the testing (with KNN) using features derived from the news headlines. With respect to the content, our approach is up to 354x and 16x faster than the baseline during the training and testing process. Therefore, we can conclude that that the classifier which guarantee the best compromise between effectiveness and efficiency is the \textbf{LiarDetector+GNB} no matter the text granularity considered.
  

Reggarding the Fakenewsnet corpus, our approach achieved execution times up to 72x faster than the baseline during the training (with GNB), and 4 times faster during the testing (with KNN) using features extracted from the news headlines. For the content, our approach is up to 662 and 32 times faster than
FNDetector during the training and testing process. We then can conclude that the classifier which guarantee the best compromise between effectiveness and efficiency is also the \textbf{LiarDetector+GNB} no matter the text granularity considered. 


\section{Cross validation: Emergent and Fake.br}
In this section, we report the classification results that we obtain over Emergent and Fake.br corpus by using stratified 10 fold cross validation model evaluation technique. Since  cross validation introduces randomness we have to complement the results with confidence intervals. In this case, we have three possible situations when comparing two means: (i) \textbf{if there is no overlap across the means, the higher mean determines the the most  accurate classifier}; (ii) \textbf{if there is overlap across the means and each confidence interval comprises the other mean, both classifiers are equally effective}; and (iii) \textbf{if there is overlap and one mean is not comprised in the other mean confidence interval, we must perform t-test}. Reggarding the t-test, we define the null and alternate hypotheses as follows:
\begin{itemize}
	\item	\textbf{Null Hypothesis (H0):} There is no significant difference
between means values attained with LiarDetector and FNDetector approaches.
\item \textbf{Alternate Hypothesis (H1):} There is a significant difference
between means values attained with LiarDetector and FNDetector approaches.
\end{itemize}



Analogously to the previous section, before apply the feature selection process, we first verify which n-gram granularity is better to extract the stylometric component of our approach. Table~\ref{tab:silverman_stylometric} and~\ref{tab:fakebr_stylometric} shows the F-measure values attained by unigrams, bigrams and both when the stylometric features are computed over the news headlines and contents of Emergent and Fake.br dataset, respectivetly.

Considering the news headlines of the Emergent dataset, the combinations unigrams+SVM and unigrams+bigrams+SVM are equally accurate for the both classes. Unigrams+bigrams+SVM achieves higher values than bigrams+SVM for the fake an real classes ($p = 0.018 < 0.05$) -- gains of 7\% and 6.3\%, respectively. The combination unigrams+SVM also attains more accurate values than bigrams+SVM -- $p = 0.07 < 0.05$ and $p = 0.017 < 0.05$ for fake and real classes. Therefore, we can conclude that when combined with the SVM classifier the best n-grams to use are unigrams and unigrams+bigrams.

When we examine the KNN results for the news headlines in Table~\ref{tab:silverman_stylometric}, the unigrams and unigrams+bigrams present the same ability in distinguishing fake from real samples. 
For the fake class, bigrams are less effective than unigrams+bigrams  and unigrams ($p = 0.04 < 0.05$). For the real class, bigrams are equally accurate to unigrams ($p = 0.06 > 0.05$), but they attains lower values than unigrams+bigrams ($p = 0.012 < 0.05$). Hence, we can conclude that when combined with the KNN classifier the best n-grams to use are unigrams+bigrams. 

With respect to the GNB results for the news headlines, unigrams, bigrams and unigrams+bigrams are equally accurate. All the combinations have overlaps that comprises the other means, except unigrams+bigrams and bigrams for the fake class. In this case, we can not reject the null hypothesis because $p = 0.09 > 0.05$.

Reggarding the RNF results for the news headlines, unigrams+bigrams obtains higher F1 values than bigrams with gains of 5\% and 2.5\% for fake and real classes. Unigrams and unigrams+bigrams are equally accurate for both classes. Unigrams present higher F1 values than the bigrams for the real class, but they present the same effectiveness for fake class ($p=0.16 > 0.05$). Thus, we can conclude that when combined with the RNF classifier the best n-grams to use are unigrams+bigrams.  

Considering the overall effectivenness of all algorithms, we choose the unigrams+bigrams representation to extract the stylometric features of news headlines. Figure~\ref{fig:emergent_headline_feature_selection} shows the results abtained by LiarDetector when we consider 25\%, 50\%, 75\%, 100\% of the most relevant features over the Emergent dataset. With exception of SVM with 25\% and GNB with 100\%, all the other scenarios present overlaps that results in equally accurate classifiers. Thus, we choose the threshold with less features (25\%) to compare with the baseline.

We can see in Table \ref{tab:silverman_final_results} that for the SVM and KNN algorithms, FNDetector and LiarDetector have the same ability of distinguish between fake and real news samples. For the GNB, our approach and the baseline are equally accurate considering the fake class, but FNDetector outperforms Liardetector in the real class ($p = 0.004 <0.05$). With respect to the RNF, the both models also achieved similar F1 values for the fake and real class ($p = 0.07 > 0.05$).

With respect to the news content of the Emergent dataset, we can see in Table~\ref{tab:silverman_stylometric} that the bigrams representation attains the higher F1 values when combined with all the learning algorithms. We then choose bigrams  to extract the stylometric features of news content. Figure~\ref{fig:emergent_content_feature_selection} shows the results abtained by LiarDetector when we consider 25\%, 50\%, 75\%, 100\% of the most relevant features over the Emergent dataset. With exception of the scenario that considers 100\% of relevant features, all the others scenarios present overlaps that results in equally accurate classifiers. Thus, we choose the threshold with less features (25\%) to compare with the baseline.

We can see in Table \ref{tab:silverman_final_results} that LiarDetector achieves higher F1 values than the baseline when combined with KNN and RNF algorithms. The F1 gains for the fake class is up to of 16.6\% and, for the real class is 19.6\% . Reggarding the SVM and GNB algorithms, both models presents similar effectiveness.


Figure~\ref{fig:cv_time_execution} shows the execution time of training and testing FNDetector and LiarDetector classifiers over the Emergent and Fakebr datasets. We can note that in all scenarios, our approach is faster than the baseline. Considering the Emergent corpus, LiarDetector achieve execution times up to 146 times faster than the baseline during the training (with GNB) --, and 320 times faster during the testing (with KNN) using features derived from the news headlines. With respect to the content, our approach is up to 2899x and 1933x  faster than the baseline during the training and testing process. Therefore, we can conclude that that the classifier which guarantee the best compromise between effectiveness and efficiency is the \textbf{LiarDetector+GNB} when the features were computed using the news headlines, and  \textbf{LiarDetector+RNF} using the news contents.


% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[]
\centering		
\caption{Stylometric features F1 scores with distinct combinations of n-grams representations and algorithms over the Celebrity dataset.}
\label{tab:celebrity_stylometric}
\begin{adjustbox}{width=1\textwidth}		
	
	\begin{tabular}{c|c|cc|cc|cc|cc}
		\toprule
		\multirow{2}{*}{\textbf{Text}}     & \multirow{2}{*}{\textbf{Stylometric Features}} & \multicolumn{2}{c|}{\textbf{Support Vector Machine}}          & \multicolumn{2}{c|}{\textbf{K-Nearest Neighbor}}              & \multicolumn{2}{c|}{\textbf{Gaussian Naive Bayes}}            & \multicolumn{2}{c}{\textbf{Random Forest}}                   \\
		& & \textbf{Fake }& \textbf{Real} &\textbf{Fake} & \textbf{Real}&\textbf{Fake} & \textbf{Real} & \textbf{Fake} & \textbf{Real} \\
		\midrule
		\multirow{3}{*}{\textbf{Headline}}  
		& Unigrams &\textbf{0.43} & \textbf{0.44} & \textbf{0.43} & \textbf{0.38} & \textbf{0.41} & \textbf{0.39} & \textbf{0.45} & \textbf{0.37}\\
		& Bigrams & 0.41 & 0.43 & \textbf{0.45} & 0.24 & \textbf{0.47} & 0.19 & 0.42 & 0.28\\
		& Unigrams+Bigrams&0.38 & 0.41 & 0.41 & 0.36 & 0.39 & 0.33 & 0.45 & 0.32\\
		 \midrule
		\multirow{3}{*}{\textbf{Content}}   
		& Unigrams & \textbf{0.65} &0.26   &\textbf{0.62} &\textbf{0.67}   &0.62 &0.34   &\textbf{0.58} &0.57\\
		& Bigrams & \textbf{0.50}  &\textbf{0.64}   &\textbf{0.48} &\textbf{0.61}   &\textbf{0.56} &\textbf{0.63}   &\textbf{0.54} &\textbf{0.49}\\
		& Unigrams+Bigrams& 0.59 &0.52   &0.37 &0.60   &0.58 &0.39   &0.57 &0.5\\		
		\bottomrule                       
	\end{tabular}
\end{adjustbox}
\end{table}

\begin{table}[]
	\centering		
	\caption{Stylometric features F1 scores with distinct combinations of n-grams representations and algorithms over the Fakenewsnet dataset.}
	\label{tab:newsnet_stylometric}
	\begin{adjustbox}{width=1\textwidth}		
		
		\begin{tabular}{c|c|cc|cc|cc|cc}
			\toprule
		\multirow{2}{*}{\textbf{Text}}     & \multirow{2}{*}{\textbf{Stylometric Features}} & \multicolumn{2}{c|}{\textbf{Support Vector Machine}}          & \multicolumn{2}{c|}{\textbf{K-Nearest Neighbor}}              & \multicolumn{2}{c|}{\textbf{Gaussian Naive Bayes}}            & \multicolumn{2}{c}{\textbf{Random Forest}}                   \\
& & \textbf{Fake }& \textbf{Real} &\textbf{Fake} & \textbf{Real}&\textbf{Fake} & \textbf{Real} & \textbf{Fake} & \textbf{Real} \\
\midrule

			\multirow{3}{*}{\textbf{Headline}}  
			& Unigrams & 0.64 &0.68 &\textbf{0.63} &0.69 &0.59 &0.65 &\textbf{0.64} &0.64\\
			& Bigrams &\textbf{0.67} &0.53 &0.56 &0.68 &\textbf{0.68} &0.53 &\textbf{0.67} &0.55\\
			& Unigrams+Bigrams&\textbf{0.64} &\textbf{0.68} &\textbf{0.62} &\textbf{0.71} &\textbf{0.6 }&\textbf{ 0.70}  &\textbf{0.6}  &\textbf{0.67}\\
			\midrule
			\multirow{3}{*}{\textbf{Content}}   
			& Unigrams & 0.65 &0.50  &\textbf{0.69} &\textbf{0.71} &0.65 &0.47 &\textbf{0.68} &\textbf{0.71}\\
			& Bigrams &0.60 &0.74 &0.63 &\textbf{0.75 }&\textbf{0.67} &\textbf{0.75 }&\textbf{0.71} &0.61\\
			& Unigrams+Bigrams&\textbf{0.68} &\textbf{0.76} &0.62 &\textbf{0.74 }&0.66 &0.51 &0.70 &0.63\\
			\bottomrule                       
		\end{tabular}
	\end{adjustbox}
\end{table}


\begin{figure}[!htb]
\centering
\begin{subfigure}{0.50\textwidth} 
  \includegraphics[width=\linewidth]{imagens/plot_res_celebrity_headline.pdf}
  \caption{Headline results.}
  \label{fig:celebrity_headline_feature_selection}
\end{subfigure}\hfill
\begin{subfigure}{0.50\textwidth}
  \includegraphics[width=\linewidth]{imagens/plot_res_celebrity_content.pdf}
 \caption{Content results.}
  \label{fig:celebrity_content_feature_selection}
\end{subfigure}
 \caption{LiarDetector results over the Celebrity dataset considering different percentages of relevant features.}
  \label{fig:celebrity_feature_selection}
\end{figure}





\begin{figure}[!htb]
	\centering
	\begin{subfigure}{0.50\textwidth} 
		\includegraphics[width=\linewidth]{imagens/plot_res_newsnet_headline.pdf}
		\caption{Headline results.}
  \label{fig:newsnet_headline_feature_selection}
	\end{subfigure}\hfill
	\begin{subfigure}{0.50\textwidth}
		\includegraphics[width=\linewidth]{imagens/plot_res_newsnet_content.pdf}
		\caption{Content results.}
  \label{fig:newsnet_content_feature_selection}
	\end{subfigure}
	\caption{LiarDetector results over the Fakenewsnet dataset considering different percentages of relevant features..}
  \label{fig:newsnet_feature_selection}
\end{figure}



\begin{table}[]
\centering		
\caption{Classification results of models trained over Celebrity dataset. PR corresponds to precision, RE to recall and F1 to F-measure.}
\label{tab:celebrity_final_results}	
\begin{adjustbox}{width=1\textwidth}		

	\begin{tabular}{c|c|c|cc|cc|cc|cc}
		\toprule
		\multirow{2}{*}{Text} & \multirow{2}{*}{Approach}                       & \multirow{2}{*}{Metric} & \multicolumn{2}{c|}{Support Vector Machine}  & \multicolumn{2}{c|}{K-Nearest Neighbor}      & \multicolumn{2}{c|}{Gaussian Naive Bayes}             & \multicolumn{2}{c}{Random Forest}           \\
		&                                                 &                         & Fake                 & Real                 & Fake                 & Real                 & Fake                 & Real                 & Fake                 & Real                 \\ \midrule
		\multirow{6}{*}{Headline}   & \multirow{3}{*}{H+JSD}                          
		&PR&0.62 & 0.61 & 0.61 & 0.6 & 0.61 & 0.67 & 0.54 & 0.55\\
		&&RE&0.61 & 0.62 & 0.58 & 0.63 & 0.75 & 0.52 & 0.67 & 0.41\\
		&&F1&0.62 & 0.61 & \textbf{0.60} & \textbf{0.61} & \textbf{0.68} & \textbf{0.59} & 0.6 & 0.47\\
		\cmidrule{2-11} 
		 & \multirow{3}{*}{FNDetector}                          
	&PR&0.64 & 0.64 & 0.61 & 0.6 & 0.38 & 0.35 & 0.61 & 0.63\\
	&&RE&0.65 & 0.62 & 0.58 & 0.63 & 0.4 & 0.33 & 0.67 & 0.55\\
	&&F1&\textbf{0.64} &\textbf{ 0.63} & \textbf{0.60 }&\textbf{ 0.61} & 0.39 & 0.34 & \textbf{0.64 }&\textbf{ 0.59}\\
		\midrule
		\multirow{6}{*}{Content}    & \multirow{3}{*}{H+JSD} 
&PR&0.64 & 0.65 & 0.64 & 0.68 & 0.68 & 0.79 & 0.59 & 0.58\\
&&RE&0.65 & 0.64 & 0.71 & 0.61 & 0.84 & 0.61 & 0.56 & 0.61\\
&&F1&\textbf{0.65} & \textbf{0.64} & \textbf{0.68 }& \textbf{0.64} & \textbf{0.75} & \textbf{0.69} & 0.57 & 0.59\\
		\cmidrule{2-11} 
		 & \multirow{3}{*}{FNDetector}                      
		  & PR   & 0.64 & 0.65 & 0.61 & 0.65 & 0.52 & 0.71 & 0.66 & 0.74\\
		 && RE & 0.67 & 0.62 & 0.7 & 0.56 & 0.94 & 0.14 & 0.79 & 0.59 \\
		 && F1 & \textbf{0.65} & \textbf{0.64} & 0.65 & 0.61 & 0.67 & 0.23 & \textbf{0.72} & \textbf{0.65} \\
	\bottomrule
	\end{tabular}
\end{adjustbox}
\end{table}




\begin{table}[]
	\centering		
	\caption{Newsnet results}
\label{tab:newsnet_final_results}		
	\begin{adjustbox}{width=1\textwidth}		
		
		\begin{tabular}{c|c|c|cc|cc|cc|cc}
			\toprule
			\multirow{2}{*}{Text} & \multirow{2}{*}{Approach}                       & \multirow{2}{*}{Metric} & \multicolumn{2}{c|}{Support Vector Machine}  & \multicolumn{2}{c|}{K-Nearest Neighbor}      & \multicolumn{2}{c|}{Gaussian Naive Bayes}             & \multicolumn{2}{c}{Random Forest}           \\
			&                                                 &                         & Fake                 & Real                 & Fake                 & Real                 & Fake                 & Real                 & Fake                 & Real                 \\ 
			\midrule
			\multirow{6}{*}{Headline}   & \multirow{3}{*}{H+JSD}                          
				&PR&0.62 & 0.62 & 0.60 & 0.63 & 0.73 & 0.76 & 0.67 & 0.76\\
				&&RE&0.63 & 0.61 & 0.68 & 0.54 & 0.77 & 0.72 & 0.82 & 0.59\\
				&&F1&0.62 & 0.62 &\textbf{0.64} & \textbf{0.58} & \textbf{0.75} & \textbf{0.74} & 0.73 & 0.67\\
			\cmidrule{2-11} 
			& \multirow{3}{*}{FNDetector}                          
			&PR&0.65 & 0.64 & 0.59 & 0.63 & 0.7 & 0.69 & 0.71 & 0.79\\
			&&RE&0.64 & 0.65 & 0.69 & 0.53 & 0.69 & 0.71 & 0.82 & 0.67\\
			&&F1&\textbf{0.64 }& \textbf{0.65} & 0.64 & 0.57 & 0.70 & 0.70 & \textbf{0.76} & \textbf{0.73}\\
			\midrule
			\multirow{6}{*}{Content}    & \multirow{3}{*}{H+JSD} 
			&PR&0.64 & 0.63 & 0.66 & 0.63 & 0.68 & 0.77 & 0.74 & 0.74\\
			&&RE&0.69 & 0.58 & 0.67 & 0.63 & 0.85 & 0.57 & 0.77 & 0.71\\
			&&F1&\textbf{0.66} & \textbf{0.60} & \textbf{0.66} & \textbf{0.63} &\textbf{0.75} & \textbf{0.65} & 0.75 & 0.72\\
			\cmidrule{2-11} 
			& \multirow{3}{*}{FNDetector}                      
			&PR&0.61 & 0.58 & 0.58 & 0.54 & 0.6 & 0.76 & 0.74 & 0.78\\
			&&RE&0.62 & 0.56 & 0.57 & 0.54 & 0.9 & 0.34 & 0.83 & 0.68\\
			&&F1&0.61 & 0.57 & 0.58 & 0.54 & 0.72 & 0.47 & \textbf{0.78} & \textbf{0.72}\\
			\bottomrule
		\end{tabular}
	\end{adjustbox}
\end{table}







\begin{figure}[!htb]
	\centering
	\begin{subfigure}{0.50\textwidth} 
		\includegraphics[width=\linewidth]{imagens/plot_res_silverman_headline.pdf}
		\caption{Headline results.}
  \label{fig:emergent_headline_feature_selection}
	\end{subfigure}\hfill
	\begin{subfigure}{0.50\textwidth}
		\includegraphics[width=\linewidth]{imagens/plot_res_silverman_content.pdf}
		\caption{Content results.}
  \label{fig:emergent_content_feature_selection}
	\end{subfigure}
	\caption{LiarDetector results over the Emergent dataset considering different percentages of relevant features.}
  \label{fig:emergent_feature_selection}
\end{figure}

\begin{table}[]
	\centering		
	\caption{Silverman results}
	\begin{adjustbox}{width=1\textwidth}		
		
		\begin{tabular}{c|c|cc|cc|cc|cc}
			\toprule
			\multirow{2}{*}{Text}     & \multirow{2}{*}{Stylometric Features} & \multicolumn{2}{c|}{Support Vector Machine}          & \multicolumn{2}{c|}{K-Nearest Neighbor}              & \multicolumn{2}{c|}{Gaussian Naive Bayes}            & \multicolumn{2}{c}{Random Forest}                   \\
			&                                       & Fake & Real &Fake & Real&Fake & Real & Fake & Real \\
			\midrule
			\multirow{3}{*}{Headline}  
& Unigrams &0.83$\pm$0.02 & 0.84$\pm$0.02 & 0.84$\pm$0.03 & 0.84$\pm$0.03 & 0.79$\pm$0.03 & 0.80$\pm$0.03 & 0.81$\pm$0.03 & 0.81$\pm$0.03\\
& Bigrams &0.78$\pm$0.03 & 0.79$\pm$0.03 & 0.77$\pm$0.04 & 0.80$\pm$0.03 & 0.76$\pm$0.04 & 0.80$\pm$0.03 & 0.77$\pm$0.05 & 0.79$\pm$0.03\\
& Uni+Bigrams &0.84$\pm$0.02 & 0.84$\pm$0.02 & 0.83$\pm$0.03 & 0.83$\pm$0.03 & 0.80$\pm$0.03 & 0.82$\pm$0.03 & 0.81$\pm$0.04 & 0.81$\pm$0.04\\
			\midrule
\multirow{3}{*}{Content}  			

& Unigrams&0.72$\pm$0.01 &  0.41$\pm$0.04 &  0.78$\pm$0.02 &  0.78$\pm$0.02 &  0.65$\pm$0.03 &  0.45$\pm$0.04 &  0.78$\pm$0.03 &  0.75$\pm$0.04\\
& Bigrams&0.85$\pm$0.03 &  0.85$\pm$0.03 &  0.84$\pm$0.03 &  0.84$\pm$0.03 &  0.82$\pm$0.04 &  0.84$\pm$0.03 &  0.82$\pm$0.03 &  0.79$\pm$0.04\\
& Unigrams+bigrams&0.54$\pm$0.09 &  0.60$\pm$0.10 &  0.61$\pm$0.09 &  0.53$\pm$0.14 &  0.63$\pm$0.06 &  0.36$\pm$0.18 &  0.61$\pm$0.07 &  0.50$\pm$0.14\\

			\bottomrule                       
		\end{tabular}
	\end{adjustbox}
\end{table}





\begin{table}[]
	\centering		
	\caption{Silverman vs baseline results}
 \label{tab:silverman_final_results}	
	\begin{adjustbox}{width=1\textwidth}		
		
		\begin{tabular}{c|c|c|cc|cc|cc|cc}
			\toprule
			\multirow{2}{*}{Text} & \multirow{2}{*}{Approach}                       & \multirow{2}{*}{Metric} & \multicolumn{2}{c|}{Support Vector Machine}  & \multicolumn{2}{c|}{K-Nearest Neighbor}      & \multicolumn{2}{c|}{Gaussian Naive Bayes}             & \multicolumn{2}{c}{Random Forest}           \\
			&                                                 &                         & Fake                 & Real                 & Fake                 & Real                 & Fake                 & Real                 & Fake                 & Real                 \\ \midrule
			\multirow{6}{*}{Headline}   & \multirow{3}{*}{LiarDetector}                          
			& PR   &0.72$\pm$0.10 & 0.75$\pm$0.13 & 0.63$\pm$0.05 & 0.65$\pm$0.06 & 0.78$\pm$0.05 & 0.83$\pm$0.04 & 0.83$\pm$0.02 & 0.79$\pm$0.06\\
			&& RE   &0.62$\pm$0.28 & 0.66$\pm$0.21 & 0.67$\pm$0.07 & 0.61$\pm$0.06 & 0.84$\pm$0.05 & 0.75$\pm$0.07 & 0.76$\pm$0.10 & 0.84$\pm$0.03\\
			&& F1&\textbf{0.56$\pm$0.20} & \textbf{0.62$\pm$0.08} & \textbf{0.65$\pm$0.05} & \textbf{0.63$\pm$0.05} &\textbf{ 0.81$\pm$0.03} & 0.79$\pm$0.04 & \textbf{0.79$\pm$0.05} & \textbf{0.81$\pm$0.03}\\
			\cmidrule{2-11} 
			& \multirow{3}{*}{FNDetector}                          
			& PR&0.72$\pm$ 0.09 & 0.74$\pm$ 0.10 & 0.66$\pm$ 0.04 & 0.68$\pm$ 0.06 & 0.83$\pm$ 0.03 & 0.82$\pm$ 0.03 & 0.73$\pm$ 0.03 & 0.86$\pm$ 0.03\\
			&& RE&0.66$\pm$ 0.19 & 0.67$\pm$ 0.22 & 0.69$\pm$ 0.07 & 0.64$\pm$ 0.05 & 0.81$\pm$ 0.04 & 0.84$\pm$ 0.03 & 0.88$\pm$ 0.03 & 0.67$\pm$ 0.06\\
			&& F1&\textbf{0.64$\pm$ 0.08} & \textbf{0.62$\pm$ 0.13} &\textbf{ 0.67$\pm$ 0.05} & \textbf{0.66$\pm$ 0.04} & \textbf{0.82$\pm$ 0.03} & \textbf{0.83$\pm$ 0.02} & \textbf{0.80$\pm$ 0.02} & \textbf{0.75$\pm$ 0.03}\\
			\midrule
			\multirow{6}{*}{Content}   & \multirow{3}{*}{LiarDetector}                          			
&PR&0.78$\pm$0.12 & 0.81$\pm$0.11 & 0.68$\pm$0.04 & 0.64$\pm$0.03 & 0.78$\pm$0.04 & 0.74$\pm$0.04 & 0.79$\pm$0.03 & 0.84$\pm$0.05\\
&&RE&0.75$\pm$0.20 & 0.68$\pm$0.22 & 0.59$\pm$0.06 & 0.71$\pm$0.06 & 0.71$\pm$0.06 & 0.80$\pm$0.04 & 0.84$\pm$0.06 & 0.76$\pm$0.05\\
&&F1&\textbf{0.70$\pm$0.12} & \textbf{0.67$\pm$0.14} & \textbf{0.63$\pm$0.04} & 0.67$\pm$0.03 & \textbf{0.74$\pm$0.04} & \textbf{0.76$\pm$0.03} & \textbf{0.81$\pm$0.03} & \textbf{0.80$\pm$0.03}\\
			\cmidrule{2-11} 
			& \multirow{3}{*}{FNDetector}                          
			&PR&0.72$\pm$0.02 & 0.71$\pm$0.01 & 0.56$\pm$0.03 & 0.55$\pm$0.02 & 0.73$\pm$0.04 & 0.78$\pm$0.03 & 0.65$\pm$0.03 & 0.72$\pm$0.03\\
			&&RE&0.70$\pm$0.04 & 0.73$\pm$0.02 & 0.52$\pm$0.04 & 0.58$\pm$0.03 & 0.81$\pm$0.05 & 0.69$\pm$0.05 & 0.78$\pm$0.04 & 0.58$\pm$0.06\\
			&&F1&\textbf{0.71$\pm$0.02} &\textbf{ 0.72$\pm$0.01} & 0.54$\pm$0.03 & 0.56$\pm$0.02 & \textbf{0.76$\pm$0.03} & \textbf{0.73$\pm$0.03} & 0.71$\pm$0.03 & 0.64$\pm$0.03\\			
			\bottomrule
		\end{tabular}
	\end{adjustbox}
\end{table}




\begin{figure}[!htb]
	\centering
	\begin{subfigure}{0.24\textwidth} 
		\includegraphics[width=\linewidth]{imagens/plot_execution_time_celebrity_headline_train.pdf}
		\caption{Celebrity: training time for headline.}
		\label{fig:celebrity_headline_time_train}
	\end{subfigure}
	\begin{subfigure}{0.24\textwidth}
		\includegraphics[width=\linewidth]{imagens/plot_execution_time_celebrity_headline_test.pdf}
		\caption{Celebrity: testing time for headline.}
		\label{fig:celebrity_headline_time_test}
	\end{subfigure}
	\begin{subfigure}{0.24\textwidth} 
	\includegraphics[width=\linewidth]{imagens/plot_execution_time_celebrity_content_train.pdf}
	\caption{Celebrity: training time for content.}
	\label{fig:celebrity_content_time_train}
	\end{subfigure}
	\begin{subfigure}{0.24\textwidth}
		\includegraphics[width=\linewidth]{imagens/plot_execution_time_celebrity_content_test.pdf}
		\caption{Celebrity: testing time for content.}
		\label{fig:celebrity_content_time_test}
	\end{subfigure}

	\begin{subfigure}{0.24\textwidth} 
	\includegraphics[width=\linewidth]{imagens/plot_execution_time_newsnet_headline_train.pdf}
	\caption{Fakenewsnet: training time for headline.}
	\label{fig:newsnet_headline_time_train}
\end{subfigure}
\begin{subfigure}{0.24\textwidth}
	\includegraphics[width=\linewidth]{imagens/plot_execution_time_newsnet_headline_test.pdf}
	\caption{Fakenewsnet: testing time for headline.}
	\label{fig:newsnet_headline_time_test}
\end{subfigure}
\begin{subfigure}{0.24\textwidth} 
	\includegraphics[width=\linewidth]{imagens/plot_execution_time_newsnet_content_train.pdf}
	\caption{Fakenewsnet: training time for headline.}
	\label{fig:newsnet_content_time_train}
\end{subfigure}
\begin{subfigure}{0.24\textwidth}
	\includegraphics[width=\linewidth]{imagens/plot_execution_time_newsnet_content_test.pdf}
	\caption{Fakenewsnet: testing time for content.}
	\label{fig:newsnet_content_time_test}
\end{subfigure}

	\caption{Classification time in seconds (logarithm scale) of our approach vs baseline.}
	\label{fig:loocv_time_execution}
\end{figure}




\section{Considerações Finais}
Neste capítulo apresentamos a avaliação experimental de nossa abordagem para identificar \textit{fake news} provenientes da Web. Podemos observar que quando os atributos linguísticos são extraídos do título da notícia, TAG apresenta resultados superiores aos \textit{baselines} considerados. Contudo, ao considerar o conteúdo principal da notícia, a nossa abordagem não apresenta resultados similares. Tal resultado sugere que a característica sintática do conteúdo das notícias falsas não difere substancialmente das reais.