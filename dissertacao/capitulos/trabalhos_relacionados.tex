
\chapter{Related Work}
\label{related_works_section}
Fake news has primarily drawn recent attention in a political context but it also has
been documented in topics such as vaccination, nutrition, and stock values~\cite{lazer2018science}.
In this chapter, we discuss techniques that have been proposed to detect fake news published in Web
sites and shared in social media. We also discuss publicly-available
datasets that have been used to evaluate these techniques.


\section{Fake News Datasets}
To date, there is a lack of large scale publicly-available fake news datasets in literature. Although online news can be massively collected from mainstream sites and social media, the challenges of create a corpus range from finding a properly definition for fake news~\citep{tandoc2018defining} to how determining its veracity at low cost and in a timely manner.
%Ground truth data can be gathered in the following ways~\cite{shu2017fake}: expert journalists, \textit{fact-checking} web sites, industry detectors, and crowdsourced workers.
We listed bellow some of the efforts to build a benchmark for fake news detection:

\label{datasets_survey}
\begin{itemize}
	
	
	\item \textbf{BS Detector}~\cite{bsdetector-kaggle}. This dataset contains $12$,$999$ news articles distributed over $244$ unreliable web sites. Each news article is labeled by a Chrome extension (rather than human annotators) as belonging to one of the ten following categories: \textit{fake news}, \textit{satire}, \textit{extreme bias}, \textit{conspiracy theory}, \textit{rumor mil}, \textit{state news}, \textit{junk science}, \textit{hate group}, \textit{clickbait} and \textit{proceed with caution}. The articles cover news from different domains such as politics, economy and health.
	
	\item \textbf{BuzzFeed-Webis}~\cite{}: This dataset comprises 1,627 news shared on Facebook from 9 news agencies over a week close to the 2016 U.S. election. The news were fact-checked claim-by-claim by Buzzfeed journalists, and then, they were rated as \textit{mostly true}, \textit{mixture of true and false}, \textit{mostly false}, and \textit{no factual content}.
		
	\item \textbf{Celebrity}~\cite{perez@coling2018}. This dataset covers news articles related to public figures of the entertainment industry (actors, singers, socialites). It was collected from online magazines and comprises a total of $500$ news articles labeled as \textit{fake} or \textit{real}. The ground truth was obtained using gossip-checking sites. 		
	
	\item \textbf{CREDBANK} \cite{mitra2015credbank}:
	This dataset consists of 60 million tweets collected over 5 months, from October 2014 to February 2015. The tweets are grouped into 1049 events, and each event is annotated with a credibility score based on the assessment of 30 Amazon Mechanical Turk annotators. The credibility scores are: \textit{centaintly accurate}, \textit{probably accurate}, \textit{uncertain accurate}, \textit{centaintly inaccurate}, \textit{probably inaccurate}, and \textit{uncertain inaccurate}.
	
	\item \textbf{Emergent}~\cite{silverman2015lies}. The Emergent dataset focuses on news articles that report rumors about world, business and technology. It contains $1$,$600$ articles collected from web sites during August to November of $2014$. The ground truth of each article was given by journalists and follows a truthiness scale: given a rumor with a known label, the annotators assign whether news headlines/contents are \textit{for}, \textit{against} or merely \textit{reporting} the rumor. 

	\item \textbf{LIAR}~\cite{wang2017liar}:
	LIAR is a collection of 12,836 political short statements. This dataset was collected from the fact checking site PolitiFact. The statements were sampled from a diverse set of sources (including TV, newspapers, official statements, campaign speeches), and each statement is labeled for truthfulness according the ratings: \textit{pants on fire}, \textit{false}, \textit{barely true}, \textit{half true}, \textit{mostly true} and \textit{true}.
	
	\item \textbf{NewsRealiabity}~\cite{}: This corpus includes 74,476 news articles about science, world and politics. The \textit{trusted} articles were sampled from the Gigawords corpus. For the \textit{fake} samples, the ground truth was given by a journalistc report\footnote{www.usnews.com/news/national-news/articles/2016-11-14/avoid-these-fake-news-sites-at-all-costs} which categorizes web sites as containing satire, hoax and propaganda news. 

	\item \textbf{TriFakeNews}~\cite{shu2017exploiting}.  TriFakeNews dataset comprises two sets of news articles which both contain metadata about news publishers, text pieces and social engagements. The first set has $182$ news labeled by BuzzFeed journalists and the second has $240$ news labeled by  PolitiFact site. Both datasets have an even distribution of real and fake news articles and cover the political scenario.
	
	\item \textbf{US-Election2016 Set} \cite{allcott2017social}:
	This dataset contains 948 fake news articles that circulated in the three months before the 2016 U.S. Presidential Elections. The news articles in this set were identified as fake by Snopes, PolitiFact, and BuzzFeed. 
\end{itemize}

Table\ref{} shows a summary of the publicly-available datasets described above. We can note that only the BuzzFeed-Webis and TriFakeNews datasets include metadata about news text (headline or body text), social interactions (shares, likes) and publishers info (web site bias). Althouth these two corpus present richeness in terms of features, BuzzFeed-Webis does not present a well balanced distribution of samples between classes, and TriFakeNews' samples have their ground truth assessed at a web site level -- each news of a given web site will inherit it is label. This labeling assumption does not fit sites that shares both fake and real articles.  

The others datasets also present limitations such as having few samples (Celebrity, Emergent and US-Election2016) or an unbalanced number of samples per class (Credbank); do not reflect news publishers speakers~\cite{wang2017liar} or do not have any news content metadata~\cite{allcott2017social}. We highlight that although \textit{BS Detector} dataset has a huge number of records, only $8$\% of them are labeled. The rest of records contains missing labels. Another disadvantage of this corpus is that its ground truth comes from a plugin rather than manual or fact-cheking labeling, thus any model trained on this dataset will learn indirectely the parameters of Chrome plugin~\cite{shu2017exploiting}.


\begin{table}[htb!]
\centering	
\footnotesize
\begin{adjustbox}{width=1\textwidth}

\begin{tabular}{lcccccc}
\toprule		
\textbf{Dataset}         & \textbf{Source} & \textbf{Ground truth} & \textbf{\#samples}   & \textbf{\begin{tabular}[c]{@{}l@{}} Text\\ metadata\end{tabular}} & \textbf{\begin{tabular}[c]{@{}l@{}}Social \\  metadata\end{tabular}} & \textbf{\begin{tabular}[c]{@{}l@{}}Publishers\\ metadata\end{tabular}} \\ \midrule
\textbf{BS Detector}     & Web &  Chrome Plugin  &  12,999  &   \checkmark   &  \checkmark  & \\
\textbf{BuzzFeed-Webis}    & Web, Facebook     & Journalists  &  1,627    &    \checkmark   &     \checkmark  & \checkmark \\
\textbf{Celebrity}       &   Web   & Gossip checker   & 500  &   \checkmark  &   &    \\
\textbf{Credbank}        &  Twitter              & Crowdsourcing         &   1049         &    \checkmark & \checkmark   &    \\
\textbf{Emergent}        & Web             & Journalists              &   1,600           &   \checkmark    &   &       \\
\textbf{Liar}            &  PolitiFact            & PolitiFact      &   12,836     &        \checkmark  &    &     \\
\textbf{NewsReliability} &  Web  & Journalists             & 74,476  &      \checkmark  &    &      \\
\textbf{TriFakeNews}     &  Web, Twitter       & BuzzFeed, Politifact     & 422 &  \checkmark  &  \checkmark   &     \checkmark      \\
\textbf{US-Election2016} &   Web, Facebook     & Snopes, Politifact  &    948   &   &     \checkmark   &   \\
\bottomrule                                                                    
\end{tabular}
\end{adjustbox}
\end{table}




%\begin{figure*}[t]
%	\centering
%	\includegraphics[width=\linewidth]{images/approach_overview.pdf}
%	\caption{Overview of the proposed approach.}
%	\label{fig:proposed_approach}
%\end{figure*}

\section{Fake News Detection}
Researches related to fake news detection fall into two main approaches: content-based and social context-based analysis~\cite{shu2017fake}. While the former is design to captures writing styles on news articles, the second aggregates users behavior by exploring social engagements.


Content-based analysis is core to identifying fake news as the information being reported in news pieces are primarily textual~\cite{kumar2018false}. Following this approach,~\citet{hossein2018tensor} addressed the problem of fake news detection using a tensor-based model that clusters news articles into different fake news categories. Their proposed model is aimed to explore the potential of content by capturing latent relations between articles and terms, as well as spatial relations between terms. They achieved 0.80 of homogeneity per fake news category over a subset of \textit{BS Detector}~\cite{bsdetector-kaggle} dataset. %

To identify linguistic characteristics of untrustworthy text,~\cite{rashkin2017truth} compared the language of real news with that of satire, hoaxes, and propaganda.
They then studied the feasibility of predicting the reliability of the news article into four categories: trusted, satire, hoax, or propaganda.
Their Max-Entropy classifier with L2 regularization on n-gram tf-idf feature vectors resulted in F1 scores of 0.65.
%
Other works have represented news articles by a combination of writing styles attributes~\citep{horne2017just, potthast@arxiv2017, perez@coling2018}.
\citet{perez@coling2018} combined morphological (POS tags), syntactic (context free grammar productions), understadability (readability indexes), psychological (LIWC~\citep{pennebaker2015liwc}), and n-grams (enconded by TF-IDF) patterns to build a classification model. Their model achieved accuracy values up to $0.76$ on \textit{Celebrity} dataset. The authors highlight that legitimate news in tabloid and entertainment magazines seem to
use more first person pronouns, talk about time, and use positive emotion words; while fake content has a predominant use of second person pronouns, negative emotion words and focus on the present. 

~\citet{horne2017just} and~\citet{potthast@arxiv2017} studied satire, fake and real news articles. To build a classifier for distinguish between these news categories, they used complexity (median depth of syntax tree, Type-Token Ratio, etc), stylistic (POS tags) and psychological (LIWC) features.~\citet{horne2017just} findings include that the language used on fake news is more similar to satire than real and it is aimed to create mental associations between entities and claims. They reported accuracy values of 0.91 and 0.78 in the tasks of distinguishing real from satire news and fake from satire, respectively.~\citet{potthast@arxiv2017} built two classification models to the task of differentiating between satire, fake, mainstream and hyperpartisan news articles. The first model is topic-based (standard \textit{bag of words}) and the second is style-based (n-grams, readability scores, and the average number of words per paragraph). They found that style-based and topic-based classifiers are somewhat effective at differentiating hyperpartisan news from mainstream news (accuracy values up to 0.71 for both models). However, they were not effective at differentiating fake from real news (0.55 accuracy for style-based and 0.52 for topic-based).

\cite{fairbanks2018credibility} investigated whether credibility and bias can be assessed using content-based (n-grams encoded by TF-IDF values) and structure-based methods. The structure-based method constructs a reputation graph where each node represents a site, and the edges represent mutually linked sites, as well as shared CSS, JavaScript, and image files. They found that both methods achieved high AUC values  in detecting bias, but only the structure-based model was able no attain high effectiviness in detecting credibility (AUC value of 0.35 for content-based and 0.88 for structure-based). As the authors emphasize, the AUC for the content model droped due to the unbalanced distributions of samples per class.    

Social context-based approaches are also important due to the ability of classification models to extract users response and the spreading patterns of news stories~\citep{castillo2011information, shu2017exploiting, ruchansky2017csi}.
%
\citet{castillo2011information} analyzed tweets related to``trending'' topics and classified them as credible or not credible. They represented the tweets by features based on messages (e.g., length, punctuation), users (e.g., number of followers, registration age), topics (e.g., ratio of positive/negative sentiment, ratio of Urls)  and propagations (e.g., depth of re-tweet tree) characteristics. They reported results of precision and recall in the range of $0.70$ and $0.80$ in the credibility assignment task.

Following a similar idea, \citet{shu2017exploiting} proposed the TriFN framework, which captures the tri-relationship between news publishers, articles and users. TriFN combines latent matrix representations with a semi-supervised linear classifier to
make predictions over \textit{TriFakeNews} dataset. They found that content-related features are not effective on their own, but when combined with features related to the publishers (partisan bias) and the users' credibility, they can attain accuracy values up to $0.83$. We note that in this work, although their dataset is rich in terms of social metadata, they had less than 125 news samples per class (see \ref{datasets_survey}) which reflects directly in their findings about news content. 
%

\citet{ruchansky2017csi} proposed a framework, called CSI, which consists of a Recurrent Neural Network
model to detect fake news based on the response of a given post received from users. The news articles are represented by features as the frequency of temporal spacing of user activity, and user propensity score to engage in a post thread. Their model achieved F1-macro values up 0.84 over a Twitter dataset and 0.93 over a Weibo dataset. 

\begin{table}[]
	\begin{adjustbox}{width=1\textwidth}	
	\begin{tabular}{lccc}
		\toprule                                                                    		
		\textbf{Work} & \textbf{Learning Approach}   & \textbf{Features}       & \textbf{Dataset} \\
		\midrule
		\citet{castillo2011information}   & Decision Tree & Linguistic+Social       & Private          \\
		\citet{shu2017exploiting}          & Support Vector Machine & Linguistic+Social       & TriFakeNews      \\
		\citet{rashkin2017truth}            & \begin{tabular}[c]{@{}c@{}}Max-Entropy, Naive Bayes\\  Long Short-term Memory\end{tabular}              & Linguistic              & NewsReliability  \\
		\citet{horne2017just}                 & Support Vector Machine & Linguistic              & BuzzFeed-Webis   \\
		\citet{potthast@arxiv2017}        & Random Forest & Linguistic              & Emergent         \\
		\citet{ruchansky2017csi}           & Recurrent Neural Network & Linguistic + Social     & Twitter          \\
		\citet{perez@coling2018}      & Support Vector Machine   & Linguistic              & Celebrity        \\
		\citet{hossein2018tensor}        & Ensemble Clustering & Linguistic              & BS Detector      \\
		\citet{fairbanks2018credibility} & \begin{tabular}[c]{@{}c@{}}Logistic Regression, Random Forest,\\ Loopy Belief Propagation\end{tabular} & Linguistic, Web Network & Private   \\
	\bottomrule                                                                    		      
	\end{tabular}
	\end{adjustbox}

\end{table}

\section{Considerações Finais}
Neste capítulo apresentamos uma breve revisão sobre trabalhos da literatura recente que investigaram o problema de \textit{fake news} relacionados a sua detecção. Adicionalmente apresentamos algumas das principais base de dados sobre o tema. Destacamos que nossa abordagem (detalhada na Seção \ref{sec:metodologia}) difere das demais apresentadas ao incluir tanto aspectos linguísticos relacionados ao conteúdo da notícia, quanto aspectos da estrutura HTML de \textit{webpages} (images, anúncios, vídeos). Além disso, por ser independente de contextos/tópicos, nossa abordagem consegue se adaptar a dinamicidade de notícias (tópicos mudam ao decorrer do tempo), enquanto se mantém eficaz. 

falar sobre abordagem(ns), fairbanks...

