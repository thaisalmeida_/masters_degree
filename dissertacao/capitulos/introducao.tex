\chapter{Introdução}\label{sec:introducao}

Notícias falsas têm sido cada vez mais utilizadas para manipular a opinião popular e influenciar os processos políticos. Isso foi possível por meio do desenvolvimento da infraestrutura da Web e pela popularidade de plataformas de mídias online, como o Facebook e o Twitter, que facilitam o compartilhamento de informações. Ao contrário das tecnologias de mídia anteriores, as informações podem ser publicadas em sites da Web e compartilhadas entre usuários de mídias sociais sem filtragem, verificação de fatos ou julgamento editorial (significativo) de terceiros~\citep{allcott2017social}. Dados de estudos recentes mostraram que 62\% da população adulta nos Estados Unidos obtém notícias por meio de mídias sociais, e pessoas que foram expostas a notícias falsas reportaram que acreditaram em sua veracidade~\citep{silverman2016}. Assim, essas plataformas se tornaram alvo de campanhas políticas pelo seu poder de influência~\citep{narayanan2018polarization, lazer2018science}. 

Embora notícias falsas tenham atraído atenção substancial na literatura recente, o problema ainda não é bem compreendido~\citep{lazer2018science}. Para entender melhor notícias falsas, como elas são propagadas e como neutralizar seus efeitos, é necessário primeiramente identificá-las. Tal processo é desafiador, uma vez que elas compreendem tópicos e estilos de linguagem diferentes, além de serem compartilhadas em plataformas online distintas~\citep{shu2017fake}. Além disso, até mesmo humanos alcançam baixas taxas de acerto ao distinguir entre artigos de notícias falsas e reais~\citep{domonoske2016students,forbes2016}.


Em 2013, o Fórum de Economia Mundial listou ``a massiva desinformação digital'' como um dos principais riscos para a sociedade moderna~\citep{howell2013digital}, uma vez que notícias falsas possuem o potencial de afetar a opinião pública, fornecendo uma ambiente favorável a manipulações de resultados de eventos como eleições~\citep{allcott2008fakenews,castillo2011information}. Desta forma, o objetivo deste trabalho surge da necessidade do desenvolvimento de métodos automáticos para identificar notícias falsas em tempo hábil, a fim de minimizar seus efeitos sobre a sociedade.

\section{Problema}


Neste trabalho, estudamos o problema de detectar notícias falsas publicadas na Web. Dada uma \textit{webpage}, nosso objetivo é determinar se a página contém uma notícia falsa ou real. Notamos que não há uma definição amplamente aceita para notícias falsas. 
Assim, nós consideramos falsas não apenas notícias provenientes de sites que publicam histórias fabricadas, mas também aquelas que provêm de sites que têm um padrão de manchetes enganosas e que promovem teorias da conspiração. 

Abordagens anteriores para identificação de notícias falsas se concentraram amplamente no uso do conteúdo das notícias para determinar suas veracidades~\citep{rashkin2017truth,hossein2018tensor}. No entanto, utilizar somente o conteúdo tem limitações importantes. Notavelmente, dada a natureza dinâmica das notícias, à medida que novos eventos são relatados, os tópicos e os discursos mudam constantemente e, portanto, um classificador treinado usando o conteúdo de artigos publicados em um determinado momento provavelmente se tornará ineficaz no futuro.
Além disso, estudos relatam que o conteúdo da página, por si só, não é suficiente para classificar com exatidão a veracidade das notícias~\citep{fairbanks2018credibility,wu2018tracing,ruchansky2017csi}.

Para enfrentar essa limitação, propomos uma nova estratégia de classificação que é independente de tópicos/contextos: em vez de usar o vocabulário de uma coleção, utilizamos recursos de marcação da Web e linguísticos (sintáticos) que capturam a estrutura de notícias falsas e reais. 


\section{Objetivos}
O objetivo geral deste trabalho é propor e demonstrar a eficácia de uma abordagem para detecção de notícias falsas baseada em características de notícias que são independentes de contexto. Tal método utilizará técnicas e conceitos das áreas de Recuperação de Informação e Aprendizagem de Máquina. 

Os objetivos específicos incluem:

\begin{enumerate}
\item Verificar a viabilidade do uso de \textit{webpages} como base de dados para caracterização de notícias falsas;

\item Propor uma abordagem que combine aspectos derivados da estrutura de notícias online para detectar notícias falsas;

\item Demonstrar por meio de estudos de caso a eficácia da abordagem proposta em relação ao estado-da-arte.

\end{enumerate}

\section{Organização da Dissertação}\label{sec:organiza}
O Capítulo \ref{chap:fundamentacao}, descreve os conceitos necessários para o entendimento dos aspectos gerais que compõem a abordagem proposta, bem como os \textit{baselines}. O Capítulo \ref{sec:trabalhos_relacionados} apresenta uma breve revisão da literatura recente sobre detecção de notícias falsas. Além disso, apresenta as bases de dados publicamente disponíveis para essa tarefa. O Capítulo \ref{sec:metodologia} detalha a abordagem proposta neste trabalho. O Capítulo \ref{sec:experimentos} apresenta a avaliação experimental de nossa abordagem, bem como os resultados obtidos. Por fim, o Capítulo 6 apresenta nossas conclusões parciais, limitações e trabalhos futuros.


